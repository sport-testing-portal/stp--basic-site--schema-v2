# #SCHEMA TESTING IS COMPLETE - THIS PROJECT IS ARCHIVED AND READ ONLY


# Directory Structure

```
  assets/             contains assets definition
  commands/           contains console commands (controllers)
  config/             contains application configurations
  controllers/        contains Web controller classes
  mail/               contains view files for e-mails
  models/             contains model classes
  runtime/            contains files generated during runtime
  tests/              contains various tests for the basic application
  vendor/             contains dependent 3rd-party packages
  views/              contains view files for the Web application
  web/                contains the entry script and Web resources
```  


chmod 776 -Rc debug

/var/www/sites/orgs/gitlab/stp--basic-site--schema-v2/yii2-app-basic/runtime

******************

cd /var/www/sites/orgs/gitlab/stp--basic-site--schema-v2/yii2-app-basic/runtime

chmod 776 -Rc debug

## Yii2 Extensions - Alphabetically
yii2-curl
yii2-encrypter
yii2-enhanced-gii
yii2-gravatar
yii2-nested-rest
yii2-rbac
yii2-save-relations-behavior
yii2-user

## Yii2 Extensions - By Category
@todo define categories
yii2-curl
yii2-encrypter
yii2-gravatar
yii2-save-relations-behavior
yii2-enhanced-gii
yii2-nested-rest
yii2-rbac
yii2-user

## Yii2 Extensions - Disabmbiguation
@todo include the author namespace of the extension
yii2-curl
yii2-encrypter
yii2-gravatar
yii2-save-relations-behavior
mootensai/yii2-enhanced-gii
tunecino/yii2-nested-rest
dektrium/yii2-rbac
dektrium/yii2-user