<?php

use yii\db\Migration;

class m180813_155554_create_table_notification_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification_type}}', [
            'notification_type_id' => $this->primaryKey(),
            'notification_type_name' => $this->string()->notNull(),
            'notification_type_desc_short' => $this->string(),
            'notification_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('notification_type__notification_type_name', '{{%notification_type}}', 'notification_type_name', true);
    }

    public function down()
    {
        $this->dropTable('{{%notification_type}}');
    }
}
