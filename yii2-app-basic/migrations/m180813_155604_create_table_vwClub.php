<?php

use yii\db\Migration;

class m180813_155604_create_table_vwClub extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwClub}}', [
            'org_type_name' => $this->string()->comment('school, camp, club, vendor, venue, media, '),
            'org_id' => $this->integer()->notNull()->defaultValue('0'),
            'org_type_id' => $this->integer(),
            'org_name' => $this->string()->notNull(),
            'org_website_url' => $this->string(),
            'org_twitter_url' => $this->string(),
            'org_facebook_url' => $this->string(),
            'org_phone_main' => $this->string(),
            'org_email_main' => $this->string(),
            'org_addr1' => $this->string(),
            'org_addr2' => $this->string(),
            'org_addr3' => $this->string(),
            'org_city' => $this->string(),
            'org_state_or_region' => $this->string(),
            'org_postal_code' => $this->string(),
            'org_country_code_iso3' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'org_created_by_username' => $this->string(),
            'org_updated_by_username' => $this->string(),
            'created_by_user_id' => $this->integer(),
            'updated_by_user_id' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwClub}}');
    }
}
