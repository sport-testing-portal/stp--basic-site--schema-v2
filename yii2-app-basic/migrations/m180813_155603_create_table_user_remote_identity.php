<?php

use yii\db\Migration;

class m180813_155603_create_table_user_remote_identity extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_remote_identity}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'provider' => $this->string()->notNull(),
            'identifier' => $this->string()->notNull(),
            'created_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'last_used_on' => $this->timestamp()->notNull()->defaultValue('0000-00-00 00:00:00'),
        ], $tableOptions);

        $this->createIndex('user_remote_identity_provider_identifier_idx', '{{%user_remote_identity}}', ['provider', 'identifier']);
        $this->createIndex('user_remote_identity_user_id_idx', '{{%user_remote_identity}}', 'user_id');
        $this->addForeignKey('fk_user_remote_identity__user', '{{%user_remote_identity}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user_remote_identity}}');
    }
}
