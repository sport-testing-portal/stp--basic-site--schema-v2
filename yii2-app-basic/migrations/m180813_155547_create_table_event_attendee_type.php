<?php

use yii\db\Migration;

class m180813_155547_create_table_event_attendee_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_attendee_type}}', [
            'event_attendee_type_id' => $this->primaryKey(),
            'event_attendee_type_name' => $this->string()->notNull()->comment('player, coach, camp contact, school_contact, etc'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('event_attendee_type__event_attendee_type_name', '{{%event_attendee_type}}', 'event_attendee_type_name', true);
    }

    public function down()
    {
        $this->dropTable('{{%event_attendee_type}}');
    }
}
