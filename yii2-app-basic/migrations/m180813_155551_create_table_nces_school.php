<?php

use yii\db\Migration;

class m180813_155551_create_table_nces_school extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%nces_school}}', [
            'nces_school_id' => $this->primaryKey(),
            'nces_school_name' => $this->string(),
            'nces_school_website' => $this->string(),
            'nces_school_unit_id' => $this->integer(),
            'nces_school_addr1' => $this->string(),
            'nces_school_addr2' => $this->string(),
            'nces_school_ope_id' => $this->integer(),
            'nces_school_city' => $this->string(),
            'nces_school_state' => $this->string(),
            'nces_school_zipcode' => $this->string(),
            'nces_school_phone_general' => $this->string(),
            'nces_school_admin' => $this->string(),
            'nces_school_admin_title' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('ix_nces_school_unit_id', '{{%nces_school}}', 'nces_school_unit_id');
        $this->createIndex('ix_nces_school__website', '{{%nces_school}}', 'nces_school_website');
    }

    public function down()
    {
        $this->dropTable('{{%nces_school}}');
    }
}
