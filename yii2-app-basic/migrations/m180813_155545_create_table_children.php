<?php

use yii\db\Migration;

class m180813_155545_create_table_children extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%children}}', [
            'id' => $this->primaryKey(),
            'father_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'age' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('father_id', '{{%children}}', 'father_id');
        $this->addForeignKey('children_ibfk_1', '{{%children}}', 'father_id', '{{%fathers}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%children}}');
    }
}
