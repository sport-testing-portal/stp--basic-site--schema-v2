<?php

use yii\db\Migration;

class m180813_155605_create_table_vwTeam extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTeam}}', [
            'org_name' => $this->string(),
            'team_name' => $this->string(),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'organizational_level' => $this->string()->comment('National Team, Professional'),
            'gender_code' => $this->string(),
            'gender_desc' => $this->string(),
            'coach_name' => $this->string(),
            'player_name' => $this->string(),
            'org_id' => $this->integer()->defaultValue('0'),
            'team_id' => $this->integer()->notNull()->defaultValue('0'),
            'coach_id' => $this->integer()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'coach__person_id' => $this->integer()->defaultValue('0'),
            'player__person_id' => $this->integer()->defaultValue('0'),
            'team_created_by_username' => $this->string(),
            'team_updated_by_username' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTeam}}');
    }
}
