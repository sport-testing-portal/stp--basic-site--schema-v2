<?php

use yii\db\Migration;

class m180813_155555_create_table_person_certification extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%person_certification}}', [
            'person_certification_id' => $this->primaryKey()->comment('person_certification_year'),
            'person_id' => $this->integer(),
            'person_certification_type_id' => $this->integer(),
            'person_certification_year' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_person_certification__person_idx', '{{%person_certification}}', 'person_id');
        $this->createIndex('fk_person_certification__per_cert_type_idx', '{{%person_certification}}', 'person_certification_type_id');
        $this->addForeignKey('fk_person_certification__per_cert_type', '{{%person_certification}}', 'person_certification_type_id', '{{%person_certification_type}}', 'person_certification_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_person_certification__person', '{{%person_certification}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%person_certification}}');
    }
}
