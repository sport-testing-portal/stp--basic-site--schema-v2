<?php

use yii\db\Migration;

class m180813_155542_create_table_app_user_device extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_user_device}}', [
            'app_user_device_id' => $this->primaryKey(),
            'app_user_id' => $this->integer(),
            'user_id' => $this->integer(),
            'device_description' => $this->string(),
            'screen_dimensions' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_app_user_device__user_idx', '{{%app_user_device}}', 'user_id');
        $this->addForeignKey('fk_app_user_device__app_user', '{{%app_user_device}}', 'app_user_id', '{{%app_user}}', 'app_user_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_app_user_device__user', '{{%app_user_device}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%app_user_device}}');
    }
}
