<?php

use yii\db\Migration;

class m180813_155542_create_table_app_user_login_attempt extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_user_login_attempt}}', [
            'app_user_login_attempt_id' => $this->primaryKey(),
            'app_user_id' => $this->integer(),
            'username' => $this->string(),
            'performed_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'is_successful' => $this->tinyInteger()->defaultValue('0'),
            'session_id' => $this->string(),
            'ipv4' => $this->integer(),
            'user_agent' => $this->string(),
        ], $tableOptions);

        $this->createIndex('fk_app_user_login_attempt__app_user_idx', '{{%app_user_login_attempt}}', 'app_user_id');
        $this->addForeignKey('fk_app_user_login_attempt__app_user', '{{%app_user_login_attempt}}', 'app_user_id', '{{%app_user}}', 'app_user_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%app_user_login_attempt}}');
    }
}
