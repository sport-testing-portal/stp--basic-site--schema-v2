<?php

use yii\db\Migration;

class m180813_155605_create_table_vwSiteOperations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwSiteOperations}}', [
            'name' => $this->string()->notNull(),
            'type' => $this->integer()->notNull(),
            'description' => $this->text(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwSiteOperations}}');
    }
}
