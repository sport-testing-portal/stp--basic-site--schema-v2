<?php

use yii\db\Migration;

class m180813_155556_create_table_player_metrix_by_the_numbers_structure extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_metrix_by_the_numbers_structure}}', [
            'id' => $this->primaryKey(),
            'person_age' => $this->integer(),
            'test_date' => $this->string(),
            'test_desc' => $this->string(),
            'split_raw' => $this->string(),
            'split_avg' => $this->decimal(),
            'split_cnt' => $this->integer(),
            'score' => $this->decimal(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%player_metrix_by_the_numbers_structure}}');
    }
}
