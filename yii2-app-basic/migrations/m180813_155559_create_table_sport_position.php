<?php

use yii\db\Migration;

class m180813_155559_create_table_sport_position extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sport_position}}', [
            'sport_position_id' => $this->primaryKey(),
            'sport_id' => $this->integer(),
            'sport_position_name' => $this->string()->notNull()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'display_order' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_sport_position__sport_idx', '{{%sport_position}}', 'sport_id');
        $this->createIndex('sport_position_idx_sport_position_name', '{{%sport_position}}', 'sport_position_name');
        $this->addForeignKey('fk_sport_position__sport', '{{%sport_position}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%sport_position}}');
    }
}
