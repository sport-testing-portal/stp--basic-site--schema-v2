<?php

use yii\db\Migration;

class m180813_155541_create_table_address extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%address}}', [
            'address_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'person_id' => $this->integer(),
            'address_type_id' => $this->integer(),
            'addr1' => $this->string(),
            'addr2' => $this->string(),
            'addr3' => $this->string()->comment('Needed for some international addresses'),
            'city' => $this->string(),
            'state_or_region' => $this->string(),
            'postal_code' => $this->string(),
            'country' => $this->string(),
            'country_code' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_address__org_idx', '{{%address}}', 'org_id');
        $this->createIndex('fk_address__address_type_idx', '{{%address}}', 'address_type_id');
        $this->createIndex('fk_address__person_idx', '{{%address}}', 'person_id');
        $this->addForeignKey('fk_address__address_type', '{{%address}}', 'address_type_id', '{{%address_type}}', 'address_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_address__org', '{{%address}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_address__person', '{{%address}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%address}}');
    }
}
