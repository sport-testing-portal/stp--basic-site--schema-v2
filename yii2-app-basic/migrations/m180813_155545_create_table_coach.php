<?php

use yii\db\Migration;

class m180813_155545_create_table_coach extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%coach}}', [
            'coach_id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'coach_type_id' => $this->integer(),
            'coach_team_coach_id' => $this->integer()->comment('The coach\'s currently selected team. Used to drive UI.'),
            'age_group_id' => $this->integer(),
            'coach_specialty' => $this->string(),
            'coach_certifications' => $this->string(),
            'coach_comments' => $this->string(),
            'coach_qrcode_uri' => $this->string()->comment('internal URL to the coach QR code'),
            'coach_info_source_scrape_url' => $this->string()->comment('coach info programmatic source data update url'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_coach__person_idx', '{{%coach}}', 'person_id');
        $this->createIndex('fk_coach__team_coach_idx', '{{%coach}}', 'coach_team_coach_id');
        $this->createIndex('fk_coach__coach_type_idx', '{{%coach}}', 'coach_type_id');
        $this->createIndex('fk_coach__age_group_idx', '{{%coach}}', 'age_group_id');
        $this->addForeignKey('fk_coach__age_group', '{{%coach}}', 'age_group_id', '{{%age_group}}', 'age_group_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_coach__coach_type', '{{%coach}}', 'coach_type_id', '{{%coach_type}}', 'coach_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_coach__person', '{{%coach}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_coach__team_coach', '{{%coach}}', 'coach_team_coach_id', '{{%team_coach}}', 'team_coach_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%coach}}');
    }
}
