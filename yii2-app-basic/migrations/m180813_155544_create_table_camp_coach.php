<?php

use yii\db\Migration;

class m180813_155544_create_table_camp_coach extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_coach}}', [
            'camp_coach_id' => $this->primaryKey(),
            'camp_id' => $this->integer(),
            'coach_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp_coach__coach_idx', '{{%camp_coach}}', 'coach_id');
        $this->createIndex('fk_camp_coach__camp_idx', '{{%camp_coach}}', 'camp_id');
        $this->addForeignKey('fk_camp_coach__camp', '{{%camp_coach}}', 'camp_id', '{{%camp}}', 'camp_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_coach__coach', '{{%camp_coach}}', 'coach_id', '{{%coach}}', 'coach_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp_coach}}');
    }
}
