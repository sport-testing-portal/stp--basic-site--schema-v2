<?php

use yii\db\Migration;

class m180813_155541_create_table_AuthItemChild extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%AuthItemChild}}', [
            'parent' => $this->string()->notNull(),
            'child' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%AuthItemChild}}', ['parent', 'child']);
        $this->createIndex('child', '{{%AuthItemChild}}', 'child');
        $this->addForeignKey('AuthItemChild_ibfk_1', '{{%AuthItemChild}}', 'parent', '{{%AuthItem}}', 'name', 'CASCADE', 'CASCADE');
        $this->addForeignKey('AuthItemChild_ibfk_2', '{{%AuthItemChild}}', 'child', '{{%AuthItem}}', 'name', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%AuthItemChild}}');
    }
}
