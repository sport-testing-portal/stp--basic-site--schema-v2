<?php

use yii\db\Migration;

class m180813_155542_create_table_app_user_location extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_user_location}}', [
            'app_user_location_id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'app_user_id' => $this->integer(),
            'location_id' => $this->integer(),
            'location_description' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_app_user_location__user_idx', '{{%app_user_location}}', 'user_id');
        $this->createIndex('fk_app_user_location__location_idx', '{{%app_user_location}}', 'location_id');
        $this->addForeignKey('fk_app_user_location__app_user', '{{%app_user_location}}', 'app_user_id', '{{%app_user}}', 'app_user_id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_app_user_location__location', '{{%app_user_location}}', 'location_id', '{{%location}}', 'location_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_app_user_location__user', '{{%app_user_location}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%app_user_location}}');
    }
}
