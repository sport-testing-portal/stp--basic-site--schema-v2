<?php

use yii\db\Migration;

class m180813_155542_create_table_app_user_profile_picture extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_user_profile_picture}}', [
            'app_user_profile_picture_id' => $this->primaryKey(),
            'app_user_id' => $this->integer()->notNull(),
            'app_user_profile_original_picture_id' => $this->integer(),
            'app_user_profile_picture_filename' => $this->string()->notNull(),
            'app_user_profile_picture_width' => $this->integer()->notNull(),
            'app_user_profile_picture_height' => $this->integer()->notNull(),
            'app_user_profile_picture_mimetype' => $this->string()->notNull(),
            'app_user_profile_picture_contents' => $this->text()->notNull(),
            'created_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('fk_app_user_profile_pictures__app_user_idx', '{{%app_user_profile_picture}}', 'app_user_id');
        $this->createIndex('user_profile_picture__width_height_idx', '{{%app_user_profile_picture}}', ['app_user_profile_picture_width', 'app_user_profile_picture_height']);
        $this->addForeignKey('fk_app_user_profile_picture__app_user', '{{%app_user_profile_picture}}', 'app_user_id', '{{%app_user}}', 'app_user_id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%app_user_profile_picture}}');
    }
}
