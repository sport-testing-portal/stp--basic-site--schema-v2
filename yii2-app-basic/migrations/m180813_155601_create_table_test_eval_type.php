<?php

use yii\db\Migration;

class m180813_155601_create_table_test_eval_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_type}}', [
            'test_eval_type_id' => $this->primaryKey(),
            'test_eval_provider_id' => $this->integer(),
            'test_eval_category_id' => $this->integer(),
            'test_eval_type_name' => $this->string()->notNull()->comment('This is category#1 in Jami - lingo (the language of the domain)'),
            'test_eval_type_name_sub_type' => $this->string()->comment('This is category#2 in Jami - lingo (the language of the domain)'),
            'test_eval_type_desc_short' => $this->string(),
            'test_eval_type_desc_long' => $this->string()->comment('Used for tool tips'),
            'test_eval_type_display_order' => $this->integer(),
            'effective_begin_dt' => $this->dateTime(),
            'effective_end_dt' => $this->dateTime(),
            'units' => $this->string(),
            'format' => $this->string(),
            'splits' => $this->tinyInteger(),
            'checkpoints' => $this->tinyInteger(),
            'attempts' => $this->tinyInteger(),
            'qualifier' => $this->string(),
            'backend_calculation' => $this->string(),
            'metric_equivalent' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_test_eval_type__test_eval_category_idx', '{{%test_eval_type}}', 'test_eval_category_id');
        $this->createIndex('test_eval_type_idx_test_eval_type_name_sub_type', '{{%test_eval_type}}', 'test_eval_type_name_sub_type');
        $this->createIndex('pet_display_order', '{{%test_eval_type}}', 'test_eval_type_display_order');
        $this->createIndex('pet_eff_end_dt', '{{%test_eval_type}}', 'effective_end_dt');
        $this->createIndex('test_eval_type_idx_test_eval_type_name', '{{%test_eval_type}}', 'test_eval_type_name');
        $this->createIndex('pet_eff_begin_dt', '{{%test_eval_type}}', 'effective_begin_dt');
        $this->addForeignKey('fk_pet__test_eval_provider', '{{%test_eval_type}}', 'test_eval_provider_id', '{{%test_eval_provider}}', 'test_eval_provider_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_test_eval_type__test_eval_category', '{{%test_eval_type}}', 'test_eval_category_id', '{{%test_eval_category}}', 'test_eval_category_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%test_eval_type}}');
    }
}
