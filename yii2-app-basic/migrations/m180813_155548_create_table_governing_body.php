<?php

use yii\db\Migration;

class m180813_155548_create_table_governing_body extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%governing_body}}', [
            'governing_body_id' => $this->primaryKey(),
            'sport_id' => $this->integer(),
            'governing_body_name' => $this->string()->notNull(),
            'governing_body_short_desc' => $this->string(),
            'governing_body_long_desc' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_governing_body__sport_idx', '{{%governing_body}}', 'sport_id');
        $this->addForeignKey('fk_governing_body__sport', '{{%governing_body}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%governing_body}}');
    }
}
