<?php

use yii\db\Migration;

class m180813_155542_create_table_age_group extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%age_group}}', [
            'age_group_id' => $this->primaryKey(),
            'sport_id' => $this->integer(),
            'age_group_name' => $this->string()->notNull(),
            'age_group_desc' => $this->string(),
            'age_group_min' => $this->integer(),
            'age_group_max' => $this->integer(),
            'display_order' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('age_group_idx_age_group_name', '{{%age_group}}', 'age_group_name');
    }

    public function down()
    {
        $this->dropTable('{{%age_group}}');
    }
}
