<?php

use yii\db\Migration;

class m180813_155604_create_table_vwOrgTeam_leftouter extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwOrgTeam_leftouter}}', [
            'org_name' => $this->string()->notNull(),
            'org_type_name' => $this->string()->comment('school, camp, club, vendor, venue, media, '),
            'team_name' => $this->string(),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'gender_code' => $this->string(),
            'gender_desc' => $this->string(),
            'coach_name' => $this->string(),
            'player_name' => $this->string(),
            'gsm_sport_name' => $this->string(),
            'org_id' => $this->integer()->notNull()->defaultValue('0'),
            'org_type_id' => $this->integer(),
            'team_id' => $this->integer()->defaultValue('0'),
            'coach_id' => $this->integer()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'coach__person_id' => $this->integer()->defaultValue('0'),
            'player__person_id' => $this->integer()->defaultValue('0'),
            'player__person_org_id' => $this->integer(),
            'team_created_by_username' => $this->string(),
            'team_updated_by_username' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwOrgTeam_leftouter}}');
    }
}
