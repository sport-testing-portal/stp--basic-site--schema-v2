<?php

use yii\db\Migration;

class m180813_155557_create_table_player_sport extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_sport}}', [
            'player_sport_id' => $this->primaryKey(),
            'player_id' => $this->integer(),
            'sport_id' => $this->integer(),
            'sport_position_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_player_sport__sport_position_idx', '{{%player_sport}}', 'sport_position_id');
        $this->createIndex('fk_player_sport__gender_idx', '{{%player_sport}}', 'gender_id');
        $this->addForeignKey('fk_player_sport__sport', '{{%player_sport}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_sport__sport_position', '{{%player_sport}}', 'sport_position_id', '{{%sport_position}}', 'sport_position_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_sport__gender', '{{%player_sport}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_player_sport__player', '{{%player_sport}}', 'player_id', '{{%player}}', 'player_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%player_sport}}');
    }
}
