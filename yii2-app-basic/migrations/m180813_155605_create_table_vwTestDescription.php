<?php

use yii\db\Migration;

class m180813_155605_create_table_vwTestDescription extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTestDescription}}', [
            'test_category' => $this->string()->notNull(),
            'test_type' => $this->string()->comment('This is category#1 in Jami - lingo (the language of the domain)'),
            'test_desc' => $this->string(),
            'provider_id' => $this->integer(),
            'cat_id' => $this->integer()->notNull()->defaultValue('0'),
            'type_id' => $this->integer()->defaultValue('0'),
            'desc_id' => $this->integer()->defaultValue('0'),
            'cat_order' => $this->integer(),
            'type_order' => $this->integer(),
            'desc_order' => $this->integer(),
            'test_desc_to_display' => $this->string(),
            'provider_code' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTestDescription}}');
    }
}
