<?php

use yii\db\Migration;

class m180813_155545_create_table_contact_message_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contact_message_type}}', [
            'contact_message_type_id' => $this->primaryKey(),
            'contact_message_type_name' => $this->string(),
            'contact_message_type_short_desc' => $this->string(),
            'contact_message_type_long_desc' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%contact_message_type}}');
    }
}
