<?php

use yii\db\Migration;

class m180813_155544_create_table_camp_locations extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp_locations}}', [
            'camp_locations_id' => $this->primaryKey(),
            'camp_id' => $this->integer(),
            'address_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp_locations__camp_idx', '{{%camp_locations}}', 'camp_id');
        $this->createIndex('fk_camp_locations__address_idx', '{{%camp_locations}}', 'address_id');
        $this->addForeignKey('fk_camp_locations__address', '{{%camp_locations}}', 'address_id', '{{%address}}', 'address_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_camp_locations__camp', '{{%camp_locations}}', 'camp_id', '{{%camp}}', 'camp_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp_locations}}');
    }
}
