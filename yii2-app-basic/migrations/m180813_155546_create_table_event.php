<?php

use yii\db\Migration;

class m180813_155546_create_table_event extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event}}', [
            'event_id' => $this->primaryKey(),
            'event_type_id' => $this->integer(),
            'event_name' => $this->string(),
            'event_access_code' => $this->string(),
            'event_location_name' => $this->string(),
            'event_location_map_url' => $this->string(),
            'event_scheduled_dt' => $this->dateTime(),
            'event_participation_fee' => $this->decimal(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_event__event_type_idx', '{{%event}}', 'event_type_id');
        $this->addForeignKey('fk_event__event_type', '{{%event}}', 'event_type_id', '{{%event_type}}', 'event_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%event}}');
    }
}
