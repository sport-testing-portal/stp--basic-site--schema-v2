<?php

use yii\db\Migration;

class m180813_155601_create_table_test_eval_summary_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_summary_log}}', [
            'test_eval_summary_log_id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'team_id' => $this->integer(),
            'test_eval_type_id' => $this->integer(),
            'source_event_id' => $this->integer()->comment('This value comes from STI'),
            'test_eval_overall_ranking' => $this->decimal(),
            'test_eval_positional_ranking' => $this->decimal(),
            'test_eval_total_score' => $this->decimal(),
            'test_eval_avg_score' => $this->decimal(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_tesl_team_idx', '{{%test_eval_summary_log}}', 'team_id');
        $this->createIndex('idx_tesl_source_event_id', '{{%test_eval_summary_log}}', 'source_event_id');
        $this->addForeignKey('fk_pesl__test_eval_type', '{{%test_eval_summary_log}}', 'test_eval_type_id', '{{%test_eval_type}}', 'test_eval_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_tesl_team', '{{%test_eval_summary_log}}', 'team_id', '{{%team}}', 'team_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_pesl__person', '{{%test_eval_summary_log}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%test_eval_summary_log}}');
    }
}
