<?php

use yii\db\Migration;

class m180813_155605_create_table_vwTableRowCount_v00 extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTableRowCount_v00}}', [
            'table_name' => $this->string()->notNull()->defaultValue(''),
            'cnt' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTableRowCount_v00}}');
    }
}
