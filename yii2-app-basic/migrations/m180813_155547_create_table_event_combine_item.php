<?php

use yii\db\Migration;

class m180813_155547_create_table_event_combine_item extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%event_combine_item}}', [
            'event_combine_item_id' => $this->primaryKey(),
            'event_combine_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_event_combine_item__event_combine_idx', '{{%event_combine_item}}', 'event_combine_id');
        $this->addForeignKey('fk_event_combine_item__event_combine', '{{%event_combine_item}}', 'event_combine_id', '{{%event_combine}}', 'event_combine_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%event_combine_item}}');
    }
}
