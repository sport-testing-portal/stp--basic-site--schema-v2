<?php

use yii\db\Migration;

class m180813_155605_create_table_vwTeamPlayer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTeamPlayer}}', [
            'id' => $this->integer()->notNull()->defaultValue('0'),
            'org_name' => $this->string(),
            'team_name' => $this->string(),
            'age_group_name' => $this->string(),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'team_league' => $this->string(),
            'team_league_id' => $this->integer()->defaultValue('0'),
            'team_league_name' => $this->string(),
            'team_division_id' => $this->integer()->defaultValue('0'),
            'team_division_name' => $this->string(),
            'organizational_level' => $this->string()->comment('National Team, Professional'),
            'team_city' => $this->string(),
            'team_state_name' => $this->string()->comment('State name with first letter capital'),
            'team_country_name' => $this->string()->defaultValue(''),
            'gender_code' => $this->string(),
            'gender_desc' => $this->string(),
            'player_name' => $this->string(),
            'team_player_id' => $this->integer()->notNull()->defaultValue('0'),
            'coach_name' => $this->string()->comment('This is not a related field. This a place to store the primary coach. The related tables will still store the related data for a coach.'),
            'coach_id' => $this->integer()->comment('This is not a related field. This a place to store the primary coach id. The related tables will still store the related data for a coach.'),
            'team_play_begin_dt' => $this->string(),
            'team_play_end_dt' => $this->string(),
            'team_play_sport_position_id' => $this->integer(),
            'team_play_sport_position2_id' => $this->integer(),
            'primary_sport_position' => $this->string()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'secondary_sport_position' => $this->string()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'org_id' => $this->integer()->defaultValue('0'),
            'team_id' => $this->integer()->notNull()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'player__person_id' => $this->integer()->defaultValue('0'),
            'team_created_by_username' => $this->string(),
            'team_updated_by_username' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTeamPlayer}}');
    }
}
