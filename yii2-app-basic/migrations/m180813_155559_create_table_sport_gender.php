<?php

use yii\db\Migration;

class m180813_155559_create_table_sport_gender extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sport_gender}}', [
            'sport_gender_id' => $this->primaryKey(),
            'sport_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_sport_gender__sport_idx', '{{%sport_gender}}', 'sport_id');
        $this->createIndex('fk_sport_gender__gender_idx', '{{%sport_gender}}', 'gender_id');
        $this->addForeignKey('fk_sport_gender__gender', '{{%sport_gender}}', 'gender_id', '{{%gender}}', 'gender_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_sport_gender__sport', '{{%sport_gender}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%sport_gender}}');
    }
}
