<?php

use yii\db\Migration;

class m180813_155558_create_table_school_sport extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_sport}}', [
            'school_sport_id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'sport_id' => $this->integer(),
            'gender_id' => $this->integer(),
            'conference_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('uk_school_sport__multikey', '{{%school_sport}}', ['school_id', 'sport_id', 'gender_id', 'conference_id'], true);
        $this->addForeignKey('fk_school_sport__school', '{{%school_sport}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_school_sport__sport', '{{%school_sport}}', 'sport_id', '{{%sport}}', 'sport_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%school_sport}}');
    }
}
