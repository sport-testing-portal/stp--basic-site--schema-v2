<?php

use yii\db\Migration;

class m180813_155558_create_table_search_criteria_template_item extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%search_criteria_template_item}}', [
            'search_criteria_template_item_id' => $this->primaryKey(),
            'search_criteria_template_id' => $this->integer()->notNull(),
            'search_criteria_id' => $this->integer()->notNull(),
            'search_criteria_default_value' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_search_criteria_template_item__search_template_idx', '{{%search_criteria_template_item}}', 'search_criteria_template_id');
        $this->createIndex('fk_search_criteria_template_item__search_criteria_idx', '{{%search_criteria_template_item}}', 'search_criteria_id');
        $this->addForeignKey('fk_search_criteria_template_item__search_criteria', '{{%search_criteria_template_item}}', 'search_criteria_id', '{{%search_criteria}}', 'search_criteria_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_search_criteria_template_item__search_template', '{{%search_criteria_template_item}}', 'search_criteria_template_id', '{{%search_criteria_template}}', 'search_criteria_template_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%search_criteria_template_item}}');
    }
}
