<?php

use yii\db\Migration;

class m180813_155554_create_table_payment_type_allocation_template extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_type_allocation_template}}', [
            'payment_type_allocation_template_id' => $this->primaryKey(),
            'payment_type_allocation_template_name' => $this->string(),
            'payment_type_allocation_template_desc_short' => $this->string(),
            'payment_type_allocation_template_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%payment_type_allocation_template}}');
    }
}
