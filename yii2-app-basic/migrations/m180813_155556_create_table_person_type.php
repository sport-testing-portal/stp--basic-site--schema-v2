<?php

use yii\db\Migration;

class m180813_155556_create_table_person_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%person_type}}', [
            'person_type_id' => $this->primaryKey(),
            'person_type_name' => $this->string()->notNull()->comment('player, coach, camp contact, school contact,'),
            'person_type_desc_short' => $this->string(),
            'person_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('person_type__person_type_name', '{{%person_type}}', 'person_type_name', true);
    }

    public function down()
    {
        $this->dropTable('{{%person_type}}');
    }
}
