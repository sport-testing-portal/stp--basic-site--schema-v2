<?php

use yii\db\Migration;

class m180813_155551_create_table_note extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%note}}', [
            'note_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'person_id' => $this->integer(),
            'note_type_id' => $this->integer(),
            'note' => $this->string(),
            'note_tags' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('fk_note__note_type_idx', '{{%note}}', 'note_type_id');
        $this->createIndex('fk_note__person_idx', '{{%note}}', 'person_id');
        $this->createIndex('fk_note__org_idx', '{{%note}}', 'org_id');
        $this->addForeignKey('fk_note__note_type', '{{%note}}', 'note_type_id', '{{%note_type}}', 'note_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_note__org', '{{%note}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_note__person', '{{%note}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%note}}');
    }
}
