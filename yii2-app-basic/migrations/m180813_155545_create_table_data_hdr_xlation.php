<?php

use yii\db\Migration;

class m180813_155545_create_table_data_hdr_xlation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%data_hdr_xlation}}', [
            'data_hdr_xlation_id' => $this->primaryKey(),
            'data_hdr_xlation_name' => $this->string(),
            'data_hdr_entity_name' => $this->string()->comment('This should be in another parent table.'),
            'hdr_sha1_hash' => $this->string(),
            'data_hdr_regex' => $this->string()->comment('Used to identify variable text headers to trash'),
            'data_hdr_is_trash_yn' => $this->char(),
            'gsm_target_table_name' => $this->string(),
            'data_hdr_entity_fetch_url' => $this->string()->comment('This should be in another parent table.'),
            'data_hdr_xlation_visual_example' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%data_hdr_xlation}}');
    }
}
