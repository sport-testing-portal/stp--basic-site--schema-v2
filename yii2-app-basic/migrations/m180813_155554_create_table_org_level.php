<?php

use yii\db\Migration;

class m180813_155554_create_table_org_level extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%org_level}}', [
            'org_level_id' => $this->primaryKey(),
            'org_type_id' => $this->integer(),
            'org_level_name' => $this->string(),
            'org_level_desc_short' => $this->string(),
            'org_level_desc_long' => $this->string(),
            'org_level_display_order' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_org_level__org_type_idx', '{{%org_level}}', 'org_type_id');
        $this->addForeignKey('fk_org_level__org_type', '{{%org_level}}', 'org_type_id', '{{%org_type}}', 'org_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%org_level}}');
    }
}
