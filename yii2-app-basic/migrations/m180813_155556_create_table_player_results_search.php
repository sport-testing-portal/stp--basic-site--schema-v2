<?php

use yii\db\Migration;

class m180813_155556_create_table_player_results_search extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%player_results_search}}', [
            'player_results_search_id' => $this->primaryKey(),
            'gender' => $this->string(),
            'age_range_min' => $this->integer(),
            'age_range_max' => $this->integer(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'test_provider' => $this->string(),
            'team' => $this->string(),
            'position' => $this->string(),
            'test_type' => $this->string(),
            'test_category' => $this->string(),
            'test_description' => $this->string(),
            'country' => $this->string(),
            'state_province' => $this->string(),
            'zip_code' => $this->string(),
            'zip_code_radius' => $this->string(),
            'city' => $this->string(),
            'city_radius' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
            'person_id' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%player_results_search}}');
    }
}
