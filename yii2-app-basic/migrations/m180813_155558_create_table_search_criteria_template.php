<?php

use yii\db\Migration;

class m180813_155558_create_table_search_criteria_template extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%search_criteria_template}}', [
            'search_criteria_template_id' => $this->primaryKey(),
            'person_id' => $this->integer(),
            'template_name' => $this->string(),
            'template_comment' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_search_criteria_template__person_idx', '{{%search_criteria_template}}', 'person_id');
        $this->addForeignKey('fk_search_criteria_template__person', '{{%search_criteria_template}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%search_criteria_template}}');
    }
}
