<?php

use yii\db\Migration;

class m180813_155548_create_table_height extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%height}}', [
            'height_id' => $this->primaryKey(),
            'english_long' => $this->string(),
            'english_short' => $this->string(),
            'in' => $this->integer(),
            'cm' => $this->integer(),
            'hands' => $this->integer()->comment('As in horses. Illustrates that any measure of height can be stored in this table'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%height}}');
    }
}
