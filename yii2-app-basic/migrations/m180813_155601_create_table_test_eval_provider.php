<?php

use yii\db\Migration;

class m180813_155601_create_table_test_eval_provider extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_eval_provider}}', [
            'test_eval_provider_id' => $this->primaryKey(),
            'test_eval_provider_name' => $this->string(),
            'test_eval_provider_desc_short' => $this->string(),
            'test_eval_provider_desc_long' => $this->string(),
            'test_eval_provider_website_url' => $this->string(),
            'test_eval_provider_logo_url' => $this->string(),
            'effective_begin_dt' => $this->dateTime(),
            'effective_end_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%test_eval_provider}}');
    }
}
