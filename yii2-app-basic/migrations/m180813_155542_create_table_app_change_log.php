<?php

use yii\db\Migration;

class m180813_155542_create_table_app_change_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%app_change_log}}', [
            'app_change_log_id' => $this->primaryKey(),
            'app_semantic_version' => $this->string()->comment('The new version number that  includes the described modifications'),
            'app_change_desc' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%app_change_log}}');
    }
}
