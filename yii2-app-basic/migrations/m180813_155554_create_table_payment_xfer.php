<?php

use yii\db\Migration;

class m180813_155554_create_table_payment_xfer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_xfer}}', [
            'payment_xfer_id' => $this->primaryKey(),
            'payment_log_id' => $this->integer(),
            'payment_xfer_entity_id' => $this->integer(),
            'payment_xfer_status_code_char' => $this->string(),
            'payment_xfer_status_code_num' => $this->integer(),
            'payment_xfer_sent_dt' => $this->dateTime(),
            'payment_xfer_recd_status_dt' => $this->dateTime(),
            'payment_xfer_blob_uri' => $this->string(),
            'payment_xfer_blob_format' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_payment_xfer__payment_log_idx', '{{%payment_xfer}}', 'payment_log_id');
        $this->createIndex('fk_payment_xfer__payment_xfer_entity_idx', '{{%payment_xfer}}', 'payment_xfer_entity_id');
        $this->addForeignKey('fk_payment_xfer__payment_log', '{{%payment_xfer}}', 'payment_log_id', '{{%payment_log}}', 'payment_log_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_payment_xfer__payment_xfer_entity', '{{%payment_xfer}}', 'payment_xfer_entity_id', '{{%payment_xfer_entity}}', 'payment_xfer_entity_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%payment_xfer}}');
    }
}
