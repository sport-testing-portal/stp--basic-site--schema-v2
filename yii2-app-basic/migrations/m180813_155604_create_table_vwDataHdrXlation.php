<?php

use yii\db\Migration;

class m180813_155604_create_table_vwDataHdrXlation extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwDataHdrXlation}}', [
            'dhx_id' => $this->integer()->notNull()->defaultValue('0'),
            'dhxi_id' => $this->integer()->defaultValue('0'),
            'source_entity' => $this->string()->comment('This should be in another parent table.'),
            'source_value' => $this->string()->comment('The column header of the 3rd party'),
            'target_value' => $this->string()->comment('The xxlrater column header of the 3rd party'),
            'col_ord' => $this->integer(),
            'target_table' => $this->string(),
            'type_name_source' => $this->string()->comment('table name containing the type_name value and row id'),
            'type_name' => $this->smallInteger()->comment('Source side column value links to a target side integer foreign key via a "type name" value. '),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwDataHdrXlation}}');
    }
}
