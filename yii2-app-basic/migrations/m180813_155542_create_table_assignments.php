<?php

use yii\db\Migration;

class m180813_155542_create_table_assignments extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%assignments}}', [
            'itemname' => $this->string()->notNull(),
            'userid' => $this->string()->notNull(),
            'bizrule' => $this->text(),
            'data' => $this->text(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARYKEY', '{{%assignments}}', ['itemname', 'userid']);
        $this->createIndex('itemname_idx', '{{%assignments}}', 'itemname');
        $this->createIndex('assignments_idx_userid', '{{%assignments}}', 'userid');
        $this->addForeignKey('assignments_ibfk_1', '{{%assignments}}', 'itemname', '{{%items}}', 'name', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%assignments}}');
    }
}
