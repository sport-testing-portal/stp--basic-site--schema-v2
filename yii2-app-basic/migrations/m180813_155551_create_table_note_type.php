<?php

use yii\db\Migration;

class m180813_155551_create_table_note_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%note_type}}', [
            'note_type_id' => $this->primaryKey(),
            'note_type_name' => $this->string()->notNull(),
            'display_order' => $this->integer(),
            'note_type_desc_short' => $this->string(),
            'note_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('note_type__note_type_name', '{{%note_type}}', 'note_type_name', true);
    }

    public function down()
    {
        $this->dropTable('{{%note_type}}');
    }
}
