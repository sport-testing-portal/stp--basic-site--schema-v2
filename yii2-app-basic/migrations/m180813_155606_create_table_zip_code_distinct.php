<?php

use yii\db\Migration;

class m180813_155606_create_table_zip_code_distinct extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%zip_code_distinct}}', [
            'zip_code_id' => $this->primaryKey(),
            'country_code_iso2' => $this->char(),
            'postal_code' => $this->string(),
            'place_name' => $this->string(),
            'state_name' => $this->string(),
            'state_code' => $this->string(),
            'county_name' => $this->string(),
            'county_code' => $this->string(),
            'community_name' => $this->string(),
            'community_code' => $this->string(),
            'latitude' => $this->string(),
            'longitude' => $this->string(),
            'accuracy' => $this->smallInteger(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('zip_code_distinct_idx_latitude', '{{%zip_code_distinct}}', 'latitude');
        $this->createIndex('zip_code_distinct_idx_postal_code', '{{%zip_code_distinct}}', 'postal_code');
        $this->createIndex('zip_code_distinct_idx_longitude', '{{%zip_code_distinct}}', 'longitude');
        $this->createIndex('zip_code_distinct_idx_place_name', '{{%zip_code_distinct}}', 'place_name');
    }

    public function down()
    {
        $this->dropTable('{{%zip_code_distinct}}');
    }
}
