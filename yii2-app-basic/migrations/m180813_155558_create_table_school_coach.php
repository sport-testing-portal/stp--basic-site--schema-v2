<?php

use yii\db\Migration;

class m180813_155558_create_table_school_coach extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%school_coach}}', [
            'school_coach_id' => $this->primaryKey(),
            'school_id' => $this->integer()->notNull(),
            'coach_id' => $this->integer()->notNull(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_school_coach__coach', '{{%school_coach}}', 'coach_id', '{{%coach}}', 'coach_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_school_coach__school', '{{%school_coach}}', 'school_id', '{{%school}}', 'school_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%school_coach}}');
    }
}
