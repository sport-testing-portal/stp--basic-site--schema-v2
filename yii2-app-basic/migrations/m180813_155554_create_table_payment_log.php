<?php

use yii\db\Migration;

class m180813_155554_create_table_payment_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_log}}', [
            'payment_log_id' => $this->primaryKey(),
            'payment_type_id' => $this->integer(),
            'data_entity_id' => $this->integer(),
            'subscription_id' => $this->integer(),
            'org_id' => $this->integer(),
            'person_id' => $this->integer(),
            'payment_amt' => $this->decimal(),
            'payment_dt' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_payment_log__person_idx', '{{%payment_log}}', 'person_id');
        $this->createIndex('fk_payment_log__org_idx', '{{%payment_log}}', 'org_id');
        $this->addForeignKey('fk_payment_log__org', '{{%payment_log}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_payment_log__payment_type', '{{%payment_log}}', 'payment_type_id', '{{%payment_type}}', 'payment_type_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_payment_log__person', '{{%payment_log}}', 'person_id', '{{%person}}', 'person_id', 'NO ACTION', 'NO ACTION');
        $this->addForeignKey('fk_payment_log__subscription', '{{%payment_log}}', 'subscription_id', '{{%subscription}}', 'subscription_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%payment_log}}');
    }
}
