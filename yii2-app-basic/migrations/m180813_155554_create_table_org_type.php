<?php

use yii\db\Migration;

class m180813_155554_create_table_org_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%org_type}}', [
            'org_type_id' => $this->primaryKey(),
            'org_type_name' => $this->string()->notNull()->comment('school, camp, club, vendor, venue, media, '),
            'org_type_sub_type' => $this->string(),
            'org_type_desc_short' => $this->string(),
            'org_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('org_type__org_type_name', '{{%org_type}}', 'org_type_name', true);
    }

    public function down()
    {
        $this->dropTable('{{%org_type}}');
    }
}
