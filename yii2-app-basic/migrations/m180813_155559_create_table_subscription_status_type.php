<?php

use yii\db\Migration;

class m180813_155559_create_table_subscription_status_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%subscription_status_type}}', [
            'subscription_status_type_id' => $this->primaryKey(),
            'subscription_status_type_name' => $this->string()->notNull(),
            'subscription_status_type_desc_short' => $this->string()->comment('Consider renaming this to sst_desc_short'),
            'subscription_status_type_desc_long' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('subscription_status_type__subscription_status_type_name', '{{%subscription_status_type}}', 'subscription_status_type_name');
    }

    public function down()
    {
        $this->dropTable('{{%subscription_status_type}}');
    }
}
