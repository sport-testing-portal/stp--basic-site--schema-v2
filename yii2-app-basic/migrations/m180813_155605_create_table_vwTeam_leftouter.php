<?php

use yii\db\Migration;

class m180813_155605_create_table_vwTeam_leftouter extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTeam_leftouter}}', [
            'org_name' => $this->string(),
            'org_type_name' => $this->string()->comment('school, camp, club, vendor, venue, media, '),
            'team_name' => $this->string(),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'organizational_level' => $this->string()->comment('National Team, Professional'),
            'team_governing_body' => $this->string(),
            'team_website_url' => $this->string()->comment('This is used when org_website_url is used for the club website'),
            'team_schedule_url' => $this->string()->comment('A link to a remote website'),
            'team_schedule_uri' => $this->string()->comment('A link to a file uploaded our site'),
            'team_statistical_highlights' => $this->string()->comment('a free form text field'),
            'gender_code' => $this->string(),
            'gender_desc' => $this->string(),
            'coach_name' => $this->string(),
            'player_name' => $this->string(),
            'gsm_sport_name' => $this->string(),
            'org_id' => $this->integer()->defaultValue('0'),
            'org_type_id' => $this->integer(),
            'team_id' => $this->integer()->notNull()->defaultValue('0'),
            'coach_id' => $this->integer()->defaultValue('0'),
            'coach__person_name_full' => $this->string(),
            'coach__person_email_work' => $this->string(),
            'coach__person_id' => $this->integer()->defaultValue('0'),
            'coach__created_by' => $this->integer(),
            'player_id' => $this->integer()->defaultValue('0'),
            'player__created_by' => $this->integer(),
            'team_player_id' => $this->integer()->defaultValue('0'),
            'team_player__created_by' => $this->integer(),
            'team_coach_id' => $this->integer()->defaultValue('0'),
            'team_coach__created_by' => $this->integer(),
            'player__person_id' => $this->integer()->defaultValue('0'),
            'player__person_org_id' => $this->integer(),
            'team_coach_player_composite_key' => $this->string(),
            'team_created_by_username' => $this->string(),
            'team_updated_by_username' => $this->string(),
            'team_updated_by_user_flname' => $this->string(),
            'team_updated_by_user_email' => $this->string(),
            'team__created_by' => $this->integer(),
            'team__updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'team__created_at' => $this->dateTime(),
            'team__updated_at' => $this->timestamp(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTeam_leftouter}}');
    }
}
