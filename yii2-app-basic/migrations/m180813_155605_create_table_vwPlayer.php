<?php

use yii\db\Migration;

class m180813_155605_create_table_vwPlayer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwPlayer}}', [
            'person_name_full' => $this->string(),
            'person_age' => $this->double(),
            'team_name' => $this->string(),
            'age_group_name' => $this->string(),
            'person_city' => $this->string(),
            'person_state_or_region' => $this->string(),
            'gsm_staff' => $this->string(),
            'player_id' => $this->integer()->notNull()->defaultValue('0'),
            'person_id' => $this->integer(),
            'player_age_group_id' => $this->integer()->comment('Used to select appropriate age appropriate teams and set default age_group_id for inesrted team records'),
            'player_team_player_id' => $this->integer(),
            'player_sport_position_id' => $this->integer(),
            'player_sport_position2_id' => $this->integer(),
            'player_sport_position_name' => $this->string()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'player_access_code' => $this->string(),
            'player_waiver_minor_dt' => $this->dateTime(),
            'player_waiver_adult_dt' => $this->dateTime(),
            'player_parent_email' => $this->string(),
            'player_sport_preference' => $this->string(),
            'player_sport_position_preference' => $this->string(),
            'player_shot_side_preference' => $this->string(),
            'player_dominant_side' => $this->string(),
            'player_dominant_foot' => $this->string(),
            'player_statistical_highlights' => $this->string(),
            'org_id' => $this->integer(),
            'app_user_id' => $this->integer(),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_dob_eng' => $this->string(),
            'person_bday_long' => $this->string(),
            'person_height' => $this->string()->comment('height is stored in inches'),
            'person_height_imperial' => $this->string(),
            'person_weight' => $this->integer()->comment('weight is stored in pounds'),
            'person_bmi' => $this->double(),
            'person_tshirt_size' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->char(),
            'person_profile_url' => $this->string()->comment('This is a link to a remote website'),
            'person_profile_uri' => $this->string()->comment('This is a link to a file uploaded onto our own website'),
            'person_high_school__graduation_year' => $this->integer()->comment('This will typically be used with players and contain a future value'),
            'person_college_graduation_year' => $this->integer(),
            'person_college_commitment_status' => $this->string(),
            'person_type_id' => $this->integer()->notNull()->defaultValue('0'),
            'person_type_name' => $this->string()->notNull()->comment('player, coach, camp contact, school contact,'),
            'person_type_desc_short' => $this->string(),
            'person_type_desc_long' => $this->string(),
            'gender_id' => $this->integer()->defaultValue('0'),
            'gender_desc' => $this->string(),
            'gender_code' => $this->string(),
            'org_type_id' => $this->integer(),
            'org_name' => $this->string(),
            'org_type_name' => $this->string()->comment('school, camp, club, vendor, venue, media, '),
            'org_website_url' => $this->string(),
            'org_twitter_url' => $this->string(),
            'org_facebook_url' => $this->string(),
            'org_phone_main' => $this->string(),
            'org_email_main' => $this->string(),
            'org_addr1' => $this->string(),
            'org_addr2' => $this->string(),
            'org_addr3' => $this->string(),
            'org_city' => $this->string(),
            'org_state_or_region' => $this->string(),
            'org_postal_code' => $this->string(),
            'org_country_code_iso3' => $this->string(),
            'team_org_org_type_id' => $this->integer(),
            'team_org_org_type_name' => $this->string()->comment('school, camp, club, vendor, venue, media, '),
            'team_org_org_name' => $this->string(),
            'team_org_org_website_url' => $this->string(),
            'team_org_org_twitter_url' => $this->string(),
            'team_org_org_facebook_url' => $this->string(),
            'team_org_org_phone_main' => $this->string(),
            'team_org_org_email_main' => $this->string(),
            'team_org_org_addr1' => $this->string(),
            'team_org_org_addr2' => $this->string(),
            'team_org_org_addr3' => $this->string(),
            'team_org_org_city' => $this->string(),
            'team_org_org_state_or_region' => $this->string(),
            'team_org_org_postal_code' => $this->string(),
            'team_org_org_country_code_iso3' => $this->string(),
            'team_org_org_governing_body' => $this->string(),
            'team_org_org_ncaa_clearing_house_id' => $this->string(),
            'team_player_id' => $this->integer()->defaultValue('0'),
            'team_id' => $this->integer(),
            'team_play_sport_position_id' => $this->integer(),
            'team_play_sport_position2_id' => $this->integer(),
            'team_play_sport_position_name' => $this->string()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'team_play_sport_position2_name' => $this->string()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'team_play_primary_position_flattext' => $this->string(),
            'team_play_primary_position' => $this->string()->comment('Forward, Midfielder, Defender, Goalkeeper, etc'),
            'team_play_begin_dt' => $this->dateTime(),
            'team_play_end_dt' => $this->dateTime(),
            'team_play_coach_id' => $this->integer()->comment('This is not a related field. This a place to store the primary coach id. The related tables will still store the related data for a coach.'),
            'team_play_coach_name' => $this->string()->comment('This is not a related field. This a place to store the primary coach. The related tables will still store the related data for a coach.'),
            'team_play_current_team' => $this->tinyInteger(),
            'team_play_statistical_highlights' => $this->string(),
            'team_play_created_at' => $this->dateTime(),
            'team_play_updated_at' => $this->timestamp(),
            'team_org_id' => $this->integer(),
            'team_school_id' => $this->integer(),
            'team_sport_id' => $this->integer(),
            'team_camp_id' => $this->integer(),
            'team_gender_id' => $this->integer(),
            'team_gender' => $this->char(),
            'team_age_group_id' => $this->integer(),
            'team_age_group' => $this->string()->comment('use this for a delimited string of age_group_names eg multiple age_groups per team'),
            'team_competition_season_id' => $this->integer()->comment('used for the primary season, eg a single link to the season table
'),
            'team_competition_season' => $this->string()->comment('used for tags, eg multiple seasons'),
            'team_league_id' => $this->integer(),
            'team_league' => $this->string(),
            'organizational_level' => $this->string()->comment('National Team, Professional'),
            'team_governing_body' => $this->string(),
            'team_website_url' => $this->string()->comment('This is used when org_website_url is used for the club website'),
            'team_schedule_url' => $this->string()->comment('A link to a remote website'),
            'team_schedule_uri' => $this->string()->comment('A link to a file uploaded our site'),
            'team_statistical_highlights' => $this->string()->comment('a free form text field'),
            'team_division_id' => $this->integer(),
            'team_division' => $this->string()->comment('The division the team plays in'),
            'team_city' => $this->string(),
            'team_state_id' => $this->integer(),
            'team_country_id' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwPlayer}}');
    }
}
