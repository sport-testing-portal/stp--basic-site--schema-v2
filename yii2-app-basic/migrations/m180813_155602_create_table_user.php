<?php

use yii\db\Migration;

class m180813_155602_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string(),
            'email' => $this->string()->notNull(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'activation_key' => $this->string(),
            'created_on' => $this->timestamp(),
            'updated_on' => $this->timestamp(),
            'last_visit_on' => $this->timestamp(),
            'password_set_on' => $this->timestamp(),
            'email_verified' => $this->tinyInteger()->notNull()->defaultValue('0'),
            'is_active' => $this->tinyInteger()->notNull()->defaultValue('0'),
            'is_disabled' => $this->tinyInteger()->notNull()->defaultValue('0'),
            'one_time_password_secret' => $this->string(),
            'one_time_password_code' => $this->string(),
            'one_time_password_counter' => $this->integer()->notNull()->defaultValue('0'),
            'srbac_set_on' => $this->timestamp(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_city' => $this->string(),
            'person_state_or_region' => $this->string(),
            'person_postal_code' => $this->string(),
            'gender_id' => $this->integer(),
            'person_date_of_birth' => $this->dateTime(),
            'password_hash' => $this->string()->notNull(),
            'auth_key' => $this->string()->notNull(),
            'confirmed_at' => $this->integer(),
            'unconfirmed_email' => $this->string(),
            'blocked_at' => $this->integer(),
            'registration_ip' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'flags' => $this->integer()->notNull()->defaultValue('0'),
            'last_login_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('user_email_verified_idx', '{{%user}}', 'email_verified');
        $this->createIndex('user_is_disabled_idx', '{{%user}}', 'is_disabled');
        $this->createIndex('user_unique_username', '{{%user}}', 'username', true);
        $this->createIndex('user_username_idx', '{{%user}}', 'username');
        $this->createIndex('user_is_active_idx', '{{%user}}', 'is_active');
        $this->createIndex('user_unique_email', '{{%user}}', 'email', true);
        $this->createIndex('user_email_idx', '{{%user}}', 'email');
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
