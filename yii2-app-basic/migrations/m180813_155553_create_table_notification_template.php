<?php

use yii\db\Migration;

class m180813_155553_create_table_notification_template extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notification_template}}', [
            'notification_template_id' => $this->primaryKey(),
            'notification_template_type_id' => $this->integer(),
            'notification_template_uri' => $this->string()->comment('See: http://www.ietf.org/rfc/rfc3986.txt The URI may be dereferenced using many protocols; http, https, ftp, ssh, etc.'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->addForeignKey('fk_ntt_notification_template_type', '{{%notification_template}}', 'notification_template_type_id', '{{%notification_template_type}}', 'notification_template_type_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%notification_template}}');
    }
}
