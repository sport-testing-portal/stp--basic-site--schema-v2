<?php

use yii\db\Migration;

class m180813_155602_create_table_user_profile_picture extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_profile_picture}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'original_picture_id' => $this->integer(),
            'filename' => $this->string()->notNull(),
            'width' => $this->integer()->notNull(),
            'height' => $this->integer()->notNull(),
            'mimetype' => $this->string()->notNull(),
            'created_on' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'contents' => $this->text()->notNull(),
        ], $tableOptions);

        $this->createIndex('user_profile_picture_user_id_idx', '{{%user_profile_picture}}', 'user_id');
        $this->createIndex('user_profile_picture_width_height_idx', '{{%user_profile_picture}}', ['width', 'height']);
        $this->createIndex('user_profile_picture_original_picture_id_idx', '{{%user_profile_picture}}', 'original_picture_id');
        $this->addForeignKey('fk_user_profile_picture__user', '{{%user_profile_picture}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_profile_picture__self_orginal_picture', '{{%user_profile_picture}}', 'original_picture_id', '{{%user_profile_picture}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user_profile_picture}}');
    }
}
