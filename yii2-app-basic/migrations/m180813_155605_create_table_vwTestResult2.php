<?php

use yii\db\Migration;

class m180813_155605_create_table_vwTestResult2 extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwTestResult2}}', [
            'person' => $this->string(),
            'ptype' => $this->string()->comment('player, coach, camp contact, school contact,'),
            'gender' => $this->string(),
            'test_desc' => $this->string(),
            'test_type' => $this->string()->comment('This is category#1 in Jami - lingo (the language of the domain)'),
            'split' => $this->string(),
            'score' => $this->decimal(),
            'test_units' => $this->string(),
            'trial_status' => $this->string(),
            'overall_ranking' => $this->integer(),
            'positional_ranking' => $this->integer(),
            'score_url' => $this->string(),
            'video_url' => $this->string(),
            'test_date' => $this->string(),
            'test_date_iso' => $this->dateTime(),
            'tester' => $this->string(),
            'person_id' => $this->integer()->notNull()->defaultValue('0'),
            'player_id' => $this->integer()->defaultValue('0'),
            'test_eval_summary_log_id' => $this->integer()->notNull()->defaultValue('0'),
            'test_eval_detail_log_id' => $this->integer()->defaultValue('0'),
            'source_event_id' => $this->integer(),
            'source_record_id' => $this->integer(),
            'import_file_id' => $this->integer(),
            'import_file_line_num' => $this->integer(),
            'source_file_name' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwTestResult2}}');
    }
}
