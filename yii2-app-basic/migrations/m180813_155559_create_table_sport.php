<?php

use yii\db\Migration;

class m180813_155559_create_table_sport extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%sport}}', [
            'sport_id' => $this->primaryKey(),
            'gsm_sport_name' => $this->string(),
            'sport_desc_short' => $this->string(),
            'sport_desc_long' => $this->string(),
            'high_school_yn' => $this->char(),
            'ncaa_yn' => $this->char(),
            'olympic_yn' => $this->char(),
            'xgame_sport_yn' => $this->char(),
            'ncaa_sport_name' => $this->string(),
            'olympic_sport_name' => $this->string(),
            'xgame_sport_name' => $this->string(),
            'ncaa_sport_type' => $this->string(),
            'ncaa_sport_season_male' => $this->string(),
            'ncaa_sport_season_female' => $this->string(),
            'ncaa_sport_season_coed' => $this->string(),
            'olympic_sport_season' => $this->string(),
            'xgame_sport_season' => $this->string(),
            'olympic_sport_gender' => $this->string(),
            'xgame_sport_gender' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%sport}}');
    }
}
