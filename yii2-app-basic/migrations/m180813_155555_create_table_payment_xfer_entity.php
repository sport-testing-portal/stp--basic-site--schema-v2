<?php

use yii\db\Migration;

class m180813_155555_create_table_payment_xfer_entity extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%payment_xfer_entity}}', [
            'payment_xfer_entity_id' => $this->primaryKey(),
            'payment_xfer_entity_name' => $this->string(),
            'payment_xfer_entity_client_secret' => $this->string()->comment('blowfish or aes this field'),
            'payment_xfer_entity_access_token' => $this->string()->comment('blowfish or aes this field'),
            'payment_xfer_entity_access_url' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%payment_xfer_entity}}');
    }
}
