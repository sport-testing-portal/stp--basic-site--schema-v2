<?php

use yii\db\Migration;

class m180813_155543_create_table_audit_entry extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%audit_entry}}', [
            'id' => $this->primaryKey(),
            'created' => $this->dateTime()->notNull(),
            'user_id' => $this->integer()->defaultValue('0'),
            'duration' => $this->float(),
            'ip' => $this->string(),
            'request_method' => $this->string(),
            'ajax' => $this->integer()->notNull()->defaultValue('0'),
            'route' => $this->string(),
            'memory_max' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('idx_user_id', '{{%audit_entry}}', 'user_id');
        $this->createIndex('idx_route', '{{%audit_entry}}', 'route');
    }

    public function down()
    {
        $this->dropTable('{{%audit_entry}}');
    }
}
