<?php

use yii\db\Migration;

class m180813_155604_create_table_vwCoachFlat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vwCoachFlat}}', [
            'user_id' => $this->integer()->comment('Only one registered user per person'),
            'username' => $this->string(),
            'person_id' => $this->integer()->notNull()->defaultValue('0'),
            'org_id' => $this->integer(),
            'coach_id' => $this->integer()->notNull()->defaultValue('0'),
            'app_user_id' => $this->integer(),
            'person_type_id' => $this->integer(),
            'person_type_name' => $this->string()->comment('player, coach, camp contact, school contact,'),
            'coach_type_id' => $this->integer(),
            'coach_type_name' => $this->string(),
            'coach_team_coach_id' => $this->integer()->comment('The coach\'s currently selected team. Used to drive UI.'),
            'org_name' => $this->string(),
            'org_type_id' => $this->integer(),
            'org_type_name' => $this->string()->comment('school, camp, club, vendor, venue, media, '),
            'person_name_prefix' => $this->string(),
            'person_name_first' => $this->string(),
            'person_name_middle' => $this->string(),
            'person_name_last' => $this->string(),
            'person_name_suffix' => $this->string(),
            'person_name_full' => $this->string(),
            'gender_id' => $this->integer(),
            'gender_code' => $this->string(),
            'gender_desc' => $this->string(),
            'person_phone_personal' => $this->string(),
            'person_email_personal' => $this->string(),
            'person_phone_work' => $this->string(),
            'person_email_work' => $this->string(),
            'person_position_work' => $this->string()->comment('Owner, Director, Administrative Assistant, Other'),
            'person_image_headshot_url' => $this->string()->comment('URL to a headshot photo'),
            'person_name_nickname' => $this->string(),
            'person_date_of_birth' => $this->dateTime(),
            'person_height' => $this->string()->comment('height is stored in inches'),
            'person_weight' => $this->integer()->comment('weight is stored in pounds'),
            'person_tshirt_size' => $this->string(),
            'person_high_school__graduation_year' => $this->integer()->comment('This will typically be used with players and contain a future value'),
            'person_college_graduation_year' => $this->integer(),
            'person_college_commitment_status' => $this->string(),
            'person_addr_1' => $this->string(),
            'person_addr_2' => $this->string(),
            'person_addr_3' => $this->string()->comment('Required for some institutional and international addresses'),
            'person_city' => $this->string(),
            'person_postal_code' => $this->string(),
            'person_country' => $this->string(),
            'person_country_code' => $this->char(),
            'person_state_or_region' => $this->string(),
            'org_affiliation_begin_dt' => $this->dateTime(),
            'org_affiliation_end_dt' => $this->dateTime(),
            'person_website' => $this->string(),
            'person_profile_url' => $this->string()->comment('This is a link to a remote website'),
            'person_profile_uri' => $this->string()->comment('This is a link to a file uploaded onto our own website'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp(),
            'created_by' => $this->integer(),
            'person_created_by_username' => $this->string(),
            'updated_by' => $this->integer(),
            'person_updated_by_username' => $this->string(),
            'age_group_id' => $this->integer(),
            'coach_specialty' => $this->string(),
            'coach_certifications' => $this->string(),
            'coach_comments' => $this->string(),
            'coach_qrcode_uri' => $this->string()->comment('internal URL to the coach QR code'),
            'coach_info_source_scrape_url' => $this->string()->comment('coach info programmatic source data update url'),
            'coach_created_by_username' => $this->string(),
            'coach_created_by' => $this->integer(),
            'coach_updated_by_username' => $this->string(),
            'coach_updated_by' => $this->integer(),
            'org_level_id' => $this->integer(),
            'org_level_name' => $this->string(),
            'org_governing_body' => $this->string(),
            'org_ncaa_clearing_house_id' => $this->string(),
            'org_website_url' => $this->string(),
            'org_twitter_url' => $this->string(),
            'org_facebook_url' => $this->string(),
            'org_phone_main' => $this->string(),
            'org_email_main' => $this->string(),
            'org_addr1' => $this->string(),
            'org_addr2' => $this->string(),
            'org_addr3' => $this->string(),
            'org_city' => $this->string(),
            'org_state_or_region' => $this->string(),
            'org_postal_code' => $this->string(),
            'org_country_code_iso3' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%vwCoachFlat}}');
    }
}
