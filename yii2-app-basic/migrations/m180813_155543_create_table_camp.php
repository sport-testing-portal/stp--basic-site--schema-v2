<?php

use yii\db\Migration;

class m180813_155543_create_table_camp extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%camp}}', [
            'camp_id' => $this->primaryKey(),
            'org_id' => $this->integer(),
            'camp_name' => $this->string()->comment('if different than org name'),
            'camp_specialty' => $this->string()->comment('Recruiting/College ID, Attack, Goalkeepers,Team, Performance'),
            'camp_cost_regular' => $this->integer(),
            'camp_cost_early_registration' => $this->integer(),
            'camp_team_discounts_available_yn' => $this->char(),
            'camp_scholarships_available_yn' => $this->char(),
            'camp_session_desc' => $this->string(),
            'camp_website_url' => $this->string(),
            'camp_session_url' => $this->string()->comment('todo - this needs to be moved to camp session'),
            'camp_registration_url' => $this->string(),
            'camp_twitter_url' => $this->string(),
            'camp_facebook_url' => $this->string(),
            'camp_scholarship_application_info' => $this->string(),
            'camp_scholarship_application_request' => $this->string(),
            'camp_organizer_description' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'lock' => $this->tinyInteger()->defaultValue('0'),
        ], $tableOptions);

        $this->createIndex('fk_camp__org_idx', '{{%camp}}', 'org_id');
        $this->addForeignKey('fk_camp__org', '{{%camp}}', 'org_id', '{{%org}}', 'org_id', 'NO ACTION', 'NO ACTION');
    }

    public function down()
    {
        $this->dropTable('{{%camp}}');
    }
}
