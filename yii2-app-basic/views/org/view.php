<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Org */

$this->title = $model->org_id;
$this->params['breadcrumbs'][] = ['label' => 'Orgs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="org-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Org'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->org_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->org_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'org_id',
        [
            'attribute' => 'orgType.org_type_id',
            'label' => 'Org Type',
        ],
        [
            'attribute' => 'orgLevel.org_level_id',
            'label' => 'Org Level',
        ],
        'org_name',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url:url',
        'org_twitter_url:url',
        'org_facebook_url:url',
        'org_phone_main',
        'org_email_main:email',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerAddress->totalCount){
    $gridColumnAddress = [
        ['class' => 'yii\grid\SerialColumn'],
            'address_id',
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            [
                'attribute' => 'addressType.address_type_id',
                'label' => 'Address Type'
            ],
            'addr1',
            'addr2',
            'addr3',
            'city',
            'state_or_region',
            'postal_code',
            'country',
            'country_code',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAddress,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-address']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Address'),
        ],
        'export' => false,
        'columns' => $gridColumnAddress
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCamp->totalCount){
    $gridColumnCamp = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_id',
                        'camp_name',
            'camp_specialty',
            'camp_cost_regular',
            'camp_cost_early_registration',
            'camp_team_discounts_available_yn',
            'camp_scholarships_available_yn',
            'camp_session_desc',
            'camp_website_url:url',
            'camp_session_url:url',
            'camp_registration_url:url',
            'camp_twitter_url:url',
            'camp_facebook_url:url',
            'camp_scholarship_application_info',
            'camp_scholarship_application_request',
            'camp_organizer_description',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCamp,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp'),
        ],
        'export' => false,
        'columns' => $gridColumnCamp
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMedia->totalCount){
    $gridColumnMedia = [
        ['class' => 'yii\grid\SerialColumn'],
            'media_id',
            [
                'attribute' => 'mediaType.media_type_id',
                'label' => 'Media Type'
            ],
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            'media_name_original',
            'media_name_unique',
            'media_desc_short',
            'media_desc_long',
            'media_url_local:url',
            'media_url_remote:url',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMedia,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-media']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Media'),
        ],
        'export' => false,
        'columns' => $gridColumnMedia
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerNote->totalCount){
    $gridColumnNote = [
        ['class' => 'yii\grid\SerialColumn'],
            'note_id',
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            [
                'attribute' => 'noteType.note_type_id',
                'label' => 'Note Type'
            ],
            'note',
            'note_tags',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerNote,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Note'),
        ],
        'export' => false,
        'columns' => $gridColumnNote
    ]);
}
?>

    </div>
    <div class="row">
        <h4>OrgLevel<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrgLevel = [
        [
            'attribute' => 'orgType.org_type_id',
            'label' => 'Org Type',
        ],
        'org_level_name',
        'org_level_desc_short',
        'org_level_desc_long',
        'org_level_display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->orgLevel,
        'attributes' => $gridColumnOrgLevel    ]);
    ?>
    <div class="row">
        <h4>OrgType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrgType = [
        'org_type_name',
        'org_type_sub_type',
        'org_type_desc_short',
        'org_type_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->orgType,
        'attributes' => $gridColumnOrgType    ]);
    ?>
    
    <div class="row">
<?php
if($providerPaymentLog->totalCount){
    $gridColumnPaymentLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'payment_log_id',
            [
                'attribute' => 'paymentType.payment_type_id',
                'label' => 'Payment Type'
            ],
            'data_entity_id',
            [
                'attribute' => 'subscription.subscription_id',
                'label' => 'Subscription'
            ],
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            'payment_amt',
            'payment_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPaymentLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payment-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Payment Log'),
        ],
        'export' => false,
        'columns' => $gridColumnPaymentLog
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPerson->totalCount){
    $gridColumnPerson = [
        ['class' => 'yii\grid\SerialColumn'],
            'person_id',
                        [
                'attribute' => 'appUser.username',
                'label' => 'App User'
            ],
            [
                'attribute' => 'personType.person_type_id',
                'label' => 'Person Type'
            ],
            'user_id',
            'person_name_prefix',
            'person_name_first',
            'person_name_middle',
            'person_name_last',
            'person_name_suffix',
            'person_name_full',
            'person_phone_personal',
            'person_email_personal:email',
            'person_phone_work',
            'person_email_work:email',
            'person_position_work',
            [
                'attribute' => 'gender.gender_id',
                'label' => 'Gender'
            ],
            'person_image_headshot_url:url',
            'person_name_nickname',
            'person_date_of_birth',
            'person_height',
            'person_weight',
            'person_tshirt_size',
            'person_high_school__graduation_year',
            'person_college_graduation_year',
            'person_college_commitment_status',
            'person_addr_1',
            'person_addr_2',
            'person_addr_3',
            'person_city',
            'person_postal_code',
            'person_country',
            [
                'attribute' => 'personCountryCode.country_id',
                'label' => 'Person Country Code'
            ],
            'person_state_or_region',
            'org_affiliation_begin_dt',
            'org_affiliation_end_dt',
            'person_website',
            'person_profile_url:url',
            'person_profile_uri',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPerson,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-person']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Person'),
        ],
        'export' => false,
        'columns' => $gridColumnPerson
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchool->totalCount){
    $gridColumnSchool = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_id',
                        [
                'attribute' => 'religiousAffiliation.religious_affiliation',
                'label' => 'Religious Affiliation'
            ],
            'conference_id__female',
            'conference_id__male',
            'conference_id__main',
            'school_ipeds_id',
            'school_unit_id',
            'school_ope_id',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchool,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School'),
        ],
        'export' => false,
        'columns' => $gridColumnSchool
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSubscription->totalCount){
    $gridColumnSubscription = [
        ['class' => 'yii\grid\SerialColumn'],
            'subscription_id',
            [
                'attribute' => 'subscriptionType.subscription_type_id',
                'label' => 'Subscription Type'
            ],
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            [
                'attribute' => 'subscriptionStatusType.subscription_status_type_id',
                'label' => 'Subscription Status Type'
            ],
            'subscription_begin_dt',
            'subscription_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSubscription,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-subscription']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Subscription'),
        ],
        'export' => false,
        'columns' => $gridColumnSubscription
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeam->totalCount){
    $gridColumnTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_id',
                        [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
            [
                'attribute' => 'sport.sport_id',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'camp.camp_id',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'gender.gender_id',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
            'team_name',
            'team_gender',
            [
                'attribute' => 'teamDivision.team_division_id',
                'label' => 'Team Division'
            ],
            [
                'attribute' => 'teamLeague.team_league_id',
                'label' => 'Team League'
            ],
            'team_division',
            'team_league',
            'organizational_level',
            'team_governing_body',
            'team_age_group',
            'team_website_url:url',
            'team_schedule_url:url',
            'team_schedule_uri',
            'team_statistical_highlights',
            'team_competition_season_id',
            'team_competition_season',
            'team_city',
            'team_state_id',
            [
                'attribute' => 'teamCountry.country_id',
                'label' => 'Team Country'
            ],
            'team_wins',
            'team_losses',
            'team_draws',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team'),
        ],
        'export' => false,
        'columns' => $gridColumnTeam
    ]);
}
?>

    </div>
</div>
