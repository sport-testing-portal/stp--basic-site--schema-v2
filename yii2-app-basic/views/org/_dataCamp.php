<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->camps,
        'key' => 'camp_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'camp_id',
        'camp_name',
        'camp_specialty',
        'camp_cost_regular',
        'camp_cost_early_registration',
        'camp_team_discounts_available_yn',
        'camp_scholarships_available_yn',
        'camp_session_desc',
        'camp_website_url:url',
        'camp_session_url:url',
        'camp_registration_url:url',
        'camp_twitter_url:url',
        'camp_facebook_url:url',
        'camp_scholarship_application_info',
        'camp_scholarship_application_request',
        'camp_organizer_description',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'camp'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
