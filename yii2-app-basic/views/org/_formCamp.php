<div class="form-group" id="add-camp">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Camp',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'camp_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'camp_name' => ['type' => TabularForm::INPUT_TEXT],
        'camp_specialty' => ['type' => TabularForm::INPUT_TEXT],
        'camp_cost_regular' => ['type' => TabularForm::INPUT_TEXT],
        'camp_cost_early_registration' => ['type' => TabularForm::INPUT_TEXT],
        'camp_team_discounts_available_yn' => ['type' => TabularForm::INPUT_TEXT],
        'camp_scholarships_available_yn' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_desc' => ['type' => TabularForm::INPUT_TEXT],
        'camp_website_url' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_url' => ['type' => TabularForm::INPUT_TEXT],
        'camp_registration_url' => ['type' => TabularForm::INPUT_TEXT],
        'camp_twitter_url' => ['type' => TabularForm::INPUT_TEXT],
        'camp_facebook_url' => ['type' => TabularForm::INPUT_TEXT],
        'camp_scholarship_application_info' => ['type' => TabularForm::INPUT_TEXT],
        'camp_scholarship_application_request' => ['type' => TabularForm::INPUT_TEXT],
        'camp_organizer_description' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowCamp(' . $key . '); return false;', 'id' => 'camp-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Camp', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowCamp()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

