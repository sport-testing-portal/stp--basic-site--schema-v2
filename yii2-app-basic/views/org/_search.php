<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrgSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-org-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'org_id')->textInput(['placeholder' => 'Org']) ?>

    <?= $form->field($model, 'org_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\OrgType::find()->orderBy('org_type_id')->asArray()->all(), 'org_type_id', 'org_type_id'),
        'options' => ['placeholder' => 'Choose Org type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'org_level_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\OrgLevel::find()->orderBy('org_level_id')->asArray()->all(), 'org_level_id', 'org_level_id'),
        'options' => ['placeholder' => 'Choose Org level'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'org_name')->textInput(['maxlength' => true, 'placeholder' => 'Org Name']) ?>

    <?= $form->field($model, 'org_governing_body')->textInput(['maxlength' => true, 'placeholder' => 'Org Governing Body']) ?>

    <?php /* echo $form->field($model, 'org_ncaa_clearing_house_id')->textInput(['maxlength' => true, 'placeholder' => 'Org Ncaa Clearing House']) */ ?>

    <?php /* echo $form->field($model, 'org_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Website Url']) */ ?>

    <?php /* echo $form->field($model, 'org_twitter_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Twitter Url']) */ ?>

    <?php /* echo $form->field($model, 'org_facebook_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Facebook Url']) */ ?>

    <?php /* echo $form->field($model, 'org_phone_main')->textInput(['maxlength' => true, 'placeholder' => 'Org Phone Main']) */ ?>

    <?php /* echo $form->field($model, 'org_email_main')->textInput(['maxlength' => true, 'placeholder' => 'Org Email Main']) */ ?>

    <?php /* echo $form->field($model, 'org_addr1')->textInput(['maxlength' => true, 'placeholder' => 'Org Addr1']) */ ?>

    <?php /* echo $form->field($model, 'org_addr2')->textInput(['maxlength' => true, 'placeholder' => 'Org Addr2']) */ ?>

    <?php /* echo $form->field($model, 'org_addr3')->textInput(['maxlength' => true, 'placeholder' => 'Org Addr3']) */ ?>

    <?php /* echo $form->field($model, 'org_city')->textInput(['maxlength' => true, 'placeholder' => 'Org City']) */ ?>

    <?php /* echo $form->field($model, 'org_state_or_region')->textInput(['maxlength' => true, 'placeholder' => 'Org State Or Region']) */ ?>

    <?php /* echo $form->field($model, 'org_postal_code')->textInput(['maxlength' => true, 'placeholder' => 'Org Postal Code']) */ ?>

    <?php /* echo $form->field($model, 'org_country_code_iso3')->textInput(['maxlength' => true, 'placeholder' => 'Org Country Code Iso3']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
