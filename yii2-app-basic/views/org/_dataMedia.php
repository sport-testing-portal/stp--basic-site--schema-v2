<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->media,
        'key' => 'media_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'media_id',
        [
                'attribute' => 'mediaType.media_type_id',
                'label' => 'Media Type'
            ],
        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
        'media_name_original',
        'media_name_unique',
        'media_desc_short',
        'media_desc_long',
        'media_url_local:url',
        'media_url_remote:url',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'media'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
