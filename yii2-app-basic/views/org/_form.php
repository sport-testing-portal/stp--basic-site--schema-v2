<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Org */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Address', 
        'relID' => 'address', 
        'value' => \yii\helpers\Json::encode($model->addresses),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Camp', 
        'relID' => 'camp', 
        'value' => \yii\helpers\Json::encode($model->camps),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Media', 
        'relID' => 'media', 
        'value' => \yii\helpers\Json::encode($model->media),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Note', 
        'relID' => 'note', 
        'value' => \yii\helpers\Json::encode($model->notes),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'PaymentLog', 
        'relID' => 'payment-log', 
        'value' => \yii\helpers\Json::encode($model->paymentLogs),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Person', 
        'relID' => 'person', 
        'value' => \yii\helpers\Json::encode($model->people),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'School', 
        'relID' => 'school', 
        'value' => \yii\helpers\Json::encode($model->schools),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Subscription', 
        'relID' => 'subscription', 
        'value' => \yii\helpers\Json::encode($model->subscriptions),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="org-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'org_id')->textInput(['placeholder' => 'Org']) ?>

    <?= $form->field($model, 'org_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\OrgType::find()->orderBy('org_type_id')->asArray()->all(), 'org_type_id', 'org_type_id'),
        'options' => ['placeholder' => 'Choose Org type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'org_level_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\OrgLevel::find()->orderBy('org_level_id')->asArray()->all(), 'org_level_id', 'org_level_id'),
        'options' => ['placeholder' => 'Choose Org level'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'org_name')->textInput(['maxlength' => true, 'placeholder' => 'Org Name']) ?>

    <?= $form->field($model, 'org_governing_body')->textInput(['maxlength' => true, 'placeholder' => 'Org Governing Body']) ?>

    <?= $form->field($model, 'org_ncaa_clearing_house_id')->textInput(['maxlength' => true, 'placeholder' => 'Org Ncaa Clearing House']) ?>

    <?= $form->field($model, 'org_website_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Website Url']) ?>

    <?= $form->field($model, 'org_twitter_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Twitter Url']) ?>

    <?= $form->field($model, 'org_facebook_url')->textInput(['maxlength' => true, 'placeholder' => 'Org Facebook Url']) ?>

    <?= $form->field($model, 'org_phone_main')->textInput(['maxlength' => true, 'placeholder' => 'Org Phone Main']) ?>

    <?= $form->field($model, 'org_email_main')->textInput(['maxlength' => true, 'placeholder' => 'Org Email Main']) ?>

    <?= $form->field($model, 'org_addr1')->textInput(['maxlength' => true, 'placeholder' => 'Org Addr1']) ?>

    <?= $form->field($model, 'org_addr2')->textInput(['maxlength' => true, 'placeholder' => 'Org Addr2']) ?>

    <?= $form->field($model, 'org_addr3')->textInput(['maxlength' => true, 'placeholder' => 'Org Addr3']) ?>

    <?= $form->field($model, 'org_city')->textInput(['maxlength' => true, 'placeholder' => 'Org City']) ?>

    <?= $form->field($model, 'org_state_or_region')->textInput(['maxlength' => true, 'placeholder' => 'Org State Or Region']) ?>

    <?= $form->field($model, 'org_postal_code')->textInput(['maxlength' => true, 'placeholder' => 'Org Postal Code']) ?>

    <?= $form->field($model, 'org_country_code_iso3')->textInput(['maxlength' => true, 'placeholder' => 'Org Country Code Iso3']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Address'),
            'content' => $this->render('_formAddress', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->addresses),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Camp'),
            'content' => $this->render('_formCamp', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->camps),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Media'),
            'content' => $this->render('_formMedia', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->media),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Note'),
            'content' => $this->render('_formNote', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->notes),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('PaymentLog'),
            'content' => $this->render('_formPaymentLog', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->paymentLogs),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Person'),
            'content' => $this->render('_formPerson', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->people),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('School'),
            'content' => $this->render('_formSchool', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->schools),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Subscription'),
            'content' => $this->render('_formSubscription', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->subscriptions),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team'),
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
