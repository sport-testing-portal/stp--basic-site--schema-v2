<div class="form-group" id="add-payment-log">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'PaymentLog',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'payment_log_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'payment_type_id' => [
            'label' => 'Payment type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\PaymentType::find()->orderBy('payment_type_id')->asArray()->all(), 'payment_type_id', 'payment_type_id'),
                'options' => ['placeholder' => 'Choose Payment type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'data_entity_id' => ['type' => TabularForm::INPUT_TEXT],
        'subscription_id' => [
            'label' => 'Subscription',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Subscription::find()->orderBy('subscription_id')->asArray()->all(), 'subscription_id', 'subscription_id'),
                'options' => ['placeholder' => 'Choose Subscription'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'person_id' => [
            'label' => 'Person',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person_id')->asArray()->all(), 'person_id', 'person_id'),
                'options' => ['placeholder' => 'Choose Person'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'payment_amt' => ['type' => TabularForm::INPUT_TEXT],
        'payment_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Payment Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPaymentLog(' . $key . '); return false;', 'id' => 'payment-log-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Payment Log', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPaymentLog()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

