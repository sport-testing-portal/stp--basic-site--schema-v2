<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->subscriptions,
        'key' => 'subscription_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'subscription_id',
        [
                'attribute' => 'subscriptionType.subscription_type_id',
                'label' => 'Subscription Type'
            ],
        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
        [
                'attribute' => 'subscriptionStatusType.subscription_status_type_id',
                'label' => 'Subscription Status Type'
            ],
        'subscription_begin_dt',
        'subscription_end_dt',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'subscription'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
