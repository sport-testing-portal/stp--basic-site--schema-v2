<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Org'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Address'),
        'content' => $this->render('_dataAddress', [
            'model' => $model,
            'row' => $model->addresses,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp'),
        'content' => $this->render('_dataCamp', [
            'model' => $model,
            'row' => $model->camps,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Media'),
        'content' => $this->render('_dataMedia', [
            'model' => $model,
            'row' => $model->media,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note'),
        'content' => $this->render('_dataNote', [
            'model' => $model,
            'row' => $model->notes,
        ]),
    ],
                    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Payment Log'),
        'content' => $this->render('_dataPaymentLog', [
            'model' => $model,
            'row' => $model->paymentLogs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Person'),
        'content' => $this->render('_dataPerson', [
            'model' => $model,
            'row' => $model->people,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School'),
        'content' => $this->render('_dataSchool', [
            'model' => $model,
            'row' => $model->schools,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Subscription'),
        'content' => $this->render('_dataSubscription', [
            'model' => $model,
            'row' => $model->subscriptions,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team'),
        'content' => $this->render('_dataTeam', [
            'model' => $model,
            'row' => $model->teams,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
