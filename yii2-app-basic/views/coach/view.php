<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Coach */

$this->title = $model->coach_id;
$this->params['breadcrumbs'][] = ['label' => 'Coaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coach-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Coach'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->coach_id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->coach_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->coach_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'teamCoaches.team_coach_id',
            'label' => 'Coach',
        ],
        [
            'attribute' => 'person.person_id',
            'label' => 'Person',
        ],
        [
            'attribute' => 'coachType.coach_type_id',
            'label' => 'Coach Type',
        ],
        [
            'attribute' => 'coachTeamCoach.team_coach_id',
            'label' => 'Coach Team Coach',
        ],
        [
            'attribute' => 'ageGroup.age_group_id',
            'label' => 'Age Group',
        ],
        'coach_specialty',
        'coach_certifications',
        'coach_comments',
        'coach_qrcode_uri',
        'coach_info_source_scrape_url:url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerCampCoach->totalCount){
    $gridColumnCampCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_coach_id',
            [
                'attribute' => 'camp.camp_id',
                'label' => 'Camp'
            ],
                        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnCampCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSessionCoach->totalCount){
    $gridColumnCampSessionCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_coach_id',
            [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
                        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSessionCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSessionCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCampSessionPlayerEval->totalCount){
    $gridColumnCampSessionPlayerEval = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_player_eval_id',
            [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
                        [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
            'camp_session_coach_comment',
            'camp_session_player_comment',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSessionPlayerEval,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session-player-eval']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session Player Eval'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSessionPlayerEval
    ]);
}
?>

    </div>
    <div class="row">
        <h4>AgeGroup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAgeGroup = [
        'sport_id',
        'age_group_name',
        'age_group_desc',
        'age_group_min',
        'age_group_max',
        'display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->ageGroup,
        'attributes' => $gridColumnAgeGroup    ]);
    ?>
    <div class="row">
        <h4>CoachType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCoachType = [
        'coach_type_name',
        'display_order',
        'coach_type_desc_short',
        'coach_type_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->coachType,
        'attributes' => $gridColumnCoachType    ]);
    ?>
    <div class="row">
        <h4>Person<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPerson = [
        'org_id',
        'app_user_id',
        'person_type_id',
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person_name_full',
        'person_phone_personal',
        'person_email_personal',
        'person_phone_work',
        'person_email_work',
        'person_position_work',
        'gender_id',
        'person_image_headshot_url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        'person_country_code',
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->person,
        'attributes' => $gridColumnPerson    ]);
    ?>
    <div class="row">
        <h4>TeamCoach<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeamCoach = [
        'team_coach_id',
        'team_id',
        [
            'attribute' => 'coach.coach_id',
            'label' => 'Coach',
        ],
        'team_coach_coach_type_id',
        'primary_position',
        'team_coach_begin_dt',
        'team_coach_end_dt',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->coachTeamCoach,
        'attributes' => $gridColumnTeamCoach    ]);
    ?>
    
    <div class="row">
<?php
if($providerCoachSport->totalCount){
    $gridColumnCoachSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'coach_sport_id',
                        [
                'attribute' => 'sport.sport_id',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'gender.gender_id',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCoachSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-coach-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Coach Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnCoachSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSchoolCoach->totalCount){
    $gridColumnSchoolCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'school_coach_id',
            [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
                        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSchoolCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('School Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnSchoolCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeamCoach->totalCount){
    $gridColumnTeamCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_coach_id',
            [
                'attribute' => 'team.team_id',
                'label' => 'Team'
            ],
                        'team_coach_coach_type_id',
            'primary_position',
            'team_coach_begin_dt',
            'team_coach_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeamCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnTeamCoach
    ]);
}
?>

    </div>
</div>
