<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Coach */

?>
<div class="coach-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->coach_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'teamCoaches.team_coach_id',
            'label' => 'Coach',
        ],
        [
            'attribute' => 'person.person_id',
            'label' => 'Person',
        ],
        [
            'attribute' => 'coachType.coach_type_id',
            'label' => 'Coach Type',
        ],
        [
            'attribute' => 'coachTeamCoach.team_coach_id',
            'label' => 'Coach Team Coach',
        ],
        [
            'attribute' => 'ageGroup.age_group_id',
            'label' => 'Age Group',
        ],
        'coach_specialty',
        'coach_certifications',
        'coach_comments',
        'coach_qrcode_uri',
        'coach_info_source_scrape_url:url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>