<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Coach */

$this->title = 'Save As New Coach: '. ' ' . $model->coach_id;
$this->params['breadcrumbs'][] = ['label' => 'Coaches', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->coach_id, 'url' => ['view', 'id' => $model->coach_id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="coach-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
