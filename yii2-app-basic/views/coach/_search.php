<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CoachSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-coach-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coach_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamCoach::find()->orderBy('coach_id')->asArray()->all(), 'coach_id', 'team_coach_id'),
        'options' => ['placeholder' => 'Choose Team coach'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'person_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person_id')->asArray()->all(), 'person_id', 'person_id'),
        'options' => ['placeholder' => 'Choose Person'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'coach_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\CoachType::find()->orderBy('coach_type_id')->asArray()->all(), 'coach_type_id', 'coach_type_id'),
        'options' => ['placeholder' => 'Choose Coach type'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'coach_team_coach_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamCoach::find()->orderBy('team_coach_id')->asArray()->all(), 'team_coach_id', 'team_coach_id'),
        'options' => ['placeholder' => 'Choose Team coach'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'age_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group_id'),
        'options' => ['placeholder' => 'Choose Age group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'coach_specialty')->textInput(['maxlength' => true, 'placeholder' => 'Coach Specialty']) */ ?>

    <?php /* echo $form->field($model, 'coach_certifications')->textInput(['maxlength' => true, 'placeholder' => 'Coach Certifications']) */ ?>

    <?php /* echo $form->field($model, 'coach_comments')->textInput(['maxlength' => true, 'placeholder' => 'Coach Comments']) */ ?>

    <?php /* echo $form->field($model, 'coach_qrcode_uri')->textInput(['maxlength' => true, 'placeholder' => 'Coach Qrcode Uri']) */ ?>

    <?php /* echo $form->field($model, 'coach_info_source_scrape_url')->textInput(['maxlength' => true, 'placeholder' => 'Coach Info Source Scrape Url']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
