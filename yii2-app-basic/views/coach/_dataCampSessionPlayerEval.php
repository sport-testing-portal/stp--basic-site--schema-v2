<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->campSessionPlayerEvals,
        'key' => 'camp_session_player_eval_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'camp_session_player_eval_id',
        [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
        [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
        'camp_session_coach_comment',
        'camp_session_player_comment',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'camp-session-player-eval'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
