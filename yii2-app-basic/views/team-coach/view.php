<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamCoach */

$this->title = $model->team_coach_id;
$this->params['breadcrumbs'][] = ['label' => 'Team Coach', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-coach-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Team Coach'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->team_coach_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->team_coach_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_coach_id',
        [
            'attribute' => 'team.team_id',
            'label' => 'Team',
        ],
        [
            'attribute' => 'coach.coach_id',
            'label' => 'Coach',
        ],
        'team_coach_coach_type_id',
        'primary_position',
        'team_coach_begin_dt',
        'team_coach_end_dt',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerCoach->totalCount){
    $gridColumnCoach = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            [
                'attribute' => 'coachType.coach_type_id',
                'label' => 'Coach Type'
            ],
                        [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
            'coach_specialty',
            'coach_certifications',
            'coach_comments',
            'coach_qrcode_uri',
            'coach_info_source_scrape_url:url',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnCoach
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Coach<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCoach = [
        'person_id',
        'coach_type_id',
        'coach_team_coach_id',
        'age_group_id',
        'coach_specialty',
        'coach_certifications',
        'coach_comments',
        'coach_qrcode_uri',
        'coach_info_source_scrape_url',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->coach,
        'attributes' => $gridColumnCoach    ]);
    ?>
    <div class="row">
        <h4>Team<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeam = [
        'org_id',
        'school_id',
        'sport_id',
        'camp_id',
        'gender_id',
        'age_group_id',
        'team_name',
        'team_gender',
        'team_division_id',
        'team_league_id',
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url',
        'team_schedule_url',
        'team_schedule_uri',
        'team_statistical_highlights',
        'team_competition_season_id',
        'team_competition_season',
        'team_city',
        'team_state_id',
        'team_country_id',
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->team,
        'attributes' => $gridColumnTeam    ]);
    ?>
</div>
