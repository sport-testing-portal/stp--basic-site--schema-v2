<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeamCoach */

$this->title = 'Update Team Coach: ' . ' ' . $model->team_coach_id;
$this->params['breadcrumbs'][] = ['label' => 'Team Coach', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->team_coach_id, 'url' => ['view', 'id' => $model->team_coach_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="team-coach-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
