<div class="form-group" id="add-coach">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Coach',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'coach_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'person_id' => [
            'label' => 'Person',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person_id')->asArray()->all(), 'person_id', 'person_id'),
                'options' => ['placeholder' => 'Choose Person'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'coach_type_id' => [
            'label' => 'Coach type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\CoachType::find()->orderBy('coach_type_id')->asArray()->all(), 'coach_type_id', 'coach_type_id'),
                'options' => ['placeholder' => 'Choose Coach type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'age_group_id' => [
            'label' => 'Age group',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group_id'),
                'options' => ['placeholder' => 'Choose Age group'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'coach_specialty' => ['type' => TabularForm::INPUT_TEXT],
        'coach_certifications' => ['type' => TabularForm::INPUT_TEXT],
        'coach_comments' => ['type' => TabularForm::INPUT_TEXT],
        'coach_qrcode_uri' => ['type' => TabularForm::INPUT_TEXT],
        'coach_info_source_scrape_url' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowCoach(' . $key . '); return false;', 'id' => 'coach-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Coach', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowCoach()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

