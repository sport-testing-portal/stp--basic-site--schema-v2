<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamCoach */

?>
<div class="team-coach-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->team_coach_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_coach_id',
        [
            'attribute' => 'team.team_id',
            'label' => 'Team',
        ],
        [
            'attribute' => 'coach.coach_id',
            'label' => 'Coach',
        ],
        'team_coach_coach_type_id',
        'primary_position',
        'team_coach_begin_dt',
        'team_coach_end_dt',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>