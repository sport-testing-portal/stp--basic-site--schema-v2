<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamCoach */

$this->title = 'Create Team Coach';
$this->params['breadcrumbs'][] = ['label' => 'Team Coach', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-coach-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
