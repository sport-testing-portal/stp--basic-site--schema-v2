<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player College Recruit'),
        'content' => $this->render('_dataPlayerCollegeRecruit', [
            'model' => $model,
            'row' => $model->playerCollegeRecruits,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player School'),
        'content' => $this->render('_dataPlayerSchool', [
            'model' => $model,
            'row' => $model->playerSchools,
        ]),
    ],
                    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Act'),
        'content' => $this->render('_dataSchoolAct', [
            'model' => $model,
            'row' => $model->schoolActs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Coach'),
        'content' => $this->render('_dataSchoolCoach', [
            'model' => $model,
            'row' => $model->schoolCoaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Sat'),
        'content' => $this->render('_dataSchoolSat', [
            'model' => $model,
            'row' => $model->schoolSats,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Sport'),
        'content' => $this->render('_dataSchoolSport', [
            'model' => $model,
            'row' => $model->schoolSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('School Stat'),
        'content' => $this->render('_dataSchoolStat', [
            'model' => $model,
            'row' => $model->schoolStats,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team'),
        'content' => $this->render('_dataTeam', [
            'model' => $model,
            'row' => $model->teams,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
