<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SchoolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-school-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'school_id')->textInput(['placeholder' => 'School']) ?>

    <?= $form->field($model, 'org_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org_id'),
        'options' => ['placeholder' => 'Choose Org'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'religious_affiliation_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\ReligiousAffiliation::find()->orderBy('religious_affiliation_id')->asArray()->all(), 'religious_affiliation_id', 'religious_affiliation'),
        'options' => ['placeholder' => 'Choose Religious affiliation'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'conference_id__female')->textInput(['placeholder' => 'Conference Id  Female']) ?>

    <?= $form->field($model, 'conference_id__male')->textInput(['placeholder' => 'Conference Id  Male']) ?>

    <?php /* echo $form->field($model, 'conference_id__main')->textInput(['placeholder' => 'Conference Id  Main']) */ ?>

    <?php /* echo $form->field($model, 'school_ipeds_id')->textInput(['placeholder' => 'School Ipeds']) */ ?>

    <?php /* echo $form->field($model, 'school_unit_id')->textInput(['placeholder' => 'School Unit']) */ ?>

    <?php /* echo $form->field($model, 'school_ope_id')->textInput(['placeholder' => 'School Ope']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
