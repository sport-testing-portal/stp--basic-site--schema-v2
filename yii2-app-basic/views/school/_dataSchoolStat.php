<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->schoolStats,
        'key' => 'school_stat_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'school_stat_id',
        'college_cost_rating',
        'college_cost_tuition_instate',
        'college_cost_tuition_outofstate',
        'college_cost_room_and_board',
        'college_cost_books_and_supplies',
        'college_early_decision_dt',
        'college_early_action_dt',
        'college_regular_decision_dt',
        'college_pct_applicants_admitted',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'school-stat'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
