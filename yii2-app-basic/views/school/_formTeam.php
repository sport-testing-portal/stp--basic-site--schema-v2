<div class="form-group" id="add-team">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Team',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'team_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'org_id' => [
            'label' => 'Org',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org_id'),
                'options' => ['placeholder' => 'Choose Org'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'sport_id' => [
            'label' => 'Sport',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Sport::find()->orderBy('sport_id')->asArray()->all(), 'sport_id', 'sport_id'),
                'options' => ['placeholder' => 'Choose Sport'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'camp_id' => [
            'label' => 'Camp',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->orderBy('camp_id')->asArray()->all(), 'camp_id', 'camp_id'),
                'options' => ['placeholder' => 'Choose Camp'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'gender_id' => [
            'label' => 'Gender',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender_id'),
                'options' => ['placeholder' => 'Choose Gender'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'age_group_id' => [
            'label' => 'Age group',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group_id'),
                'options' => ['placeholder' => 'Choose Age group'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_name' => ['type' => TabularForm::INPUT_TEXT],
        'team_gender' => ['type' => TabularForm::INPUT_TEXT],
        'team_division_id' => [
            'label' => 'Team division',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamDivision::find()->orderBy('team_division_id')->asArray()->all(), 'team_division_id', 'team_division_id'),
                'options' => ['placeholder' => 'Choose Team division'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_league_id' => [
            'label' => 'Team league',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamLeague::find()->orderBy('team_league_id')->asArray()->all(), 'team_league_id', 'team_league_id'),
                'options' => ['placeholder' => 'Choose Team league'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_division' => ['type' => TabularForm::INPUT_TEXT],
        'team_league' => ['type' => TabularForm::INPUT_TEXT],
        'organizational_level' => ['type' => TabularForm::INPUT_TEXT],
        'team_governing_body' => ['type' => TabularForm::INPUT_TEXT],
        'team_age_group' => ['type' => TabularForm::INPUT_TEXT],
        'team_website_url' => ['type' => TabularForm::INPUT_TEXT],
        'team_schedule_url' => ['type' => TabularForm::INPUT_TEXT],
        'team_schedule_uri' => ['type' => TabularForm::INPUT_TEXT],
        'team_statistical_highlights' => ['type' => TabularForm::INPUT_TEXT],
        'team_competition_season_id' => ['type' => TabularForm::INPUT_TEXT],
        'team_competition_season' => ['type' => TabularForm::INPUT_TEXT],
        'team_city' => ['type' => TabularForm::INPUT_TEXT],
        'team_state_id' => ['type' => TabularForm::INPUT_TEXT],
        'team_country_id' => [
            'label' => 'Country',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->orderBy('country_id')->asArray()->all(), 'country_id', 'country_id'),
                'options' => ['placeholder' => 'Choose Country'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_wins' => ['type' => TabularForm::INPUT_TEXT],
        'team_losses' => ['type' => TabularForm::INPUT_TEXT],
        'team_draws' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowTeam(' . $key . '); return false;', 'id' => 'team-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Team', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTeam()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

