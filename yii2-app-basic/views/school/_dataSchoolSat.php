<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->schoolSats,
        'key' => 'school_sat_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'school_sat_id',
        'school_unit_id',
        'school_sat_i_verbal_25_pct',
        'school_sat_i_verbal_75_pct',
        'school_sat_i_math_25_pct',
        'school_sat_i_math_75_pct',
        'school_sat_student_submit_cnt',
        'school_sat_student_submit_pct',
        'school_sat_report_period',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'school-sat'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
