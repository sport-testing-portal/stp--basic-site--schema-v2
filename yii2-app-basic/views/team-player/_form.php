<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamPlayer */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Player', 
        'relID' => 'player', 
        'value' => \yii\helpers\Json::encode($model->players),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="team-player-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'team_player_id')->textInput(['placeholder' => 'Team Player']) ?>

    <?= $form->field($model, 'team_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->orderBy('team_id')->asArray()->all(), 'team_id', 'team_id'),
        'options' => ['placeholder' => 'Choose Team'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Player::find()->orderBy('player_id')->asArray()->all(), 'player_id', 'player_id'),
        'options' => ['placeholder' => 'Choose Player'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_play_sport_position_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position_id')->asArray()->all(), 'sport_position_id', 'sport_position_id'),
        'options' => ['placeholder' => 'Choose Sport position'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_play_sport_position2_id')->textInput(['placeholder' => 'Team Play Sport Position2']) ?>

    <?= $form->field($model, 'primary_position')->textInput(['maxlength' => true, 'placeholder' => 'Primary Position']) ?>

    <?= $form->field($model, 'team_play_statistical_highlights')->textInput(['maxlength' => true, 'placeholder' => 'Team Play Statistical Highlights']) ?>

    <?= $form->field($model, 'coach_id')->textInput(['placeholder' => 'Coach']) ?>

    <?= $form->field($model, 'coach_name')->textInput(['maxlength' => true, 'placeholder' => 'Coach Name']) ?>

    <?= $form->field($model, 'team_play_current_team')->textInput(['placeholder' => 'Team Play Current Team']) ?>

    <?= $form->field($model, 'team_play_begin_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Team Play Begin Dt',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'team_play_end_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Team Play End Dt',
                'autoclose' => true,
            ]
        ],
    ]); ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Player'),
            'content' => $this->render('_formPlayer', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->players),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
