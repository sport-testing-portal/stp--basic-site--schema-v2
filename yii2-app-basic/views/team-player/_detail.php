<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamPlayer */

?>
<div class="team-player-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->team_player_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_player_id',
        [
            'attribute' => 'team.team_id',
            'label' => 'Team',
        ],
        [
            'attribute' => 'player.player_id',
            'label' => 'Player',
        ],
        [
            'attribute' => 'teamPlaySportPosition.sport_position_id',
            'label' => 'Team Play Sport Position',
        ],
        'team_play_sport_position2_id',
        'primary_position',
        'team_play_statistical_highlights',
        'coach_id',
        'coach_name',
        'team_play_current_team',
        'team_play_begin_dt',
        'team_play_end_dt',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>