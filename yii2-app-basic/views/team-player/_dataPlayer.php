<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->players,
        'key' => 'player_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
        [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
        [
                'attribute' => 'playerSportPosition.sport_position_id',
                'label' => 'Player Sport Position'
            ],
        'player_sport_position2_id',
        'player_access_code',
        'player_waiver_minor_dt',
        'player_waiver_adult_dt',
        'player_parent_email:email',
        'player_sport_preference',
        'player_sport_position_preference',
        'player_shot_side_preference',
        'player_dominant_side',
        'player_dominant_foot',
        'player_statistical_highlights',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'player'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
