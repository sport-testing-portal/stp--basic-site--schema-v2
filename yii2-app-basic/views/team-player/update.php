<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeamPlayer */

$this->title = 'Update Team Player: ' . ' ' . $model->team_player_id;
$this->params['breadcrumbs'][] = ['label' => 'Team Player', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->team_player_id, 'url' => ['view', 'id' => $model->team_player_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="team-player-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
