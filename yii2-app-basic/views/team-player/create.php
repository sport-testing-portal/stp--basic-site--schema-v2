<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamPlayer */

$this->title = 'Create Team Player';
$this->params['breadcrumbs'][] = ['label' => 'Team Player', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-player-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
