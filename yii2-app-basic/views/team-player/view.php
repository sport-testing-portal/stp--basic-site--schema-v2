<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamPlayer */

$this->title = $model->team_player_id;
$this->params['breadcrumbs'][] = ['label' => 'Team Player', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-player-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Team Player'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->team_player_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->team_player_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_player_id',
        [
            'attribute' => 'team.team_id',
            'label' => 'Team',
        ],
        [
            'attribute' => 'player.player_id',
            'label' => 'Player',
        ],
        [
            'attribute' => 'teamPlaySportPosition.sport_position_id',
            'label' => 'Team Play Sport Position',
        ],
        'team_play_sport_position2_id',
        'primary_position',
        'team_play_statistical_highlights',
        'coach_id',
        'coach_name',
        'team_play_current_team',
        'team_play_begin_dt',
        'team_play_end_dt',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerPlayer->totalCount){
    $gridColumnPlayer = [
        ['class' => 'yii\grid\SerialColumn'],
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
                        [
                'attribute' => 'playerSportPosition.sport_position_id',
                'label' => 'Player Sport Position'
            ],
            'player_sport_position2_id',
            'player_access_code',
            'player_waiver_minor_dt',
            'player_waiver_adult_dt',
            'player_parent_email:email',
            'player_sport_preference',
            'player_sport_position_preference',
            'player_shot_side_preference',
            'player_dominant_side',
            'player_dominant_foot',
            'player_statistical_highlights',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayer,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayer
    ]);
}
?>

    </div>
    <div class="row">
        <h4>Player<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPlayer = [
        'person_id',
        'age_group_id',
        'player_team_player_id',
        'player_sport_position_id',
        'player_sport_position2_id',
        'player_access_code',
        'player_waiver_minor_dt',
        'player_waiver_adult_dt',
        'player_parent_email',
        'player_sport_preference',
        'player_sport_position_preference',
        'player_shot_side_preference',
        'player_dominant_side',
        'player_dominant_foot',
        'player_statistical_highlights',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->player,
        'attributes' => $gridColumnPlayer    ]);
    ?>
    <div class="row">
        <h4>SportPosition<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSportPosition = [
        'sport_position_id',
        'sport_id',
        'sport_position_name',
        'display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->teamPlaySportPosition,
        'attributes' => $gridColumnSportPosition    ]);
    ?>
    <div class="row">
        <h4>Team<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeam = [
        'org_id',
        'school_id',
        'sport_id',
        'camp_id',
        'gender_id',
        'age_group_id',
        'team_name',
        'team_gender',
        'team_division_id',
        'team_league_id',
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url',
        'team_schedule_url',
        'team_schedule_uri',
        'team_statistical_highlights',
        'team_competition_season_id',
        'team_competition_season',
        'team_city',
        'team_state_id',
        'team_country_id',
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->team,
        'attributes' => $gridColumnTeam    ]);
    ?>
</div>
