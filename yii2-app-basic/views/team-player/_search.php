<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamPlayerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-team-player-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'team_player_id')->textInput(['placeholder' => 'Team Player']) ?>

    <?= $form->field($model, 'team_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->orderBy('team_id')->asArray()->all(), 'team_id', 'team_id'),
        'options' => ['placeholder' => 'Choose Team'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Player::find()->orderBy('player_id')->asArray()->all(), 'player_id', 'player_id'),
        'options' => ['placeholder' => 'Choose Player'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_play_sport_position_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position_id')->asArray()->all(), 'sport_position_id', 'sport_position_id'),
        'options' => ['placeholder' => 'Choose Sport position'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'team_play_sport_position2_id')->textInput(['placeholder' => 'Team Play Sport Position2']) ?>

    <?php /* echo $form->field($model, 'primary_position')->textInput(['maxlength' => true, 'placeholder' => 'Primary Position']) */ ?>

    <?php /* echo $form->field($model, 'team_play_statistical_highlights')->textInput(['maxlength' => true, 'placeholder' => 'Team Play Statistical Highlights']) */ ?>

    <?php /* echo $form->field($model, 'coach_id')->textInput(['placeholder' => 'Coach']) */ ?>

    <?php /* echo $form->field($model, 'coach_name')->textInput(['maxlength' => true, 'placeholder' => 'Coach Name']) */ ?>

    <?php /* echo $form->field($model, 'team_play_current_team')->textInput(['placeholder' => 'Team Play Current Team']) */ ?>

    <?php /* echo $form->field($model, 'team_play_begin_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Team Play Begin Dt',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'team_play_end_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Team Play End Dt',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
