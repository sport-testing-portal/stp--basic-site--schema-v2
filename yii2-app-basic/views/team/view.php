<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->team_id;
$this->params['breadcrumbs'][] = ['label' => 'Team', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Team'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->team_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->team_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_id',
        [
            'attribute' => 'org.org_id',
            'label' => 'Org',
        ],
        [
            'attribute' => 'school.school_id',
            'label' => 'School',
        ],
        [
            'attribute' => 'sport.sport_id',
            'label' => 'Sport',
        ],
        [
            'attribute' => 'camp.camp_id',
            'label' => 'Camp',
        ],
        [
            'attribute' => 'gender.gender_id',
            'label' => 'Gender',
        ],
        [
            'attribute' => 'ageGroup.age_group_id',
            'label' => 'Age Group',
        ],
        'team_name',
        'team_gender',
        [
            'attribute' => 'teamDivision.team_division_id',
            'label' => 'Team Division',
        ],
        [
            'attribute' => 'teamLeague.team_league_id',
            'label' => 'Team League',
        ],
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url:url',
        'team_schedule_url:url',
        'team_schedule_uri',
        'team_statistical_highlights',
        'team_competition_season_id',
        'team_competition_season',
        'team_city',
        'team_state_id',
        [
            'attribute' => 'teamCountry.country_id',
            'label' => 'Team Country',
        ],
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerPlayerTeam->totalCount){
    $gridColumnPlayerTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_team_id',
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        'begin_dt',
            'end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Team'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerTeam
    ]);
}
?>

    </div>
    <div class="row">
        <h4>AgeGroup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAgeGroup = [
        'sport_id',
        'age_group_name',
        'age_group_desc',
        'age_group_min',
        'age_group_max',
        'display_order',
    ];
    echo DetailView::widget([
        'model' => $model->ageGroup,
        'attributes' => $gridColumnAgeGroup    ]);
    ?>
    <div class="row">
        <h4>Camp<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCamp = [
        [
            'attribute' => 'org.org_id',
            'label' => 'Org',
        ],
        'camp_name',
        'camp_specialty',
        'camp_cost_regular',
        'camp_cost_early_registration',
        'camp_team_discounts_available_yn',
        'camp_scholarships_available_yn',
        'camp_session_desc',
        'camp_website_url',
        'camp_session_url',
        'camp_registration_url',
        'camp_twitter_url',
        'camp_facebook_url',
        'camp_scholarship_application_info',
        'camp_scholarship_application_request',
        'camp_organizer_description',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->camp,
        'attributes' => $gridColumnCamp    ]);
    ?>
    <div class="row">
        <h4>Country<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCountry = [
        'country_id',
        'iso2',
        'short_name',
        'long_name',
        'iso3',
        'numcode',
        'un_member',
        'calling_code',
        'cctld',
    ];
    echo DetailView::widget([
        'model' => $model->teamCountry,
        'attributes' => $gridColumnCountry    ]);
    ?>
    <div class="row">
        <h4>TeamDivision<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeamDivision = [
        'team_division_name',
        'team_division_desc_short',
        'team_division_desc_long',
        'team_division_display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->teamDivision,
        'attributes' => $gridColumnTeamDivision    ]);
    ?>
    <div class="row">
        <h4>Gender<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGender = [
        'gender_desc',
        'gender_code',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->gender,
        'attributes' => $gridColumnGender    ]);
    ?>
    <div class="row">
        <h4>TeamLeague<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeamLeague = [
        'team_league_name',
        'team_league_desc_short',
        'team_league_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->teamLeague,
        'attributes' => $gridColumnTeamLeague    ]);
    ?>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org_name',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    <div class="row">
        <h4>School<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSchool = [
        [
            'attribute' => 'org.org_id',
            'label' => 'Org',
        ],
        'religious_affiliation_id',
        'conference_id__female',
        'conference_id__male',
        'conference_id__main',
        'school_ipeds_id',
        'school_unit_id',
        'school_ope_id',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->school,
        'attributes' => $gridColumnSchool    ]);
    ?>
    <div class="row">
        <h4>Sport<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSport = [
        'gsm_sport_name',
        'sport_desc_short',
        'sport_desc_long',
        'high_school_yn',
        'ncaa_yn',
        'olympic_yn',
        'xgame_sport_yn',
        'ncaa_sport_name',
        'olympic_sport_name',
        'xgame_sport_name',
        'ncaa_sport_type',
        'ncaa_sport_season_male',
        'ncaa_sport_season_female',
        'ncaa_sport_season_coed',
        'olympic_sport_season',
        'xgame_sport_season',
        'olympic_sport_gender',
        'xgame_sport_gender',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->sport,
        'attributes' => $gridColumnSport    ]);
    ?>
    
    <div class="row">
<?php
if($providerTeamCoach->totalCount){
    $gridColumnTeamCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_coach_id',
                        [
                'attribute' => 'coach.coach_id',
                'label' => 'Coach'
            ],
            'team_coach_coach_type_id',
            'primary_position',
            'team_coach_begin_dt',
            'team_coach_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeamCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnTeamCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeamPlayer->totalCount){
    $gridColumnTeamPlayer = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_player_id',
                        [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
            [
                'attribute' => 'teamPlaySportPosition.sport_position_id',
                'label' => 'Team Play Sport Position'
            ],
            'team_play_sport_position2_id',
            'primary_position',
            'team_play_statistical_highlights',
            'coach_id',
            'coach_name',
            'team_play_current_team',
            'team_play_begin_dt',
            'team_play_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeamPlayer,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team-player']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team Player'),
        ],
        'export' => false,
        'columns' => $gridColumnTeamPlayer
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTestEvalSummaryLog->totalCount){
    $gridColumnTestEvalSummaryLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'test_eval_summary_log_id',
            [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
                        [
                'attribute' => 'testEvalType.test_eval_type_id',
                'label' => 'Test Eval Type'
            ],
            'source_event_id',
            'test_eval_overall_ranking',
            'test_eval_positional_ranking',
            'test_eval_total_score',
            'test_eval_avg_score',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTestEvalSummaryLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-test-eval-summary-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Test Eval Summary Log'),
        ],
        'export' => false,
        'columns' => $gridColumnTestEvalSummaryLog
    ]);
}
?>

    </div>
</div>
