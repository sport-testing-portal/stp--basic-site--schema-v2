<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

?>
<div class="team-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->team_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_id',
        [
            'attribute' => 'org.org_id',
            'label' => 'Org',
        ],
        [
            'attribute' => 'school.school_id',
            'label' => 'School',
        ],
        [
            'attribute' => 'sport.sport_id',
            'label' => 'Sport',
        ],
        [
            'attribute' => 'camp.camp_id',
            'label' => 'Camp',
        ],
        [
            'attribute' => 'gender.gender_id',
            'label' => 'Gender',
        ],
        [
            'attribute' => 'ageGroup.age_group_id',
            'label' => 'Age Group',
        ],
        'team_name',
        'team_gender',
        [
            'attribute' => 'teamDivision.team_division_id',
            'label' => 'Team Division',
        ],
        [
            'attribute' => 'teamLeague.team_league_id',
            'label' => 'Team League',
        ],
        'team_division',
        'team_league',
        'organizational_level',
        'team_governing_body',
        'team_age_group',
        'team_website_url:url',
        'team_schedule_url:url',
        'team_schedule_uri',
        'team_statistical_highlights',
        'team_competition_season_id',
        'team_competition_season',
        'team_city',
        'team_state_id',
        [
            'attribute' => 'teamCountry.country_id',
            'label' => 'Team Country',
        ],
        'team_wins',
        'team_losses',
        'team_draws',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>