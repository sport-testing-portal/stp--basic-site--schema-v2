<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->testEvalSummaryLogs,
        'key' => 'test_eval_summary_log_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'test_eval_summary_log_id',
        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
        [
                'attribute' => 'testEvalType.test_eval_type_id',
                'label' => 'Test Eval Type'
            ],
        'source_event_id',
        'test_eval_overall_ranking',
        'test_eval_positional_ranking',
        'test_eval_total_score',
        'test_eval_avg_score',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'test-eval-summary-log'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
