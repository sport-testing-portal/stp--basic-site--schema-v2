<div class="form-group" id="add-player-sport">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'PlayerSport',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'player_sport_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'sport_id' => [
            'label' => 'Sport',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Sport::find()->orderBy('sport_id')->asArray()->all(), 'sport_id', 'sport_id'),
                'options' => ['placeholder' => 'Choose Sport'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'sport_position_id' => [
            'label' => 'Sport position',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position_id')->asArray()->all(), 'sport_position_id', 'sport_position_id'),
                'options' => ['placeholder' => 'Choose Sport position'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'gender_id' => [
            'label' => 'Gender',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender_id'),
                'options' => ['placeholder' => 'Choose Gender'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPlayerSport(' . $key . '); return false;', 'id' => 'player-sport-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Player Sport', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPlayerSport()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

