<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PlayerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-player-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'player_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamPlayer::find()->orderBy('player_id')->asArray()->all(), 'player_id', 'team_player_id'),
        'options' => ['placeholder' => 'Choose Team player'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'person_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\Person::find()->orderBy('person_id')->asArray()->all(), 'person_id', 'person_id'),
        'options' => ['placeholder' => 'Choose Person'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'age_group_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\AgeGroup::find()->orderBy('age_group_id')->asArray()->all(), 'age_group_id', 'age_group_id'),
        'options' => ['placeholder' => 'Choose Age group'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_team_player_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\TeamPlayer::find()->orderBy('team_player_id')->asArray()->all(), 'team_player_id', 'team_player_id'),
        'options' => ['placeholder' => 'Choose Team player'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'player_sport_position_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position_id')->asArray()->all(), 'sport_position_id', 'sport_position_id'),
        'options' => ['placeholder' => 'Choose Sport position'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'player_sport_position2_id')->textInput(['placeholder' => 'Player Sport Position2']) */ ?>

    <?php /* echo $form->field($model, 'player_access_code')->textInput(['maxlength' => true, 'placeholder' => 'Player Access Code']) */ ?>

    <?php /* echo $form->field($model, 'player_waiver_minor_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Player Waiver Minor Dt',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'player_waiver_adult_dt')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => 'Choose Player Waiver Adult Dt',
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'player_parent_email')->textInput(['maxlength' => true, 'placeholder' => 'Player Parent Email']) */ ?>

    <?php /* echo $form->field($model, 'player_sport_preference')->textInput(['maxlength' => true, 'placeholder' => 'Player Sport Preference']) */ ?>

    <?php /* echo $form->field($model, 'player_sport_position_preference')->textInput(['maxlength' => true, 'placeholder' => 'Player Sport Position Preference']) */ ?>

    <?php /* echo $form->field($model, 'player_shot_side_preference')->textInput(['maxlength' => true, 'placeholder' => 'Player Shot Side Preference']) */ ?>

    <?php /* echo $form->field($model, 'player_dominant_side')->textInput(['maxlength' => true, 'placeholder' => 'Player Dominant Side']) */ ?>

    <?php /* echo $form->field($model, 'player_dominant_foot')->textInput(['maxlength' => true, 'placeholder' => 'Player Dominant Foot']) */ ?>

    <?php /* echo $form->field($model, 'player_statistical_highlights')->textInput(['maxlength' => true, 'placeholder' => 'Player Statistical Highlights']) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
