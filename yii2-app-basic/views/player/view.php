<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Player */

$this->title = $model->player_id;
$this->params['breadcrumbs'][] = ['label' => 'Player', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Player'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->player_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->player_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        [
            'attribute' => 'teamPlayers.team_player_id',
            'label' => 'Player',
        ],
        [
            'attribute' => 'person.person_id',
            'label' => 'Person',
        ],
        [
            'attribute' => 'ageGroup.age_group_id',
            'label' => 'Age Group',
        ],
        [
            'attribute' => 'playerTeamPlayer.team_player_id',
            'label' => 'Player Team Player',
        ],
        [
            'attribute' => 'playerSportPosition.sport_position_id',
            'label' => 'Player Sport Position',
        ],
        'player_sport_position2_id',
        'player_access_code',
        'player_waiver_minor_dt',
        'player_waiver_adult_dt',
        'player_parent_email:email',
        'player_sport_preference',
        'player_sport_position_preference',
        'player_shot_side_preference',
        'player_dominant_side',
        'player_dominant_foot',
        'player_statistical_highlights',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerCampSessionPlayerEval->totalCount){
    $gridColumnCampSessionPlayerEval = [
        ['class' => 'yii\grid\SerialColumn'],
            'camp_session_player_eval_id',
            [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
            [
                'attribute' => 'coach.coach_id',
                'label' => 'Coach'
            ],
                        'camp_session_coach_comment',
            'camp_session_player_comment',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCampSessionPlayerEval,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-camp-session-player-eval']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Camp Session Player Eval'),
        ],
        'export' => false,
        'columns' => $gridColumnCampSessionPlayerEval
    ]);
}
?>

    </div>
    <div class="row">
        <h4>AgeGroup<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAgeGroup = [
        'sport_id',
        'age_group_name',
        'age_group_desc',
        'age_group_min',
        'age_group_max',
        'display_order',
    ];
    echo DetailView::widget([
        'model' => $model->ageGroup,
        'attributes' => $gridColumnAgeGroup    ]);
    ?>
    <div class="row">
        <h4>Person<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPerson = [
        'org_id',
        'app_user_id',
        'person_type_id',
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person_name_full',
        'person_phone_personal',
        'person_email_personal',
        'person_phone_work',
        'person_email_work',
        'person_position_work',
        'gender_id',
        'person_image_headshot_url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        'person_country_code',
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->person,
        'attributes' => $gridColumnPerson    ]);
    ?>
    <div class="row">
        <h4>SportPosition<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSportPosition = [
        'sport_position_id',
        'sport_id',
        'sport_position_name',
        'display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->playerSportPosition,
        'attributes' => $gridColumnSportPosition    ]);
    ?>
    <div class="row">
        <h4>TeamPlayer<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnTeamPlayer = [
        'team_player_id',
        'team_id',
        [
            'attribute' => 'player.player_id',
            'label' => 'Player',
        ],
        'team_play_sport_position_id',
        'team_play_sport_position2_id',
        'primary_position',
        'team_play_statistical_highlights',
        'coach_id',
        'coach_name',
        'team_play_current_team',
        'team_play_begin_dt',
        'team_play_end_dt',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->playerTeamPlayer,
        'attributes' => $gridColumnTeamPlayer    ]);
    ?>
    
    <div class="row">
<?php
if($providerPlayerAcademic->totalCount){
    $gridColumnPlayerAcademic = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_academic_id',
                        'sat_composite_score_max',
            'sat_critical_reading_score_max',
            'sat_math_score_max',
            'sat_writing_score_max',
            'sat_scheduled_dt',
            'psat_composite_score_max',
            'psat_critical_reading_score_max',
            'psat_math_score_max',
            'psat_writing_score_max',
            'act_composite_score_max',
            'act_math_score_max',
            'act_english_score_max',
            'act_scheduled_dt',
            'grade_point_average',
            'class_rank',
            'school_size',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerAcademic,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-academic']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Academic'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerAcademic
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerCampLog->totalCount){
    $gridColumnPlayerCampLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_camp_log_id',
                        [
                'attribute' => 'camp.camp_id',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'campSession.camp_session_id',
                'label' => 'Camp Session'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerCampLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-camp-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Camp Log'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerCampLog
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerCollegeRecruit->totalCount){
    $gridColumnPlayerCollegeRecruit = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_college_recruit_id',
                        [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerCollegeRecruit,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-college-recruit']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player College Recruit'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerCollegeRecruit
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerContact->totalCount){
    $gridColumnPlayerContact = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'playerContact.player_contact_type_id',
                'label' => 'Player Contact'
            ],
            [
                'attribute' => 'playerContactType.player_contact_type_id',
                'label' => 'Player Contact Type'
            ],
                        [
                'attribute' => 'person.person_id',
                'label' => 'Person'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerContact,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-contact']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Contact'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerContact
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerSchool->totalCount){
    $gridColumnPlayerSchool = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_school_id',
                        [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
            'player_school_comment',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerSchool,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-school']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player School'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerSchool
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerSport->totalCount){
    $gridColumnPlayerSport = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_sport_id',
                        [
                'attribute' => 'sport.sport_id',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'sportPosition.sport_position_id',
                'label' => 'Sport Position'
            ],
            [
                'attribute' => 'gender.gender_id',
                'label' => 'Gender'
            ],
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerSport,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-sport']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Sport'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerSport
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerTeam->totalCount){
    $gridColumnPlayerTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_team_id',
                        [
                'attribute' => 'team.team_id',
                'label' => 'Team'
            ],
            'begin_dt',
            'end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Team'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerTeam
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTeamPlayer->totalCount){
    $gridColumnTeamPlayer = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_player_id',
            [
                'attribute' => 'team.team_id',
                'label' => 'Team'
            ],
                        [
                'attribute' => 'teamPlaySportPosition.sport_position_id',
                'label' => 'Team Play Sport Position'
            ],
            'team_play_sport_position2_id',
            'primary_position',
            'team_play_statistical_highlights',
            'coach_id',
            'coach_name',
            'team_play_current_team',
            'team_play_begin_dt',
            'team_play_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeamPlayer,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team-player']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team Player'),
        ],
        'export' => false,
        'columns' => $gridColumnTeamPlayer
    ]);
}
?>

    </div>
</div>
