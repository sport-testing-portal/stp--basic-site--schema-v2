<div class="form-group" id="add-team-player">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'TeamPlayer',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'team_player_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'team_id' => [
            'label' => 'Team',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->orderBy('team_id')->asArray()->all(), 'team_id', 'team_id'),
                'options' => ['placeholder' => 'Choose Team'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_play_sport_position_id' => [
            'label' => 'Sport position',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\SportPosition::find()->orderBy('sport_position_id')->asArray()->all(), 'sport_position_id', 'sport_position_id'),
                'options' => ['placeholder' => 'Choose Sport position'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'team_play_sport_position2_id' => ['type' => TabularForm::INPUT_TEXT],
        'primary_position' => ['type' => TabularForm::INPUT_TEXT],
        'team_play_statistical_highlights' => ['type' => TabularForm::INPUT_TEXT],
        'coach_id' => ['type' => TabularForm::INPUT_TEXT],
        'coach_name' => ['type' => TabularForm::INPUT_TEXT],
        'team_play_current_team' => ['type' => TabularForm::INPUT_TEXT],
        'team_play_begin_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Team Play Begin Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'team_play_end_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Team Play End Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowTeamPlayer(' . $key . '); return false;', 'id' => 'team-player-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Team Player', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTeamPlayer()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

