<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Camp Session Player Eval'),
        'content' => $this->render('_dataCampSessionPlayerEval', [
            'model' => $model,
            'row' => $model->campSessionPlayerEvals,
        ]),
    ],
                            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Academic'),
        'content' => $this->render('_dataPlayerAcademic', [
            'model' => $model,
            'row' => $model->playerAcademics,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Camp Log'),
        'content' => $this->render('_dataPlayerCampLog', [
            'model' => $model,
            'row' => $model->playerCampLogs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player College Recruit'),
        'content' => $this->render('_dataPlayerCollegeRecruit', [
            'model' => $model,
            'row' => $model->playerCollegeRecruits,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Contact'),
        'content' => $this->render('_dataPlayerContact', [
            'model' => $model,
            'row' => $model->playerContacts,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player School'),
        'content' => $this->render('_dataPlayerSchool', [
            'model' => $model,
            'row' => $model->playerSchools,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Sport'),
        'content' => $this->render('_dataPlayerSport', [
            'model' => $model,
            'row' => $model->playerSports,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Team'),
        'content' => $this->render('_dataPlayerTeam', [
            'model' => $model,
            'row' => $model->playerTeams,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Team Player'),
        'content' => $this->render('_dataTeamPlayer', [
            'model' => $model,
            'row' => $model->teamPlayers,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
