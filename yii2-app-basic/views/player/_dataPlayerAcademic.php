<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->playerAcademics,
        'key' => 'player_academic_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'player_academic_id',
        'sat_composite_score_max',
        'sat_critical_reading_score_max',
        'sat_math_score_max',
        'sat_writing_score_max',
        'sat_scheduled_dt',
        'psat_composite_score_max',
        'psat_critical_reading_score_max',
        'psat_math_score_max',
        'psat_writing_score_max',
        'act_composite_score_max',
        'act_math_score_max',
        'act_english_score_max',
        'act_scheduled_dt',
        'grade_point_average',
        'class_rank',
        'school_size',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'player-academic'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
