<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Player */

$this->title = 'Create Player';
$this->params['breadcrumbs'][] = ['label' => 'Player', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
