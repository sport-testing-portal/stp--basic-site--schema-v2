<div class="form-group" id="add-event-attendee-log">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'EventAttendeeLog',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'event_attendee_log_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'event_id' => [
            'label' => 'Event',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Event::find()->orderBy('event_id')->asArray()->all(), 'event_id', 'event_id'),
                'options' => ['placeholder' => 'Choose Event'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'event_attendee_type_id' => [
            'label' => 'Event attendee type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\EventAttendeeType::find()->orderBy('event_attendee_type_id')->asArray()->all(), 'event_attendee_type_id', 'event_attendee_type_id'),
                'options' => ['placeholder' => 'Choose Event attendee type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'event_rsvp_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Event Rsvp Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'event_attendance_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Event Attendance Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowEventAttendeeLog(' . $key . '); return false;', 'id' => 'event-attendee-log-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Event Attendee Log', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowEventAttendeeLog()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

