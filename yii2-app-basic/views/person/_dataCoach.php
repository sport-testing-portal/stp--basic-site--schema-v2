<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->coaches,
        'key' => 'coach_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        [
                'attribute' => 'teamCoaches.team_coach_id',
                'label' => 'Coach'
            ],
        [
                'attribute' => 'coachType.coach_type_id',
                'label' => 'Coach Type'
            ],
        [
                'attribute' => 'coachTeamCoach.team_coach_id',
                'label' => 'Coach Team Coach'
            ],
        [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
        'coach_specialty',
        'coach_certifications',
        'coach_comments',
        'coach_qrcode_uri',
        'coach_info_source_scrape_url:url',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'coach'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
