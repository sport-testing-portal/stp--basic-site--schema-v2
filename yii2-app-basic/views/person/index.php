<?php

/* @var $this yii\web\View */
/* @var $searchModel app\models\PersonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'People';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="person-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Person', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        'person_id',
        [
                'attribute' => 'org_id',
                'label' => 'Org',
                'value' => function($model){
                    if ($model->org)
                    {return $model->org->org_id;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->asArray()->all(), 'org_id', 'org_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Org', 'id' => 'grid-person-search-org_id']
            ],
        [
                'attribute' => 'app_user_id',
                'label' => 'App User',
                'value' => function($model){
                    if ($model->appUser)
                    {return $model->appUser->username;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\AppUser::find()->asArray()->all(), 'app_user_id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'App user', 'id' => 'grid-person-search-app_user_id']
            ],
        [
                'attribute' => 'person_type_id',
                'label' => 'Person Type',
                'value' => function($model){
                    if ($model->personType)
                    {return $model->personType->person_type_id;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\PersonType::find()->asArray()->all(), 'person_type_id', 'person_type_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Person type', 'id' => 'grid-person-search-person_type_id']
            ],
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person_name_full',
        'person_phone_personal',
        'person_email_personal:email',
        'person_phone_work',
        'person_email_work:email',
        'person_position_work',
        [
                'attribute' => 'gender_id',
                'label' => 'Gender',
                'value' => function($model){
                    if ($model->gender)
                    {return $model->gender->gender_id;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->asArray()->all(), 'gender_id', 'gender_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Gender', 'id' => 'grid-person-search-gender_id']
            ],
        'person_image_headshot_url:url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        [
                'attribute' => 'person_country_code',
                'label' => 'Person Country Code',
                'value' => function($model){
                    if ($model->personCountryCode)
                    {return $model->personCountryCode->country_id;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\app\models\Country::find()->asArray()->all(), 'iso3', 'country_id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Country', 'id' => 'grid-person-search-person_country_code']
            ],
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url:url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-person']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>
