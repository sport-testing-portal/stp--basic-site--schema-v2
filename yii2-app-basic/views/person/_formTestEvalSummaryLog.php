<div class="form-group" id="add-test-eval-summary-log">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'TestEvalSummaryLog',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'test_eval_summary_log_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'team_id' => [
            'label' => 'Team',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Team::find()->orderBy('team_id')->asArray()->all(), 'team_id', 'team_id'),
                'options' => ['placeholder' => 'Choose Team'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'test_eval_type_id' => [
            'label' => 'Test eval type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\TestEvalType::find()->orderBy('test_eval_type_id')->asArray()->all(), 'test_eval_type_id', 'test_eval_type_id'),
                'options' => ['placeholder' => 'Choose Test eval type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'source_event_id' => ['type' => TabularForm::INPUT_TEXT],
        'test_eval_overall_ranking' => ['type' => TabularForm::INPUT_TEXT],
        'test_eval_positional_ranking' => ['type' => TabularForm::INPUT_TEXT],
        'test_eval_total_score' => ['type' => TabularForm::INPUT_TEXT],
        'test_eval_avg_score' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowTestEvalSummaryLog(' . $key . '); return false;', 'id' => 'test-eval-summary-log-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Test Eval Summary Log', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTestEvalSummaryLog()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

