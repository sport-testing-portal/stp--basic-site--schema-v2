<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->eventAttendeeLogs,
        'key' => 'event_attendee_log_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'event_attendee_log_id',
        [
                'attribute' => 'event.event_id',
                'label' => 'Event'
            ],
        [
                'attribute' => 'eventAttendeeType.event_attendee_type_id',
                'label' => 'Event Attendee Type'
            ],
        'event_rsvp_dt',
        'event_attendance_dt',
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'event-attendee-log'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
