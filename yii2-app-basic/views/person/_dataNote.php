<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->notes,
        'key' => 'note_id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        'note_id',
        [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
        [
                'attribute' => 'noteType.note_type_id',
                'label' => 'Note Type'
            ],
        'note',
        'note_tags',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'note'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
