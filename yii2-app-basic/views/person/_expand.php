<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Person'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Address'),
        'content' => $this->render('_dataAddress', [
            'model' => $model,
            'row' => $model->addresses,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Coach'),
        'content' => $this->render('_dataCoach', [
            'model' => $model,
            'row' => $model->coaches,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Event Attendee Log'),
        'content' => $this->render('_dataEventAttendeeLog', [
            'model' => $model,
            'row' => $model->eventAttendeeLogs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Media'),
        'content' => $this->render('_dataMedia', [
            'model' => $model,
            'row' => $model->media,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Note'),
        'content' => $this->render('_dataNote', [
            'model' => $model,
            'row' => $model->notes,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Payment Log'),
        'content' => $this->render('_dataPaymentLog', [
            'model' => $model,
            'row' => $model->paymentLogs,
        ]),
    ],
                                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Person Certification'),
        'content' => $this->render('_dataPersonCertification', [
            'model' => $model,
            'row' => $model->personCertifications,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player'),
        'content' => $this->render('_dataPlayer', [
            'model' => $model,
            'row' => $model->players,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Contact'),
        'content' => $this->render('_dataPlayerContact', [
            'model' => $model,
            'row' => $model->playerContacts,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Player Results Search Log'),
        'content' => $this->render('_dataPlayerResultsSearchLog', [
            'model' => $model,
            'row' => $model->playerResultsSearchLogs,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Search Criteria Template'),
        'content' => $this->render('_dataSearchCriteriaTemplate', [
            'model' => $model,
            'row' => $model->searchCriteriaTemplates,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Search History'),
        'content' => $this->render('_dataSearchHistory', [
            'model' => $model,
            'row' => $model->searchHistories,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Subscription'),
        'content' => $this->render('_dataSubscription', [
            'model' => $model,
            'row' => $model->subscriptions,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Test Eval Summary Log'),
        'content' => $this->render('_dataTestEvalSummaryLog', [
            'model' => $model,
            'row' => $model->testEvalSummaryLogs,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
