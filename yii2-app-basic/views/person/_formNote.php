<div class="form-group" id="add-note">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Note',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'note_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'org_id' => [
            'label' => 'Org',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Org::find()->orderBy('org_id')->asArray()->all(), 'org_id', 'org_id'),
                'options' => ['placeholder' => 'Choose Org'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'note_type_id' => [
            'label' => 'Note type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\NoteType::find()->orderBy('note_type_id')->asArray()->all(), 'note_type_id', 'note_type_id'),
                'options' => ['placeholder' => 'Choose Note type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'note' => ['type' => TabularForm::INPUT_TEXT],
        'note_tags' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowNote(' . $key . '); return false;', 'id' => 'note-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Note', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowNote()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

