<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Person */

$this->title = $model->person_id;
$this->params['breadcrumbs'][] = ['label' => 'People', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="person-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Person'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->person_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->person_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'person_id',
        [
            'attribute' => 'org.org_id',
            'label' => 'Org',
        ],
        [
            'attribute' => 'appUser.username',
            'label' => 'App User',
        ],
        [
            'attribute' => 'personType.person_type_id',
            'label' => 'Person Type',
        ],
        'user_id',
        'person_name_prefix',
        'person_name_first',
        'person_name_middle',
        'person_name_last',
        'person_name_suffix',
        'person_name_full',
        'person_phone_personal',
        'person_email_personal:email',
        'person_phone_work',
        'person_email_work:email',
        'person_position_work',
        [
            'attribute' => 'gender.gender_id',
            'label' => 'Gender',
        ],
        'person_image_headshot_url:url',
        'person_name_nickname',
        'person_date_of_birth',
        'person_height',
        'person_weight',
        'person_tshirt_size',
        'person_high_school__graduation_year',
        'person_college_graduation_year',
        'person_college_commitment_status',
        'person_addr_1',
        'person_addr_2',
        'person_addr_3',
        'person_city',
        'person_postal_code',
        'person_country',
        [
            'attribute' => 'personCountryCode.country_id',
            'label' => 'Person Country Code',
        ],
        'person_state_or_region',
        'org_affiliation_begin_dt',
        'org_affiliation_end_dt',
        'person_website',
        'person_profile_url:url',
        'person_profile_uri',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerAddress->totalCount){
    $gridColumnAddress = [
        ['class' => 'yii\grid\SerialColumn'],
            'address_id',
            [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
                        [
                'attribute' => 'addressType.address_type_id',
                'label' => 'Address Type'
            ],
            'addr1',
            'addr2',
            'addr3',
            'city',
            'state_or_region',
            'postal_code',
            'country',
            'country_code',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAddress,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-address']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Address'),
        ],
        'export' => false,
        'columns' => $gridColumnAddress
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerCoach->totalCount){
    $gridColumnCoach = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'teamCoaches.team_coach_id',
                'label' => 'Coach'
            ],
                        [
                'attribute' => 'coachType.coach_type_id',
                'label' => 'Coach Type'
            ],
            [
                'attribute' => 'coachTeamCoach.team_coach_id',
                'label' => 'Coach Team Coach'
            ],
            [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
            'coach_specialty',
            'coach_certifications',
            'coach_comments',
            'coach_qrcode_uri',
            'coach_info_source_scrape_url:url',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerCoach,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-coach']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Coach'),
        ],
        'export' => false,
        'columns' => $gridColumnCoach
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerEventAttendeeLog->totalCount){
    $gridColumnEventAttendeeLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'event_attendee_log_id',
            [
                'attribute' => 'event.event_id',
                'label' => 'Event'
            ],
                        [
                'attribute' => 'eventAttendeeType.event_attendee_type_id',
                'label' => 'Event Attendee Type'
            ],
            'event_rsvp_dt',
            'event_attendance_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerEventAttendeeLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-event-attendee-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Event Attendee Log'),
        ],
        'export' => false,
        'columns' => $gridColumnEventAttendeeLog
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerMedia->totalCount){
    $gridColumnMedia = [
        ['class' => 'yii\grid\SerialColumn'],
            'media_id',
            [
                'attribute' => 'mediaType.media_type_id',
                'label' => 'Media Type'
            ],
            [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
                        'media_name_original',
            'media_name_unique',
            'media_desc_short',
            'media_desc_long',
            'media_url_local:url',
            'media_url_remote:url',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerMedia,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-media']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Media'),
        ],
        'export' => false,
        'columns' => $gridColumnMedia
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerNote->totalCount){
    $gridColumnNote = [
        ['class' => 'yii\grid\SerialColumn'],
            'note_id',
            [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
                        [
                'attribute' => 'noteType.note_type_id',
                'label' => 'Note Type'
            ],
            'note',
            'note_tags',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerNote,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-note']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Note'),
        ],
        'export' => false,
        'columns' => $gridColumnNote
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPaymentLog->totalCount){
    $gridColumnPaymentLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'payment_log_id',
            [
                'attribute' => 'paymentType.payment_type_id',
                'label' => 'Payment Type'
            ],
            'data_entity_id',
            [
                'attribute' => 'subscription.subscription_id',
                'label' => 'Subscription'
            ],
            [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
                        'payment_amt',
            'payment_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPaymentLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-payment-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Payment Log'),
        ],
        'export' => false,
        'columns' => $gridColumnPaymentLog
    ]);
}
?>

    </div>
    <div class="row">
        <h4>AppUser<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnAppUser = [
        'username',
                'email',
        'firstname',
        'lastname',
        'activation_key',
        'created_on',
        'updated_on',
        'last_visit_on',
        'password_set_on',
        'email_verified',
        'is_active',
        'is_disabled',
        'one_time_password_secret',
        'one_time_password_code',
        'one_time_password_counter',
    ];
    echo DetailView::widget([
        'model' => $model->appUser,
        'attributes' => $gridColumnAppUser    ]);
    ?>
    <div class="row">
        <h4>Country<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnCountry = [
        'country_id',
        'iso2',
        'short_name',
        'long_name',
        'iso3',
        'numcode',
        'un_member',
        'calling_code',
        'cctld',
    ];
    echo DetailView::widget([
        'model' => $model->personCountryCode,
        'attributes' => $gridColumnCountry    ]);
    ?>
    <div class="row">
        <h4>Gender<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnGender = [
        'gender_desc',
        'gender_code',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->gender,
        'attributes' => $gridColumnGender    ]);
    ?>
    <div class="row">
        <h4>Org<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnOrg = [
        'org_type_id',
        'org_level_id',
        'org_name',
        'org_governing_body',
        'org_ncaa_clearing_house_id',
        'org_website_url',
        'org_twitter_url',
        'org_facebook_url',
        'org_phone_main',
        'org_email_main',
        'org_addr1',
        'org_addr2',
        'org_addr3',
        'org_city',
        'org_state_or_region',
        'org_postal_code',
        'org_country_code_iso3',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->org,
        'attributes' => $gridColumnOrg    ]);
    ?>
    <div class="row">
        <h4>PersonType<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnPersonType = [
        'person_type_name',
        'person_type_desc_short',
        'person_type_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model->personType,
        'attributes' => $gridColumnPersonType    ]);
    ?>
    
    <div class="row">
<?php
if($providerPersonCertification->totalCount){
    $gridColumnPersonCertification = [
        ['class' => 'yii\grid\SerialColumn'],
            'person_certification_id',
                        [
                'attribute' => 'personCertificationType.person_certification_type_id',
                'label' => 'Person Certification Type'
            ],
            'person_certification_year',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPersonCertification,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-person-certification']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Person Certification'),
        ],
        'export' => false,
        'columns' => $gridColumnPersonCertification
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayer->totalCount){
    $gridColumnPlayer = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'teamPlayers.team_player_id',
                'label' => 'Player'
            ],
                        [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
            [
                'attribute' => 'playerTeamPlayer.team_player_id',
                'label' => 'Player Team Player'
            ],
            [
                'attribute' => 'playerSportPosition.sport_position_id',
                'label' => 'Player Sport Position'
            ],
            'player_sport_position2_id',
            'player_access_code',
            'player_waiver_minor_dt',
            'player_waiver_adult_dt',
            'player_parent_email:email',
            'player_sport_preference',
            'player_sport_position_preference',
            'player_shot_side_preference',
            'player_dominant_side',
            'player_dominant_foot',
            'player_statistical_highlights',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayer,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayer
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerContact->totalCount){
    $gridColumnPlayerContact = [
        ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'playerContact.player_contact_type_id',
                'label' => 'Player Contact'
            ],
            [
                'attribute' => 'playerContactType.player_contact_type_id',
                'label' => 'Player Contact Type'
            ],
            [
                'attribute' => 'player.player_id',
                'label' => 'Player'
            ],
                        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerContact,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-contact']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Contact'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerContact
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerPlayerResultsSearchLog->totalCount){
    $gridColumnPlayerResultsSearchLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'player_results_search_log_id',
            [
                'attribute' => 'user.username',
                'label' => 'User'
            ],
                        'row_count',
            'gender',
            'age_range_min',
            'age_range_max',
            'first_name',
            'last_name',
            'test_provider',
            'team',
            'position',
            'test_type',
            'test_category',
            'test_description',
            'country',
            'state_province',
            'zip_code',
            'zip_code_radius',
            'city',
            'city_radius',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPlayerResultsSearchLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-player-results-search-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Player Results Search Log'),
        ],
        'export' => false,
        'columns' => $gridColumnPlayerResultsSearchLog
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSearchCriteriaTemplate->totalCount){
    $gridColumnSearchCriteriaTemplate = [
        ['class' => 'yii\grid\SerialColumn'],
            'search_criteria_template_id',
                        'template_name',
            'template_comment',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSearchCriteriaTemplate,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-search-criteria-template']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Search Criteria Template'),
        ],
        'export' => false,
        'columns' => $gridColumnSearchCriteriaTemplate
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSearchHistory->totalCount){
    $gridColumnSearchHistory = [
        ['class' => 'yii\grid\SerialColumn'],
            'search_history_id',
            [
                'attribute' => 'searchCriteriaTemplate.search_criteria_template_id',
                'label' => 'Search Criteria Template'
            ],
                        'user_group_name',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSearchHistory,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-search-history']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Search History'),
        ],
        'export' => false,
        'columns' => $gridColumnSearchHistory
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSubscription->totalCount){
    $gridColumnSubscription = [
        ['class' => 'yii\grid\SerialColumn'],
            'subscription_id',
            [
                'attribute' => 'subscriptionType.subscription_type_id',
                'label' => 'Subscription Type'
            ],
            [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
                        [
                'attribute' => 'subscriptionStatusType.subscription_status_type_id',
                'label' => 'Subscription Status Type'
            ],
            'subscription_begin_dt',
            'subscription_end_dt',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSubscription,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-subscription']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Subscription'),
        ],
        'export' => false,
        'columns' => $gridColumnSubscription
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerTestEvalSummaryLog->totalCount){
    $gridColumnTestEvalSummaryLog = [
        ['class' => 'yii\grid\SerialColumn'],
            'test_eval_summary_log_id',
                        [
                'attribute' => 'team.team_id',
                'label' => 'Team'
            ],
            [
                'attribute' => 'testEvalType.test_eval_type_id',
                'label' => 'Test Eval Type'
            ],
            'source_event_id',
            'test_eval_overall_ranking',
            'test_eval_positional_ranking',
            'test_eval_total_score',
            'test_eval_avg_score',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTestEvalSummaryLog,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-test-eval-summary-log']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Test Eval Summary Log'),
        ],
        'export' => false,
        'columns' => $gridColumnTestEvalSummaryLog
    ]);
}
?>

    </div>
</div>
