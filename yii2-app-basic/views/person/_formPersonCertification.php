<div class="form-group" id="add-person-certification">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'PersonCertification',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'person_certification_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'person_certification_type_id' => [
            'label' => 'Person certification type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\PersonCertificationType::find()->orderBy('person_certification_type_id')->asArray()->all(), 'person_certification_type_id', 'person_certification_type_id'),
                'options' => ['placeholder' => 'Choose Person certification type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'person_certification_year' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowPersonCertification(' . $key . '); return false;', 'id' => 'person-certification-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Person Certification', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowPersonCertification()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

