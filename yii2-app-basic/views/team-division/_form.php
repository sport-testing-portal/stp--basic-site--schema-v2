<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamDivision */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Team', 
        'relID' => 'team', 
        'value' => \yii\helpers\Json::encode($model->teams),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="team-division-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'team_division_id')->textInput(['placeholder' => 'Team Division']) ?>

    <?= $form->field($model, 'team_division_name')->textInput(['maxlength' => true, 'placeholder' => 'Team Division Name']) ?>

    <?= $form->field($model, 'team_division_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Team Division Desc Short']) ?>

    <?= $form->field($model, 'team_division_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Team Division Desc Long']) ?>

    <?= $form->field($model, 'team_division_display_order')->textInput(['placeholder' => 'Team Division Display Order']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('Team'),
            'content' => $this->render('_formTeam', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->teams),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
