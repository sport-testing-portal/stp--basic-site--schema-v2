<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamDivision */

$this->title = 'Create Team Division';
$this->params['breadcrumbs'][] = ['label' => 'Team Division', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-division-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
