<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamDivision */

?>
<div class="team-division-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->team_division_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_division_id',
        'team_division_name',
        'team_division_desc_short',
        'team_division_desc_long',
        'team_division_display_order',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>