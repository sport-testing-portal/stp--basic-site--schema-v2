<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamDivisionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-team-division-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'team_division_id')->textInput(['placeholder' => 'Team Division']) ?>

    <?= $form->field($model, 'team_division_name')->textInput(['maxlength' => true, 'placeholder' => 'Team Division Name']) ?>

    <?= $form->field($model, 'team_division_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Team Division Desc Short']) ?>

    <?= $form->field($model, 'team_division_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Team Division Desc Long']) ?>

    <?= $form->field($model, 'team_division_display_order')->textInput(['placeholder' => 'Team Division Display Order']) ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
