<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeamLeague */

$this->title = 'Update Team League: ' . ' ' . $model->team_league_id;
$this->params['breadcrumbs'][] = ['label' => 'Team League', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->team_league_id, 'url' => ['view', 'id' => $model->team_league_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="team-league-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
