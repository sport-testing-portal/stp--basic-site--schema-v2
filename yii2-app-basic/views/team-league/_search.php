<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TeamLeagueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-team-league-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'team_league_id')->textInput(['placeholder' => 'Team League']) ?>

    <?= $form->field($model, 'team_league_name')->textInput(['maxlength' => true, 'placeholder' => 'Team League Name']) ?>

    <?= $form->field($model, 'team_league_desc_short')->textInput(['maxlength' => true, 'placeholder' => 'Team League Desc Short']) ?>

    <?= $form->field($model, 'team_league_desc_long')->textInput(['maxlength' => true, 'placeholder' => 'Team League Desc Long']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
