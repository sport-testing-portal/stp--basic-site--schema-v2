<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamLeague */

$this->title = $model->team_league_id;
$this->params['breadcrumbs'][] = ['label' => 'Team League', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-league-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Team League'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->team_league_id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->team_league_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_league_id',
        'team_league_name',
        'team_league_desc_short',
        'team_league_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerTeam->totalCount){
    $gridColumnTeam = [
        ['class' => 'yii\grid\SerialColumn'],
            'team_id',
            [
                'attribute' => 'org.org_id',
                'label' => 'Org'
            ],
            [
                'attribute' => 'school.school_id',
                'label' => 'School'
            ],
            [
                'attribute' => 'sport.sport_id',
                'label' => 'Sport'
            ],
            [
                'attribute' => 'camp.camp_id',
                'label' => 'Camp'
            ],
            [
                'attribute' => 'gender.gender_id',
                'label' => 'Gender'
            ],
            [
                'attribute' => 'ageGroup.age_group_id',
                'label' => 'Age Group'
            ],
            'team_name',
            'team_gender',
            [
                'attribute' => 'teamDivision.team_division_id',
                'label' => 'Team Division'
            ],
                        'team_division',
            'team_league',
            'organizational_level',
            'team_governing_body',
            'team_age_group',
            'team_website_url:url',
            'team_schedule_url:url',
            'team_schedule_uri',
            'team_statistical_highlights',
            'team_competition_season_id',
            'team_competition_season',
            'team_city',
            'team_state_id',
            [
                'attribute' => 'teamCountry.country_id',
                'label' => 'Team Country'
            ],
            'team_wins',
            'team_losses',
            'team_draws',
            ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTeam,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-team']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Team'),
        ],
        'export' => false,
        'columns' => $gridColumnTeam
    ]);
}
?>

    </div>
</div>
