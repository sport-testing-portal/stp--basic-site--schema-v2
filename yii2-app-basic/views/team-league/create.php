<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamLeague */

$this->title = 'Create Team League';
$this->params['breadcrumbs'][] = ['label' => 'Team League', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-league-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
