<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamLeague */

?>
<div class="team-league-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->team_league_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'team_league_id',
        'team_league_name',
        'team_league_desc_short',
        'team_league_desc_long',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>