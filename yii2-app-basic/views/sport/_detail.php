<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Sport */

?>
<div class="sport-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->sport_id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'sport_id',
        'sport_name',
        'sport_desc_short',
        'sport_desc_long',
        'high_school_yn',
        'ncaa_yn',
        'olympic_yn',
        'xgame_sport_yn',
        'ncaa_sport_name',
        'olympic_sport_name',
        'xgame_sport_name',
        'ncaa_sport_type',
        'ncaa_sport_season_male',
        'ncaa_sport_season_female',
        'ncaa_sport_season_coed',
        'olympic_sport_season',
        'xgame_sport_season',
        'olympic_sport_gender',
        'xgame_sport_gender',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>