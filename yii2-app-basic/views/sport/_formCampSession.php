<div class="form-group" id="add-camp-session">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'CampSession',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'camp_session_id' => ['type' => TabularForm::INPUT_HIDDEN],
        'camp_id' => [
            'label' => 'Camp',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Camp::find()->orderBy('camp_id')->asArray()->all(), 'camp_id', 'camp_id'),
                'options' => ['placeholder' => 'Choose Camp'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'gender_id' => [
            'label' => 'Gender',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Gender::find()->orderBy('gender_id')->asArray()->all(), 'gender_id', 'gender_id'),
                'options' => ['placeholder' => 'Choose Gender'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'address_id' => [
            'label' => 'Address',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Address::find()->orderBy('address_id')->asArray()->all(), 'address_id', 'address_id'),
                'options' => ['placeholder' => 'Choose Address'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'season_id' => [
            'label' => 'Season',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\app\models\Season::find()->orderBy('season')->asArray()->all(), 'season_id', 'season'),
                'options' => ['placeholder' => 'Choose Season'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'camp_session_location' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_url' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_ages' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_skill_level' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_type' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_begin_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Camp Session Begin Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'camp_session_end_dt' => ['type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
                'saveFormat' => 'php:Y-m-d H:i:s',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Camp Session End Dt',
                        'autoclose' => true,
                    ]
                ],
            ]
        ],
        'camp_session_specialty' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_scholarships_available_yn' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_description' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_cost_regular_residential' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_cost_regular_commuter' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_cost_regular_day' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_cost_early_residential' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_cost_early_commuter' => ['type' => TabularForm::INPUT_TEXT],
        'camp_session_cost_early_day' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'columnOptions' => ['hidden'=>true]],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowCampSession(' . $key . '); return false;', 'id' => 'camp-session-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Camp Session', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowCampSession()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

