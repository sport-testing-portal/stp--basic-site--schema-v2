<?php

namespace app\models;

use Yii;
use \app\models\base\CampSessionPlayerEval as BaseCampSessionPlayerEval;

/**
 * This is the model class for table "camp_session_player_eval".
 */
class CampSessionPlayerEval extends BaseCampSessionPlayerEval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_session_id', 'coach_id', 'player_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['camp_session_coach_comment', 'camp_session_player_comment'], 'string', 'max' => 450],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_session_player_eval_id' => 'Camp Session Player Eval ID',
            'camp_session_id' => 'Camp Session ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'camp_session_coach_comment' => 'Camp Session Coach Comment',
            'camp_session_player_comment' => 'Camp Session Player Comment',
            'lock' => 'Lock',
        ];
    }
}
