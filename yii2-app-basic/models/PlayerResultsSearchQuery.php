<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerResultsSearch]].
 *
 * @see PlayerResultsSearch
 */
class PlayerResultsSearchQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerResultsSearch[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerResultsSearch|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
