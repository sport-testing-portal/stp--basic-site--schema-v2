<?php

namespace app\models;

use Yii;
use \app\models\base\ImportFile as BaseImportFile;

/**
 * This is the model class for table "import_file".
 */
class ImportFile extends BaseImportFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['import_file_source_file_date', 'import_file_target_file_date', 'created_at', 'updated_at'], 'safe'],
            [['import_file_source_file_size', 'import_file_target_file_size', 'created_by', 'updated_by'], 'integer'],
            [['import_file_source_file_name', 'import_file_target_file_name'], 'string', 'max' => 150],
            [['import_file_source_file_sha256', 'import_file_target_file_sha256'], 'string', 'max' => 64],
            [['import_file_source_file_password', 'import_file_target_file_password'], 'string', 'max' => 256],
            [['import_file_description_short'], 'string', 'max' => 45],
            [['import_file_description_long'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'import_file_id' => 'Import File ID',
            'import_file_source_file_name' => 'Import File Source File Name',
            'import_file_source_file_date' => 'Import File Source File Date',
            'import_file_source_file_size' => 'Import File Source File Size',
            'import_file_source_file_sha256' => 'Import File Source File Sha256',
            'import_file_source_file_password' => 'Import File Source File Password',
            'import_file_target_file_name' => 'Import File Target File Name',
            'import_file_target_file_date' => 'Import File Target File Date',
            'import_file_target_file_size' => 'Import File Target File Size',
            'import_file_target_file_sha256' => 'Import File Target File Sha256',
            'import_file_target_file_password' => 'Import File Target File Password',
            'import_file_description_short' => 'Import File Description Short',
            'import_file_description_long' => 'Import File Description Long',
        ];
    }
}
