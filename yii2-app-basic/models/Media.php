<?php

namespace app\models;

use Yii;
use \app\models\base\Media as BaseMedia;

/**
 * This is the model class for table "media".
 */
class Media extends BaseMedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['media_type_id', 'org_id', 'person_id'], 'required'],
            [['media_type_id', 'org_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['media_name_original', 'media_name_unique', 'media_desc_long'], 'string', 'max' => 255],
            [['media_desc_short'], 'string', 'max' => 45],
            [['media_url_local', 'media_url_remote'], 'string', 'max' => 245],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'media_id' => 'Media ID',
            'media_type_id' => 'Media Type ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'media_name_original' => 'Media Name Original',
            'media_name_unique' => 'Media Name Unique',
            'media_desc_short' => 'Media Desc Short',
            'media_desc_long' => 'Media Desc Long',
            'media_url_local' => 'Media Url Local',
            'media_url_remote' => 'Media Url Remote',
        ];
    }
}
