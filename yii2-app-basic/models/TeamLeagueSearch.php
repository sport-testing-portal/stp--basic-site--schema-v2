<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeamLeague;

/**
 * app\models\TeamLeagueSearch represents the model behind the search form about `app\models\TeamLeague`.
 */
 class TeamLeagueSearch extends TeamLeague
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_league_id', 'created_by', 'updated_by'], 'integer'],
            [['team_league_name', 'team_league_desc_short', 'team_league_desc_long', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamLeague::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'team_league_id' => $this->team_league_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'team_league_name', $this->team_league_name])
            ->andFilterWhere(['like', 'team_league_desc_short', $this->team_league_desc_short])
            ->andFilterWhere(['like', 'team_league_desc_long', $this->team_league_desc_long])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
