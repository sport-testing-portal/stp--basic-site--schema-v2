<?php

namespace app\models;

use Yii;
use \app\models\base\EventCombine as BaseEventCombine;

/**
 * This is the model class for table "event_combine".
 */
class EventCombine extends BaseEventCombine
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['event_id', 'event_combine_name_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_combine_id' => 'Event Combine ID',
            'event_id' => 'Event ID',
            'event_combine_name_id' => 'Event Combine Name ID',
            'lock' => 'Lock',
        ];
    }
}
