<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ZipCode]].
 *
 * @see ZipCode
 */
class ZipCodeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ZipCode[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ZipCode|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
