<?php

namespace app\models;

use Yii;
use \app\models\base\EventAttendeeType as BaseEventAttendeeType;

/**
 * This is the model class for table "event_attendee_type".
 */
class EventAttendeeType extends BaseEventAttendeeType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['event_attendee_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['event_attendee_type_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['event_attendee_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_attendee_type_id' => 'Event Attendee Type ID',
            'event_attendee_type_name' => 'Event Attendee Type Name',
            'lock' => 'Lock',
        ];
    }
}
