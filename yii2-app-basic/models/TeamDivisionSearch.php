<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeamDivision;

/**
 * app\models\TeamDivisionSearch represents the model behind the search form about `app\models\TeamDivision`.
 */
 class TeamDivisionSearch extends TeamDivision
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_division_id', 'team_division_display_order', 'created_by', 'updated_by'], 'integer'],
            [['team_division_name', 'team_division_desc_short', 'team_division_desc_long', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamDivision::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'team_division_id' => $this->team_division_id,
            'team_division_display_order' => $this->team_division_display_order,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'team_division_name', $this->team_division_name])
            ->andFilterWhere(['like', 'team_division_desc_short', $this->team_division_desc_short])
            ->andFilterWhere(['like', 'team_division_desc_long', $this->team_division_desc_long])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
