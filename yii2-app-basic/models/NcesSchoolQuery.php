<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NcesSchool]].
 *
 * @see NcesSchool
 */
class NcesSchoolQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NcesSchool[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NcesSchool|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
