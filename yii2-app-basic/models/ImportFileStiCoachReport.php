<?php

namespace app\models;

use Yii;
use \app\models\base\ImportFileStiCoachReport as BaseImportFileStiCoachReport;

/**
 * This is the model class for table "import_file__sti_coach_report".
 */
class ImportFileStiCoachReport extends BaseImportFileStiCoachReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['import_file_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['field_0001', 'field_0002', 'field_0003', 'field_0004', 'field_0005', 'field_0006', 'field_0007', 'field_0008', 'field_0009', 'field_0010', 'field_0011', 'field_0012', 'field_0013', 'field_0014', 'field_0015', 'field_0016', 'field_0017', 'field_0018', 'field_0019', 'field_0020', 'field_0021', 'field_0022', 'field_0023', 'field_0024', 'field_0025', 'field_0026', 'field_0027', 'field_0028', 'field_0029', 'field_0030', 'field_0031', 'field_0032', 'field_0033', 'field_0034', 'field_0035', 'field_0036'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'import_file__sti_coach_report_id' => 'Import File  Sti Coach Report ID',
            'import_file_id' => 'Import File ID',
            'field_0001' => 'Field 0001',
            'field_0002' => 'Field 0002',
            'field_0003' => 'Field 0003',
            'field_0004' => 'Field 0004',
            'field_0005' => 'Field 0005',
            'field_0006' => 'Field 0006',
            'field_0007' => 'Field 0007',
            'field_0008' => 'Field 0008',
            'field_0009' => 'Field 0009',
            'field_0010' => 'Field 0010',
            'field_0011' => 'Field 0011',
            'field_0012' => 'Field 0012',
            'field_0013' => 'Field 0013',
            'field_0014' => 'Field 0014',
            'field_0015' => 'Field 0015',
            'field_0016' => 'Field 0016',
            'field_0017' => 'Field 0017',
            'field_0018' => 'Field 0018',
            'field_0019' => 'Field 0019',
            'field_0020' => 'Field 0020',
            'field_0021' => 'Field 0021',
            'field_0022' => 'Field 0022',
            'field_0023' => 'Field 0023',
            'field_0024' => 'Field 0024',
            'field_0025' => 'Field 0025',
            'field_0026' => 'Field 0026',
            'field_0027' => 'Field 0027',
            'field_0028' => 'Field 0028',
            'field_0029' => 'Field 0029',
            'field_0030' => 'Field 0030',
            'field_0031' => 'Field 0031',
            'field_0032' => 'Field 0032',
            'field_0033' => 'Field 0033',
            'field_0034' => 'Field 0034',
            'field_0035' => 'Field 0035',
            'field_0036' => 'Field 0036',
        ];
    }
}
