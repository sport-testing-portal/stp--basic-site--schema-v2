<?php

namespace app\models;

use Yii;
use \app\models\base\VwCoach as BaseVwCoach;

/**
 * This is the model class for table "vwCoach".
 */
class VwCoach extends BaseVwCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'person_id', 'org_id', 'coach_id', 'team_coach_id', 'app_user_id', 'person_type_id', 'coach_type_id', 'coach_team_coach_id', 'org_type_id', 'team_id', 'gender_id', 'person_weight', 'created_by', 'updated_by', 'coach_created_by', 'coach_updated_by', 'team_coach_coach_type_id', 'team_coach_created_by', 'team_coach_updated_by', 'team_org_id', 'team_school_id', 'team_sport_id', 'team_camp_id', 'team_gender_id', 'team_age_group_id', 'team_wins', 'team_losses', 'team_draws'], 'integer'],
            [['person_date_of_birth', 'org_affiliation_begin_dt', 'org_affiliation_end_dt', 'created_at', 'updated_at', 'team_coach_begin_dt', 'team_coach_end_dt', 'team_coach_created_at', 'team_coach_updated_at'], 'safe'],
            [['person_type_name', 'org_type_name', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region', 'org_state_or_region', 'team_age_group_depreciated'], 'string', 'max' => 45],
            [['coach_type_name', 'team_name', 'person_city', 'org_email_main', 'org_city', 'team_coach_primary_position'], 'string', 'max' => 75],
            [['org_name', 'person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'coach_info_source_scrape_url', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['person_name_prefix', 'person_postal_code', 'org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['person_name_full'], 'string', 'max' => 90],
            [['gender_code', 'person_height'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['person_email_personal', 'person_email_work'], 'string', 'max' => 60],
            [['person_tshirt_size', 'team_division'], 'string', 'max' => 10],
            [['person_country_code', 'org_country_code_iso3'], 'string', 'max' => 3],
            [['person_created_by_username', 'person_updated_by_username', 'coach_created_by_username', 'coach_updated_by_username'], 'string', 'max' => 255],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri', 'org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['team_gender'], 'string', 'max' => 1],
            [['team_winlossdraw'], 'string', 'max' => 35],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'user_id' => 'User ID',
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'coach_id' => 'Coach ID',
            'team_coach_id' => 'Team Coach ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'coach_type_id' => 'Coach Type ID',
            'coach_type_name' => 'Coach Type Name',
            'coach_team_coach_id' => 'Coach Team Coach ID',
            'org_name' => 'Org Name',
            'org_type_id' => 'Org Type ID',
            'org_type_name' => 'Org Type Name',
            'team_name' => 'Team Name',
            'team_id' => 'Team ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'gender_id' => 'Gender ID',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'org_affiliation_begin_dt' => 'Org Affiliation Begin Dt',
            'org_affiliation_end_dt' => 'Org Affiliation End Dt',
            'person_created_by_username' => 'Person Created By Username',
            'person_updated_by_username' => 'Person Updated By Username',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'coach_created_by_username' => 'Coach Created By Username',
            'coach_created_by' => 'Coach Created By',
            'coach_updated_by_username' => 'Coach Updated By Username',
            'coach_updated_by' => 'Coach Updated By',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
            'team_coach_primary_position' => 'Team Coach Primary Position',
            'team_coach_coach_type_id' => 'Team Coach Coach Type ID',
            'team_coach_begin_dt' => 'Team Coach Begin Dt',
            'team_coach_end_dt' => 'Team Coach End Dt',
            'team_coach_created_at' => 'Team Coach Created At',
            'team_coach_updated_at' => 'Team Coach Updated At',
            'team_coach_created_by' => 'Team Coach Created By',
            'team_coach_updated_by' => 'Team Coach Updated By',
            'team_org_id' => 'Team Org ID',
            'team_school_id' => 'Team School ID',
            'team_sport_id' => 'Team Sport ID',
            'team_camp_id' => 'Team Camp ID',
            'team_gender_id' => 'Team Gender ID',
            'team_age_group_id' => 'Team Age Group ID',
            'team_gender' => 'Team Gender',
            'team_division' => 'Team Division',
            'team_age_group_depreciated' => 'Team Age Group Depreciated',
            'team_wins' => 'Team Wins',
            'team_losses' => 'Team Losses',
            'team_draws' => 'Team Draws',
            'team_winlossdraw' => 'Team Winlossdraw',
        ];
    }
}
