<?php

namespace app\models;

use Yii;
use \app\models\base\OrgLevel as BaseOrgLevel;

/**
 * This is the model class for table "org_level".
 */
class OrgLevel extends BaseOrgLevel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_type_id', 'org_level_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_level_name', 'org_level_desc_short'], 'string', 'max' => 75],
            [['org_level_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_level_id' => 'Org Level ID',
            'org_type_id' => 'Org Type ID',
            'org_level_name' => 'Org Level Name',
            'org_level_desc_short' => 'Org Level Desc Short',
            'org_level_desc_long' => 'Org Level Desc Long',
            'org_level_display_order' => 'Org Level Display Order',
            'lock' => 'Lock',
        ];
    }
}
