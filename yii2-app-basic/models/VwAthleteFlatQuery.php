<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwAthleteFlat]].
 *
 * @see VwAthleteFlat
 */
class VwAthleteFlatQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwAthleteFlat[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwAthleteFlat|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
