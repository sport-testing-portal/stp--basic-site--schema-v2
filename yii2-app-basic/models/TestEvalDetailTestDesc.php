<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalDetailTestDesc as BaseTestEvalDetailTestDesc;

/**
 * This is the model class for table "test_eval_detail_test_desc".
 */
class TestEvalDetailTestDesc extends BaseTestEvalDetailTestDesc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_eval_type_id', 'test_eval_detail_test_desc_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['test_eval_detail_test_desc', 'units', 'format', 'qualifier'], 'string', 'max' => 45],
            [['rating_bias'], 'string', 'max' => 5],
            [['splits', 'checkpoints', 'attempts'], 'string', 'max' => 4],
            [['backend_calculation', 'metric_equivalent'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_detail_test_desc_id' => 'Test Eval Detail Test Desc ID',
            'test_eval_type_id' => 'Test Eval Type ID',
            'test_eval_detail_test_desc' => 'Test Eval Detail Test Desc',
            'test_eval_detail_test_desc_display_order' => 'Test Eval Detail Test Desc Display Order',
            'rating_bias' => 'Rating Bias',
            'units' => 'Units',
            'format' => 'Format',
            'splits' => 'Splits',
            'checkpoints' => 'Checkpoints',
            'attempts' => 'Attempts',
            'qualifier' => 'Qualifier',
            'backend_calculation' => 'Backend Calculation',
            'metric_equivalent' => 'Metric Equivalent',
            'lock' => 'Lock',
        ];
    }
}
