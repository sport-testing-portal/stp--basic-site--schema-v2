<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__codebase".
 *
 * @property integer $codebase_id
 * @property string $file_name
 * @property string $file_path_uri
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\MetadataCodebaseCecStatus[] $metadataCodebaseCecStatuses
 */
class MetadataCodebase extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'metadataCodebaseCecStatuses'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['file_name'], 'string', 'max' => 75],
            [['file_path_uri'], 'string', 'max' => 175],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__codebase';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'codebase_id' => 'Codebase ID',
            'file_name' => 'File Name',
            'file_path_uri' => 'File Path Uri',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMetadataCodebaseCecStatuses()
    {
        return $this->hasMany(\app\models\MetadataCodebaseCecStatus::className(), ['codebase_id' => 'codebase_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataCodebaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataCodebaseQuery(get_called_class());
    }
}
