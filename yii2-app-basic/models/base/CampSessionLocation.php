<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "camp_session_location".
 *
 * @property integer $camp_session_location_id
 * @property integer $camp_session_id
 * @property integer $address_id
 * @property integer $season_id
 * @property integer $gender_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Address $address
 * @property \app\models\CampSession $campSession
 * @property \app\models\Gender $gender
 * @property \app\models\Season $season
 */
class CampSessionLocation extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'address',
            'campSession',
            'gender',
            'season'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['camp_session_id', 'address_id', 'season_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'camp_session_location';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'camp_session_location_id' => 'Camp Session Location ID',
            'camp_session_id' => 'Camp Session ID',
            'address_id' => 'Address ID',
            'season_id' => 'Season ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(\app\models\Address::className(), ['address_id' => 'address_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSession()
    {
        return $this->hasOne(\app\models\CampSession::className(), ['camp_session_id' => 'camp_session_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(\app\models\Gender::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeason()
    {
        return $this->hasOne(\app\models\Season::className(), ['season_id' => 'season_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\CampSessionLocationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\CampSessionLocationQuery(get_called_class());
    }
}
