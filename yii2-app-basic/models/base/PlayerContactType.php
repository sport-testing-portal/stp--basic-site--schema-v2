<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_contact_type".
 *
 * @property integer $player_contact_type_id
 * @property string $player_contact_type_name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PlayerContact $playerContact
 * @property \app\models\PlayerContact[] $playerContacts
 */
class PlayerContactType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'playerContact',
            'playerContacts'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_contact_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['player_contact_type_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['player_contact_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_contact_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_contact_type_id' => 'Player Contact Type ID',
            'player_contact_type_name' => 'Player Contact Type Name',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerContact()
    {
        return $this->hasOne(\app\models\PlayerContact::className(), ['player_contact_id' => 'player_contact_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerContacts()
    {
        return $this->hasMany(\app\models\PlayerContact::className(), ['player_contact_type_id' => 'player_contact_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerContactTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerContactTypeQuery(get_called_class());
    }
}
