<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwCoachTeams6".
 *
 * @property string $person_name_full
 * @property double $person_age
 * @property string $team_name
 * @property string $age_group_name
 * @property string $person_city
 * @property string $person_state_or_region
 * @property string $gsm_staff
 * @property integer $coach_id
 * @property integer $coach_person_id
 * @property integer $coach_age_group_id
 * @property integer $coach_team_coach_id
 * @property string $coach_specialty
 * @property string $coach_certifications
 * @property string $coach_qrcode_uri
 * @property string $coach_info_source_scrape_url
 * @property integer $person_id
 * @property integer $person_org_id
 * @property integer $person_app_user_id
 * @property integer $person_person_type_id
 * @property integer $person_user_id
 * @property string $person_name_prefix
 * @property string $person_name_first
 * @property string $person_name_middle
 * @property string $person_name_last
 * @property string $person_name_suffix
 * @property string $person_phone_personal
 * @property string $person_email_personal
 * @property string $person_phone_work
 * @property string $person_email_work
 * @property string $person_position_work
 * @property integer $person_gender_id
 * @property string $person_image_headshot_url
 * @property string $person_name_nickname
 * @property string $person_date_of_birth
 * @property string $person_dob_eng
 * @property string $person_bday_long
 * @property string $person_height
 * @property string $person_height_imperial
 * @property integer $person_weight
 * @property double $person_bmi
 * @property string $person_tshirt_size
 * @property string $person_addr_1
 * @property string $person_addr_2
 * @property string $person_addr_3
 * @property string $person_postal_code
 * @property string $person_country
 * @property string $person_country_code
 * @property string $person_profile_url
 * @property string $person_profile_uri
 * @property integer $person_high_school__graduation_year
 * @property integer $person_college_graduation_year
 * @property string $person_college_commitment_status
 * @property integer $person_type_id
 * @property string $person_type_name
 * @property string $person_type_desc_short
 * @property string $person_type_desc_long
 * @property integer $gender_id
 * @property string $gender_desc
 * @property string $gender_code
 * @property integer $org_id
 * @property integer $org_org_type_id
 * @property integer $org_org_level_id
 * @property string $org_name
 * @property string $org_type_name
 * @property string $org_website_url
 * @property string $org_twitter_url
 * @property string $org_facebook_url
 * @property string $org_phone_main
 * @property string $org_email_main
 * @property string $org_addr1
 * @property string $org_addr2
 * @property string $org_addr3
 * @property string $org_city
 * @property string $org_state_or_region
 * @property string $org_postal_code
 * @property string $org_country_code_iso3
 * @property integer $team_org_org_type_id
 * @property string $team_org_org_type_name
 * @property integer $team_org_basetable_org_type_id
 * @property string $team_org_basetable_org_type_name
 * @property integer $team_org_org_id
 * @property integer $team_org_org_level_id
 * @property string $team_org_org_level_name
 * @property integer $team_org_basetable_org_level_id
 * @property string $team_org_basetable_org_level_name
 * @property string $team_org_org_name
 * @property string $team_org_org_website_url
 * @property string $team_org_org_twitter_url
 * @property string $team_org_org_facebook_url
 * @property string $team_org_org_phone_main
 * @property string $team_org_org_email_main
 * @property string $team_org_org_addr1
 * @property string $team_org_org_addr2
 * @property string $team_org_org_addr3
 * @property string $team_org_org_city
 * @property string $team_org_org_state_or_region
 * @property string $team_org_org_postal_code
 * @property string $team_org_org_country_code_iso3
 * @property string $team_org_org_governing_body
 * @property string $team_org_org_ncaa_clearing_house_id
 * @property integer $team_org_school_conference_id_main
 * @property integer $team_org_school_conference_id_female
 * @property integer $team_org_school_conference_id_male
 * @property integer $team_org_school_school_ipeds_id
 * @property string $team_org_school_conference_name
 * @property string $team_org_school_ncaa_division
 * @property integer $team_org_school_conf_basetable_conference_id
 * @property string $team_org_school_conf_basetable_conference_name
 * @property string $team_org_school_conf_basetable_ncaa_division
 * @property integer $team_coach_id
 * @property integer $team_coach_team_id
 * @property integer $team_coach_coach_type_id
 * @property string $team_coach_primary_position_name
 * @property string $team_coach_begin_dt
 * @property string $team_coach_end_dt
 * @property integer $team_coach_coach_id
 * @property string $team_coach_created_at
 * @property string $team_coach_updated_at
 * @property integer $team_org_id
 * @property integer $team_school_id
 * @property integer $team_sport_id
 * @property string $team_sport_name
 * @property integer $team_camp_id
 * @property integer $team_gender_id
 * @property string $team_gender
 * @property integer $team_age_group_id
 * @property string $team_age_group
 * @property integer $team_competition_season_id
 * @property string $team_competition_season
 * @property integer $team_league_id
 * @property string $team_league
 * @property string $team_league_name
 * @property string $organizational_level
 * @property string $team_governing_body
 * @property string $team_website_url
 * @property string $team_schedule_url
 * @property string $team_schedule_uri
 * @property string $team_statistical_highlights
 * @property integer $team_team_division_id
 * @property string $team_division
 * @property string $team_division_name
 * @property string $team_city
 * @property integer $team_state_id
 * @property integer $team_country_id
 * @property integer $team_wins
 * @property integer $team_losses
 * @property integer $team_draws
 * @property string $team_wins_losses_draws
 * @property integer $team_basetable_team_league_id
 * @property string $team_basetable_team_league_name
 * @property integer $team_basetable_team_division_id
 * @property string $team_basetable_team_division_name
 */
class VwCoachTeams6 extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_age', 'person_bmi'], 'number'],
            [['coach_id', 'coach_person_id', 'coach_age_group_id', 'coach_team_coach_id', 'person_id', 'person_org_id', 'person_app_user_id', 'person_person_type_id', 'person_user_id', 'person_gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'person_type_id', 'gender_id', 'org_id', 'org_org_type_id', 'org_org_level_id', 'team_org_org_type_id', 'team_org_basetable_org_type_id', 'team_org_org_id', 'team_org_org_level_id', 'team_org_basetable_org_level_id', 'team_org_school_conference_id_main', 'team_org_school_conference_id_female', 'team_org_school_conference_id_male', 'team_org_school_school_ipeds_id', 'team_org_school_conf_basetable_conference_id', 'team_coach_id', 'team_coach_team_id', 'team_coach_coach_type_id', 'team_coach_coach_id', 'team_org_id', 'team_school_id', 'team_sport_id', 'team_camp_id', 'team_gender_id', 'team_age_group_id', 'team_competition_season_id', 'team_league_id', 'team_team_division_id', 'team_state_id', 'team_country_id', 'team_wins', 'team_losses', 'team_draws', 'team_basetable_team_league_id', 'team_basetable_team_division_id'], 'integer'],
            [['person_date_of_birth', 'team_coach_begin_dt', 'team_coach_end_dt', 'team_coach_created_at', 'team_coach_updated_at'], 'safe'],
            [['person_type_name'], 'required'],
            [['person_name_full', 'person_profile_url', 'team_website_url'], 'string', 'max' => 90],
            [['team_name', 'person_city', 'org_email_main', 'org_city', 'team_org_org_level_name', 'team_org_basetable_org_level_name', 'team_org_org_email_main', 'team_org_org_city', 'team_org_school_conference_name', 'team_org_school_conf_basetable_conference_name', 'team_league', 'team_schedule_url', 'team_division_name', 'team_city', 'team_basetable_team_division_name'], 'string', 'max' => 75],
            [['age_group_name', 'person_state_or_region', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_height_imperial', 'person_country', 'person_type_name', 'person_type_desc_short', 'person_type_desc_long', 'org_type_name', 'org_state_or_region', 'team_org_org_type_name', 'team_org_basetable_org_type_name', 'team_org_org_state_or_region', 'team_org_org_governing_body', 'team_coach_primary_position_name', 'team_sport_name', 'team_age_group', 'team_league_name', 'organizational_level', 'team_governing_body', 'team_basetable_team_league_name'], 'string', 'max' => 45],
            [['gsm_staff'], 'string', 'max' => 65],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri', 'org_website_url', 'org_twitter_url', 'org_facebook_url', 'team_org_org_website_url', 'team_org_org_twitter_url', 'team_org_org_facebook_url'], 'string', 'max' => 150],
            [['coach_info_source_scrape_url', 'person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'org_name', 'org_addr1', 'org_addr2', 'org_addr3', 'team_org_org_name', 'team_org_org_addr1', 'team_org_org_addr2', 'team_org_org_addr3'], 'string', 'max' => 100],
            [['person_name_prefix', 'person_postal_code', 'person_college_commitment_status', 'org_phone_main', 'org_postal_code', 'team_org_org_phone_main', 'team_org_org_postal_code', 'team_org_org_ncaa_clearing_house_id'], 'string', 'max' => 25],
            [['person_email_personal', 'person_email_work', 'person_profile_uri', 'team_competition_season', 'team_schedule_uri'], 'string', 'max' => 60],
            [['person_dob_eng', 'person_tshirt_size', 'team_division'], 'string', 'max' => 10],
            [['person_bday_long'], 'string', 'max' => 69],
            [['person_height', 'gender_code', 'team_org_school_ncaa_division', 'team_org_school_conf_basetable_ncaa_division'], 'string', 'max' => 5],
            [['person_country_code', 'org_country_code_iso3', 'team_org_org_country_code_iso3'], 'string', 'max' => 3],
            [['gender_desc'], 'string', 'max' => 30],
            [['team_gender'], 'string', 'max' => 1],
            [['team_statistical_highlights'], 'string', 'max' => 300],
            [['team_wins_losses_draws'], 'string', 'max' => 35],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwCoachTeams6';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_name_full' => 'Person Name Full',
            'person_age' => 'Person Age',
            'team_name' => 'Team Name',
            'age_group_name' => 'Age Group Name',
            'person_city' => 'Person City',
            'person_state_or_region' => 'Person State Or Region',
            'gsm_staff' => 'Gsm Staff',
            'coach_id' => 'Coach ID',
            'coach_person_id' => 'Coach Person ID',
            'coach_age_group_id' => 'Coach Age Group ID',
            'coach_team_coach_id' => 'Coach Team Coach ID',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'person_id' => 'Person ID',
            'person_org_id' => 'Person Org ID',
            'person_app_user_id' => 'Person App User ID',
            'person_person_type_id' => 'Person Person Type ID',
            'person_user_id' => 'Person User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_gender_id' => 'Person Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_dob_eng' => 'Person Dob Eng',
            'person_bday_long' => 'Person Bday Long',
            'person_height' => 'Person Height',
            'person_height_imperial' => 'Person Height Imperial',
            'person_weight' => 'Person Weight',
            'person_bmi' => 'Person Bmi',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_profile_url' => 'Person Profile Url',
            'person_profile_uri' => 'Person Profile Uri',
            'person_high_school__graduation_year' => 'Person High School  Graduation Year',
            'person_college_graduation_year' => 'Person College Graduation Year',
            'person_college_commitment_status' => 'Person College Commitment Status',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'person_type_desc_short' => 'Person Type Desc Short',
            'person_type_desc_long' => 'Person Type Desc Long',
            'gender_id' => 'Gender ID',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'org_id' => 'Org ID',
            'org_org_type_id' => 'Org Org Type ID',
            'org_org_level_id' => 'Org Org Level ID',
            'org_name' => 'Org Name',
            'org_type_name' => 'Org Type Name',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
            'team_org_org_type_id' => 'Team Org Org Type ID',
            'team_org_org_type_name' => 'Team Org Org Type Name',
            'team_org_basetable_org_type_id' => 'Team Org Basetable Org Type ID',
            'team_org_basetable_org_type_name' => 'Team Org Basetable Org Type Name',
            'team_org_org_id' => 'Team Org Org ID',
            'team_org_org_level_id' => 'Team Org Org Level ID',
            'team_org_org_level_name' => 'Team Org Org Level Name',
            'team_org_basetable_org_level_id' => 'Team Org Basetable Org Level ID',
            'team_org_basetable_org_level_name' => 'Team Org Basetable Org Level Name',
            'team_org_org_name' => 'Team Org Org Name',
            'team_org_org_website_url' => 'Team Org Org Website Url',
            'team_org_org_twitter_url' => 'Team Org Org Twitter Url',
            'team_org_org_facebook_url' => 'Team Org Org Facebook Url',
            'team_org_org_phone_main' => 'Team Org Org Phone Main',
            'team_org_org_email_main' => 'Team Org Org Email Main',
            'team_org_org_addr1' => 'Team Org Org Addr1',
            'team_org_org_addr2' => 'Team Org Org Addr2',
            'team_org_org_addr3' => 'Team Org Org Addr3',
            'team_org_org_city' => 'Team Org Org City',
            'team_org_org_state_or_region' => 'Team Org Org State Or Region',
            'team_org_org_postal_code' => 'Team Org Org Postal Code',
            'team_org_org_country_code_iso3' => 'Team Org Org Country Code Iso3',
            'team_org_org_governing_body' => 'Team Org Org Governing Body',
            'team_org_org_ncaa_clearing_house_id' => 'Team Org Org Ncaa Clearing House ID',
            'team_org_school_conference_id_main' => 'Team Org School Conference Id Main',
            'team_org_school_conference_id_female' => 'Team Org School Conference Id Female',
            'team_org_school_conference_id_male' => 'Team Org School Conference Id Male',
            'team_org_school_school_ipeds_id' => 'Team Org School School Ipeds ID',
            'team_org_school_conference_name' => 'Team Org School Conference Name',
            'team_org_school_ncaa_division' => 'Team Org School Ncaa Division',
            'team_org_school_conf_basetable_conference_id' => 'Team Org School Conf Basetable Conference ID',
            'team_org_school_conf_basetable_conference_name' => 'Team Org School Conf Basetable Conference Name',
            'team_org_school_conf_basetable_ncaa_division' => 'Team Org School Conf Basetable Ncaa Division',
            'team_coach_id' => 'Team Coach ID',
            'team_coach_team_id' => 'Team Coach Team ID',
            'team_coach_coach_type_id' => 'Team Coach Coach Type ID',
            'team_coach_primary_position_name' => 'Team Coach Primary Position Name',
            'team_coach_begin_dt' => 'Team Coach Begin Dt',
            'team_coach_end_dt' => 'Team Coach End Dt',
            'team_coach_coach_id' => 'Team Coach Coach ID',
            'team_coach_created_at' => 'Team Coach Created At',
            'team_coach_updated_at' => 'Team Coach Updated At',
            'team_org_id' => 'Team Org ID',
            'team_school_id' => 'Team School ID',
            'team_sport_id' => 'Team Sport ID',
            'team_sport_name' => 'Team Sport Name',
            'team_camp_id' => 'Team Camp ID',
            'team_gender_id' => 'Team Gender ID',
            'team_gender' => 'Team Gender',
            'team_age_group_id' => 'Team Age Group ID',
            'team_age_group' => 'Team Age Group',
            'team_competition_season_id' => 'Team Competition Season ID',
            'team_competition_season' => 'Team Competition Season',
            'team_league_id' => 'Team League ID',
            'team_league' => 'Team League',
            'team_league_name' => 'Team League Name',
            'organizational_level' => 'Organizational Level',
            'team_governing_body' => 'Team Governing Body',
            'team_website_url' => 'Team Website Url',
            'team_schedule_url' => 'Team Schedule Url',
            'team_schedule_uri' => 'Team Schedule Uri',
            'team_statistical_highlights' => 'Team Statistical Highlights',
            'team_team_division_id' => 'Team Team Division ID',
            'team_division' => 'Team Division',
            'team_division_name' => 'Team Division Name',
            'team_city' => 'Team City',
            'team_state_id' => 'Team State ID',
            'team_country_id' => 'Team Country ID',
            'team_wins' => 'Team Wins',
            'team_losses' => 'Team Losses',
            'team_draws' => 'Team Draws',
            'team_wins_losses_draws' => 'Team Wins Losses Draws',
            'team_basetable_team_league_id' => 'Team Basetable Team League ID',
            'team_basetable_team_league_name' => 'Team Basetable Team League Name',
            'team_basetable_team_division_id' => 'Team Basetable Team Division ID',
            'team_basetable_team_division_name' => 'Team Basetable Team Division Name',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwCoachTeams6Query the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwCoachTeams6Query(get_called_class());
    }
}
