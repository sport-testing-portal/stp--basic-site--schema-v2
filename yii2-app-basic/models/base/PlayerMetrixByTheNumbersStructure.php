<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_metrix_by_the_numbers_structure".
 *
 * @property integer $id
 * @property integer $person_age
 * @property string $test_date
 * @property string $test_desc
 * @property string $split_raw
 * @property string $split_avg
 * @property integer $split_cnt
 * @property string $score
 */
class PlayerMetrixByTheNumbersStructure extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_age', 'split_cnt'], 'integer'],
            [['split_avg', 'score'], 'number'],
            [['test_date', 'test_desc', 'split_raw'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_metrix_by_the_numbers_structure';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person_age' => 'Person Age',
            'test_date' => 'Test Date',
            'test_desc' => 'Test Desc',
            'split_raw' => 'Split Raw',
            'split_avg' => 'Split Avg',
            'split_cnt' => 'Split Cnt',
            'score' => 'Score',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerMetrixByTheNumbersStructureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerMetrixByTheNumbersStructureQuery(get_called_class());
    }
}
