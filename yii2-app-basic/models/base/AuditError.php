<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "audit_error".
 *
 * @property integer $id
 * @property integer $entry_id
 * @property string $created
 * @property string $message
 * @property integer $code
 * @property string $file
 * @property integer $line
 * @property resource $trace
 * @property string $hash
 * @property integer $emailed
 *
 * @property \app\models\AuditEntry $entry
 */
class AuditError extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'entry'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entry_id', 'created', 'message'], 'required'],
            [['entry_id', 'code', 'line'], 'integer'],
            [['created'], 'safe'],
            [['message', 'trace'], 'string'],
            [['file'], 'string', 'max' => 512],
            [['hash'], 'string', 'max' => 32],
            [['emailed'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_error';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'message' => 'Message',
            'code' => 'Code',
            'file' => 'File',
            'line' => 'Line',
            'trace' => 'Trace',
            'hash' => 'Hash',
            'emailed' => 'Emailed',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntry()
    {
        return $this->hasOne(\app\models\AuditEntry::className(), ['id' => 'entry_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AuditErrorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AuditErrorQuery(get_called_class());
    }
}
