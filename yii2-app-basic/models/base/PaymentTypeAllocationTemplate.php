<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "payment_type_allocation_template".
 *
 * @property integer $payment_type_allocation_template_id
 * @property string $payment_type_allocation_template_name
 * @property string $payment_type_allocation_template_desc_short
 * @property string $payment_type_allocation_template_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PaymentType[] $paymentTypes
 */
class PaymentTypeAllocationTemplate extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'paymentTypes'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['payment_type_allocation_template_name'], 'string', 'max' => 100],
            [['payment_type_allocation_template_desc_short'], 'string', 'max' => 45],
            [['payment_type_allocation_template_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_type_allocation_template';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_type_allocation_template_id' => 'Payment Type Allocation Template ID',
            'payment_type_allocation_template_name' => 'Payment Type Allocation Template Name',
            'payment_type_allocation_template_desc_short' => 'Payment Type Allocation Template Desc Short',
            'payment_type_allocation_template_desc_long' => 'Payment Type Allocation Template Desc Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentTypes()
    {
        return $this->hasMany(\app\models\PaymentType::className(), ['payment_type_allocation_template_id' => 'payment_type_allocation_template_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PaymentTypeAllocationTemplateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PaymentTypeAllocationTemplateQuery(get_called_class());
    }
}
