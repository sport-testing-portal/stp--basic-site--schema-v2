<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "media".
 *
 * @property integer $media_id
 * @property integer $media_type_id
 * @property integer $org_id
 * @property integer $person_id
 * @property string $media_name_original
 * @property string $media_name_unique
 * @property string $media_desc_short
 * @property string $media_desc_long
 * @property string $media_url_local
 * @property string $media_url_remote
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\MediaType $mediaType
 * @property \app\models\Org $org
 * @property \app\models\Person $person
 */
class Media extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'mediaType',
            'org',
            'person'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_type_id', 'org_id', 'person_id'], 'required'],
            [['media_type_id', 'org_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['media_name_original', 'media_name_unique', 'media_desc_long'], 'string', 'max' => 255],
            [['media_desc_short'], 'string', 'max' => 45],
            [['media_url_local', 'media_url_remote'], 'string', 'max' => 245],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'media_id' => 'Media ID',
            'media_type_id' => 'Media Type ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'media_name_original' => 'Media Name Original',
            'media_name_unique' => 'Media Name Unique',
            'media_desc_short' => 'Media Desc Short',
            'media_desc_long' => 'Media Desc Long',
            'media_url_local' => 'Media Url Local',
            'media_url_remote' => 'Media Url Remote',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMediaType()
    {
        return $this->hasOne(\app\models\MediaType::className(), ['media_type_id' => 'media_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MediaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MediaQuery(get_called_class());
    }
}
