<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwPersonCertificationFull".
 *
 * @property integer $person_certification_id
 * @property integer $person_id
 * @property integer $person_certification_type_id
 * @property integer $person_certification_year
 * @property integer $person_type_id
 * @property string $person_certification_type_name
 * @property string $person_certification_subtype_name
 * @property integer $person_certification_type_display_order
 * @property string $person_certification_type_description_short
 * @property string $person_certification_type_description_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class VwPersonCertificationFull extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_certification_id', 'person_id', 'person_certification_type_id', 'person_certification_year', 'person_type_id', 'person_certification_type_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['person_certification_type_name', 'person_certification_subtype_name', 'person_certification_type_description_short'], 'string', 'max' => 45],
            [['person_certification_type_description_long'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwPersonCertificationFull';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_certification_id' => 'Person Certification ID',
            'person_id' => 'Person ID',
            'person_certification_type_id' => 'Person Certification Type ID',
            'person_certification_year' => 'Person Certification Year',
            'person_type_id' => 'Person Type ID',
            'person_certification_type_name' => 'Person Certification Type Name',
            'person_certification_subtype_name' => 'Person Certification Subtype Name',
            'person_certification_type_display_order' => 'Person Certification Type Display Order',
            'person_certification_type_description_short' => 'Person Certification Type Description Short',
            'person_certification_type_description_long' => 'Person Certification Type Description Long',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwPersonCertificationFullQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwPersonCertificationFullQuery(get_called_class());
    }
}
