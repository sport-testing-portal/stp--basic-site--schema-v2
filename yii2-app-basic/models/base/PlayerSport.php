<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_sport".
 *
 * @property integer $player_sport_id
 * @property integer $player_id
 * @property integer $sport_id
 * @property integer $sport_position_id
 * @property integer $gender_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Gender $gender
 * @property \app\models\Player $player
 * @property \app\models\Sport $sport
 * @property \app\models\SportPosition $sportPosition
 */
class PlayerSport extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'gender',
            'player',
            'sport',
            'sportPosition'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'sport_id', 'sport_position_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_sport';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_sport_id' => 'Player Sport ID',
            'player_id' => 'Player ID',
            'sport_id' => 'Sport ID',
            'sport_position_id' => 'Sport Position ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(\app\models\Gender::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(\app\models\Player::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(\app\models\Sport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportPosition()
    {
        return $this->hasOne(\app\models\SportPosition::className(), ['sport_position_id' => 'sport_position_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerSportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerSportQuery(get_called_class());
    }
}
