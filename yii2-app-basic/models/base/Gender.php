<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "gender".
 *
 * @property integer $gender_id
 * @property string $gender_desc
 * @property string $gender_code
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\CampSession[] $campSessions
 * @property \app\models\CampSessionLocation[] $campSessionLocations
 * @property \app\models\CampSessionSport[] $campSessionSports
 * @property \app\models\CampSport[] $campSports
 * @property \app\models\CoachSport[] $coachSports
 * @property \app\models\Person[] $people
 * @property \app\models\PlayerSport[] $playerSports
 * @property \app\models\SportGender[] $sportGenders
 * @property \app\models\Team[] $teams
 */
class Gender extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'campSessions',
            'campSessionLocations',
            'campSessionSports',
            'campSports',
            'coachSports',
            'people',
            'playerSports',
            'sportGenders',
            'teams'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['gender_desc'], 'string', 'max' => 30],
            [['gender_code'], 'string', 'max' => 5],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gender';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gender_id' => 'Gender ID',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessions()
    {
        return $this->hasMany(\app\models\CampSession::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionLocations()
    {
        return $this->hasMany(\app\models\CampSessionLocation::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionSports()
    {
        return $this->hasMany(\app\models\CampSessionSport::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSports()
    {
        return $this->hasMany(\app\models\CampSport::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachSports()
    {
        return $this->hasMany(\app\models\CoachSport::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(\app\models\Person::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSports()
    {
        return $this->hasMany(\app\models\PlayerSport::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportGenders()
    {
        return $this->hasMany(\app\models\SportGender::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(\app\models\Team::className(), ['gender_id' => 'gender_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\GenderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\GenderQuery(get_called_class());
    }
}
