<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "app_user_profile_picture".
 *
 * @property integer $app_user_profile_picture_id
 * @property integer $app_user_id
 * @property integer $app_user_profile_original_picture_id
 * @property string $app_user_profile_picture_filename
 * @property integer $app_user_profile_picture_width
 * @property integer $app_user_profile_picture_height
 * @property string $app_user_profile_picture_mimetype
 * @property string $app_user_profile_picture_contents
 * @property string $created_on
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\AppUser $appUser
 */
class AppUserProfilePicture extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'appUser'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['app_user_id', 'app_user_profile_picture_filename', 'app_user_profile_picture_width', 'app_user_profile_picture_height', 'app_user_profile_picture_mimetype', 'app_user_profile_picture_contents'], 'required'],
            [['app_user_id', 'app_user_profile_original_picture_id', 'app_user_profile_picture_width', 'app_user_profile_picture_height', 'created_by', 'updated_by'], 'integer'],
            [['app_user_profile_picture_contents'], 'string'],
            [['created_on', 'created_at', 'updated_at'], 'safe'],
            [['app_user_profile_picture_filename', 'app_user_profile_picture_mimetype'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user_profile_picture';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_user_profile_picture_id' => 'App User Profile Picture ID',
            'app_user_id' => 'App User ID',
            'app_user_profile_original_picture_id' => 'App User Profile Original Picture ID',
            'app_user_profile_picture_filename' => 'App User Profile Picture Filename',
            'app_user_profile_picture_width' => 'App User Profile Picture Width',
            'app_user_profile_picture_height' => 'App User Profile Picture Height',
            'app_user_profile_picture_mimetype' => 'App User Profile Picture Mimetype',
            'app_user_profile_picture_contents' => 'App User Profile Picture Contents',
            'created_on' => 'Created On',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUser()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['app_user_id' => 'app_user_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AppUserProfilePictureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AppUserProfilePictureQuery(get_called_class());
    }
}
