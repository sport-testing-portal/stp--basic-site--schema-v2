<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "audit_entry".
 *
 * @property integer $id
 * @property string $created
 * @property integer $user_id
 * @property double $duration
 * @property string $ip
 * @property string $request_method
 * @property integer $ajax
 * @property string $route
 * @property integer $memory_max
 *
 * @property \app\models\AuditData[] $auditDatas
 * @property \app\models\AuditError[] $auditErrors
 * @property \app\models\AuditJavascript[] $auditJavascripts
 * @property \app\models\AuditMail[] $auditMails
 * @property \app\models\AuditTrail[] $auditTrails
 */
class AuditEntry extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'auditDatas',
            'auditErrors',
            'auditJavascripts',
            'auditMails',
            'auditTrails'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created'], 'required'],
            [['created'], 'safe'],
            [['user_id', 'ajax', 'memory_max'], 'integer'],
            [['duration'], 'number'],
            [['ip'], 'string', 'max' => 45],
            [['request_method'], 'string', 'max' => 16],
            [['route'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'audit_entry';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'duration' => 'Duration',
            'ip' => 'Ip',
            'request_method' => 'Request Method',
            'ajax' => 'Ajax',
            'route' => 'Route',
            'memory_max' => 'Memory Max',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditDatas()
    {
        return $this->hasMany(\app\models\AuditData::className(), ['entry_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditErrors()
    {
        return $this->hasMany(\app\models\AuditError::className(), ['entry_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditJavascripts()
    {
        return $this->hasMany(\app\models\AuditJavascript::className(), ['entry_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditMails()
    {
        return $this->hasMany(\app\models\AuditMail::className(), ['entry_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuditTrails()
    {
        return $this->hasMany(\app\models\AuditTrail::className(), ['entry_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AuditEntryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AuditEntryQuery(get_called_class());
    }
}
