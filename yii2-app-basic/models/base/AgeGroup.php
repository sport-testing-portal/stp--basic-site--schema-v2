<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "age_group".
 *
 * @property integer $age_group_id
 * @property integer $sport_id
 * @property string $age_group_name
 * @property string $age_group_desc
 * @property integer $age_group_min
 * @property integer $age_group_max
 * @property integer $display_order
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Coach[] $coaches
 * @property \app\models\Player[] $players
 * @property \app\models\Team[] $teams
 */
class AgeGroup extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'coaches',
            'players',
            'teams'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sport_id', 'age_group_min', 'age_group_max', 'display_order', 'created_by', 'updated_by'], 'integer'],
            [['age_group_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['age_group_name', 'age_group_desc'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'age_group';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'age_group_id' => 'Age Group ID',
            'sport_id' => 'Sport ID',
            'age_group_name' => 'Age Group Name',
            'age_group_desc' => 'Age Group Desc',
            'age_group_min' => 'Age Group Min',
            'age_group_max' => 'Age Group Max',
            'display_order' => 'Display Order',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoaches()
    {
        return $this->hasMany(\app\models\Coach::className(), ['age_group_id' => 'age_group_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(\app\models\Player::className(), ['age_group_id' => 'age_group_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(\app\models\Team::className(), ['age_group_id' => 'age_group_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AgeGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AgeGroupQuery(get_called_class());
    }
}
