<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "app_user_remote_identity".
 *
 * @property integer $app_user_remote_identity_id
 * @property integer $app_user_id
 * @property string $provider
 * @property string $identifier
 * @property string $created_on
 * @property string $last_used_on
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\AppUser $appUser
 */
class AppUserRemoteIdentity extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'appUser'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['app_user_id', 'provider', 'identifier'], 'required'],
            [['app_user_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'last_used_on', 'created_at', 'updated_at'], 'safe'],
            [['provider', 'identifier'], 'string', 'max' => 100],
            [['app_user_id', 'provider', 'identifier'], 'unique', 'targetAttribute' => ['app_user_id', 'provider', 'identifier'], 'message' => 'The combination of App User ID, Provider and Identifier has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user_remote_identity';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_user_remote_identity_id' => 'App User Remote Identity ID',
            'app_user_id' => 'App User ID',
            'provider' => 'Provider',
            'identifier' => 'Identifier',
            'created_on' => 'Created On',
            'last_used_on' => 'Last Used On',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUser()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['app_user_id' => 'app_user_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AppUserRemoteIdentityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AppUserRemoteIdentityQuery(get_called_class());
    }
}
