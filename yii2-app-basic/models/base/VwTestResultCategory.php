<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTestResultCategory".
 *
 * @property integer $id
 * @property string $person
 * @property string $ptype
 * @property string $gender_code
 * @property string $rating_bias
 * @property integer $tedtd_display_order
 * @property string $test_desc
 * @property string $category
 * @property string $test_category
 * @property string $test_type
 * @property string $test_subtype
 * @property string $split
 * @property string $score
 * @property string $test_units
 * @property integer $attempt
 * @property string $trial_status
 * @property integer $overall_ranking
 * @property integer $positional_ranking
 * @property string $total_overall_ranking
 * @property string $total_positional_ranking
 * @property string $score_url
 * @property string $video_url
 * @property string $test_date
 * @property string $test_date_iso
 * @property string $tester
 * @property string $person_name_first
 * @property string $person_name_last
 * @property string $person_city
 * @property string $person_country_code
 * @property string $person_state_or_region
 * @property string $person_postal_code
 * @property integer $player_sport_position_id
 * @property string $sport_position_name
 * @property string $gender_desc
 * @property integer $gender_id
 * @property string $age_group_name
 * @property integer $age_group_id
 * @property integer $age_group_min
 * @property integer $age_group_max
 * @property string $country_long_name
 * @property string $latitude
 * @property string $longitude
 * @property string $combine_name
 * @property integer $user_id
 * @property integer $person_id
 * @property integer $player_id
 * @property integer $source_event_id
 * @property integer $summary_id
 * @property integer $detail_id
 * @property integer $provider_id
 * @property integer $category_id
 * @property integer $type_id
 * @property integer $desc_id
 * @property integer $file_id
 */
class VwTestResultCategory extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'tedtd_display_order', 'overall_ranking', 'positional_ranking', 'player_sport_position_id', 'gender_id', 'age_group_id', 'age_group_min', 'age_group_max', 'user_id', 'person_id', 'player_id', 'source_event_id', 'summary_id', 'detail_id', 'provider_id', 'category_id', 'type_id', 'desc_id', 'file_id'], 'integer'],
            [['score', 'total_overall_ranking', 'total_positional_ranking'], 'number'],
            [['test_date_iso'], 'safe'],
            [['person'], 'string', 'max' => 90],
            [['ptype', 'test_desc', 'category', 'test_category', 'split', 'test_units', 'trial_status', 'person_name_first', 'person_name_last', 'person_state_or_region', 'sport_position_name', 'age_group_name', 'latitude', 'longitude'], 'string', 'max' => 45],
            [['gender_code', 'rating_bias'], 'string', 'max' => 5],
            [['test_type', 'test_subtype', 'score_url', 'video_url'], 'string', 'max' => 150],
            [['attempt'], 'string', 'max' => 4],
            [['test_date'], 'string', 'max' => 40],
            [['tester', 'person_city'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['person_postal_code'], 'string', 'max' => 25],
            [['gender_desc'], 'string', 'max' => 30],
            [['country_long_name'], 'string', 'max' => 80],
            [['combine_name'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTestResultCategory';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'person' => 'Person',
            'ptype' => 'Ptype',
            'gender_code' => 'Gender Code',
            'rating_bias' => 'Rating Bias',
            'tedtd_display_order' => 'Tedtd Display Order',
            'test_desc' => 'Test Desc',
            'category' => 'Category',
            'test_category' => 'Test Category',
            'test_type' => 'Test Type',
            'test_subtype' => 'Test Subtype',
            'split' => 'Split',
            'score' => 'Score',
            'test_units' => 'Test Units',
            'attempt' => 'Attempt',
            'trial_status' => 'Trial Status',
            'overall_ranking' => 'Overall Ranking',
            'positional_ranking' => 'Positional Ranking',
            'total_overall_ranking' => 'Total Overall Ranking',
            'total_positional_ranking' => 'Total Positional Ranking',
            'score_url' => 'Score Url',
            'video_url' => 'Video Url',
            'test_date' => 'Test Date',
            'test_date_iso' => 'Test Date Iso',
            'tester' => 'Tester',
            'person_name_first' => 'Person Name First',
            'person_name_last' => 'Person Name Last',
            'person_city' => 'Person City',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'person_postal_code' => 'Person Postal Code',
            'player_sport_position_id' => 'Player Sport Position ID',
            'sport_position_name' => 'Sport Position Name',
            'gender_desc' => 'Gender Desc',
            'gender_id' => 'Gender ID',
            'age_group_name' => 'Age Group Name',
            'age_group_id' => 'Age Group ID',
            'age_group_min' => 'Age Group Min',
            'age_group_max' => 'Age Group Max',
            'country_long_name' => 'Country Long Name',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'combine_name' => 'Combine Name',
            'user_id' => 'User ID',
            'person_id' => 'Person ID',
            'player_id' => 'Player ID',
            'source_event_id' => 'Source Event ID',
            'summary_id' => 'Summary ID',
            'detail_id' => 'Detail ID',
            'provider_id' => 'Provider ID',
            'category_id' => 'Category ID',
            'type_id' => 'Type ID',
            'desc_id' => 'Desc ID',
            'file_id' => 'File ID',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTestResultCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTestResultCategoryQuery(get_called_class());
    }
}
