<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "address".
 *
 * @property integer $address_id
 * @property integer $org_id
 * @property integer $person_id
 * @property integer $address_type_id
 * @property string $addr1
 * @property string $addr2
 * @property string $addr3
 * @property string $city
 * @property string $state_or_region
 * @property string $postal_code
 * @property string $country
 * @property string $country_code
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\AddressType $addressType
 * @property \app\models\Org $org
 * @property \app\models\Person $person
 * @property \app\models\CampLocations[] $campLocations
 * @property \app\models\CampSession[] $campSessions
 * @property \app\models\CampSessionLocation[] $campSessionLocations
 */
class Address extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'addressType',
            'org',
            'person',
            'campLocations',
            'campSessions',
            'campSessionLocations'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'person_id', 'address_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['addr1', 'addr2', 'addr3'], 'string', 'max' => 100],
            [['city', 'state_or_region', 'postal_code', 'country'], 'string', 'max' => 45],
            [['country_code'], 'string', 'max' => 5],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'address_id' => 'Address ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'address_type_id' => 'Address Type ID',
            'addr1' => 'Addr1',
            'addr2' => 'Addr2',
            'addr3' => 'Addr3',
            'city' => 'City',
            'state_or_region' => 'State Or Region',
            'postal_code' => 'Postal Code',
            'country' => 'Country',
            'country_code' => 'Country Code',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressType()
    {
        return $this->hasOne(\app\models\AddressType::className(), ['address_type_id' => 'address_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampLocations()
    {
        return $this->hasMany(\app\models\CampLocations::className(), ['address_id' => 'address_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessions()
    {
        return $this->hasMany(\app\models\CampSession::className(), ['address_id' => 'address_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessionLocations()
    {
        return $this->hasMany(\app\models\CampSessionLocation::className(), ['address_id' => 'address_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AddressQuery(get_called_class());
    }
}
