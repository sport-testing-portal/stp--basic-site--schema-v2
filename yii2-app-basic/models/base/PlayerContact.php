<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "player_contact".
 *
 * @property integer $player_contact_id
 * @property integer $player_contact_type_id
 * @property integer $player_id
 * @property integer $person_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PlayerContactType $playerContact
 * @property \app\models\Person $person
 * @property \app\models\Player $player
 * @property \app\models\PlayerContactType $playerContactType
 */
class PlayerContact extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'playerContact',
            'person',
            'player',
            'playerContactType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_contact_type_id', 'player_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player_contact';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'player_contact_id' => 'Player Contact ID',
            'player_contact_type_id' => 'Player Contact Type ID',
            'player_id' => 'Player ID',
            'person_id' => 'Person ID',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerContact()
    {
        return $this->hasOne(\app\models\PlayerContactType::className(), ['player_contact_type_id' => 'player_contact_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayer()
    {
        return $this->hasOne(\app\models\Player::className(), ['player_id' => 'player_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerContactType()
    {
        return $this->hasOne(\app\models\PlayerContactType::className(), ['player_contact_type_id' => 'player_contact_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PlayerContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PlayerContactQuery(get_called_class());
    }
}
