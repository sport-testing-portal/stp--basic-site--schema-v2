<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "nces_school".
 *
 * @property integer $nces_school_id
 * @property string $nces_school_name
 * @property string $nces_school_website
 * @property integer $nces_school_unit_id
 * @property string $nces_school_addr1
 * @property string $nces_school_addr2
 * @property integer $nces_school_ope_id
 * @property string $nces_school_city
 * @property string $nces_school_state
 * @property string $nces_school_zipcode
 * @property string $nces_school_phone_general
 * @property string $nces_school_admin
 * @property string $nces_school_admin_title
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class NcesSchool extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nces_school_unit_id', 'nces_school_ope_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nces_school_name', 'nces_school_website', 'nces_school_addr1'], 'string', 'max' => 125],
            [['nces_school_addr2', 'nces_school_state'], 'string', 'max' => 45],
            [['nces_school_city', 'nces_school_admin_title'], 'string', 'max' => 65],
            [['nces_school_zipcode'], 'string', 'max' => 15],
            [['nces_school_phone_general'], 'string', 'max' => 25],
            [['nces_school_admin'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nces_school';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nces_school_id' => 'Nces School ID',
            'nces_school_name' => 'Nces School Name',
            'nces_school_website' => 'Nces School Website',
            'nces_school_unit_id' => 'Nces School Unit ID',
            'nces_school_addr1' => 'Nces School Addr1',
            'nces_school_addr2' => 'Nces School Addr2',
            'nces_school_ope_id' => 'Nces School Ope ID',
            'nces_school_city' => 'Nces School City',
            'nces_school_state' => 'Nces School State',
            'nces_school_zipcode' => 'Nces School Zipcode',
            'nces_school_phone_general' => 'Nces School Phone General',
            'nces_school_admin' => 'Nces School Admin',
            'nces_school_admin_title' => 'Nces School Admin Title',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\NcesSchoolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\NcesSchoolQuery(get_called_class());
    }
}
