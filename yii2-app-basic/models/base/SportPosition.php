<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "sport_position".
 *
 * @property integer $sport_position_id
 * @property integer $sport_id
 * @property string $sport_position_name
 * @property integer $display_order
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Player[] $players
 * @property \app\models\PlayerSport[] $playerSports
 * @property \app\models\Sport $sport
 * @property \app\models\TeamPlayer[] $teamPlayers
 */
class SportPosition extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'players',
            'playerSports',
            'sport',
            'teamPlayers'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sport_id', 'display_order', 'created_by', 'updated_by'], 'integer'],
            [['sport_position_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['sport_position_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sport_position';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sport_position_id' => 'Sport Position ID',
            'sport_id' => 'Sport ID',
            'sport_position_name' => 'Sport Position Name',
            'display_order' => 'Display Order',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayers()
    {
        return $this->hasMany(\app\models\Player::className(), ['player_sport_position_id' => 'sport_position_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSports()
    {
        return $this->hasMany(\app\models\PlayerSport::className(), ['sport_position_id' => 'sport_position_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(\app\models\Sport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamPlayers()
    {
        return $this->hasMany(\app\models\TeamPlayer::className(), ['team_play_sport_position_id' => 'sport_position_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SportPositionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SportPositionQuery(get_called_class());
    }
}
