<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_category".
 *
 * @property integer $test_eval_category_id
 * @property string $test_eval_category_name
 * @property integer $test_eval_category_display_order
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalType[] $testEvalTypes
 */
class TestEvalCategory extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalTypes'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_eval_category_name'], 'required'],
            [['test_eval_category_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['test_eval_category_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['test_eval_category_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_category';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_category_id' => 'Test Eval Category ID',
            'test_eval_category_name' => 'Test Eval Category Name',
            'test_eval_category_display_order' => 'Test Eval Category Display Order',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalTypes()
    {
        return $this->hasMany(\app\models\TestEvalType::className(), ['test_eval_category_id' => 'test_eval_category_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalCategoryQuery(get_called_class());
    }
}
