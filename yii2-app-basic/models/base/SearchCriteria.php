<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "search_criteria".
 *
 * @property integer $search_criteria_id
 * @property string $table_name
 * @property string $field_name
 * @property string $field_is_indexed_yn
 * @property string $comments
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\SearchCriteriaTemplateItem[] $searchCriteriaTemplateItems
 */
class SearchCriteria extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'searchCriteriaTemplateItems'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table_name', 'field_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['table_name', 'field_name'], 'string', 'max' => 45],
            [['field_is_indexed_yn', 'lock'], 'string', 'max' => 1],
            [['comments'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'search_criteria';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'search_criteria_id' => 'Search Criteria ID',
            'table_name' => 'Table Name',
            'field_name' => 'Field Name',
            'field_is_indexed_yn' => 'Field Is Indexed Yn',
            'comments' => 'Comments',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSearchCriteriaTemplateItems()
    {
        return $this->hasMany(\app\models\SearchCriteriaTemplateItem::className(), ['search_criteria_id' => 'search_criteria_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SearchCriteriaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SearchCriteriaQuery(get_called_class());
    }
}
