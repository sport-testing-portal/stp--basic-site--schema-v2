<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "data_hdr_xlation_item".
 *
 * @property integer $data_hdr_xlation_item_id
 * @property integer $data_hdr_xlation_id
 * @property string $data_hdr_xlation_item_source_value
 * @property string $data_hdr_xlation_item_target_value
 * @property integer $ordinal_position_num
 * @property string $data_hdr_xlation_item_target_table
 * @property string $data_hdr_source_value_type_name_source
 * @property integer $data_hdr_source_value_is_type_name
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\DataHdrXlation $dataHdrXlation
 */
class DataHdrXlationItem extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'dataHdrXlation'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_hdr_xlation_id', 'ordinal_position_num', 'data_hdr_source_value_is_type_name', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['data_hdr_xlation_item_source_value', 'data_hdr_xlation_item_target_value'], 'string', 'max' => 75],
            [['data_hdr_xlation_item_target_table', 'data_hdr_source_value_type_name_source'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_hdr_xlation_item';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'data_hdr_xlation_item_id' => 'Data Hdr Xlation Item ID',
            'data_hdr_xlation_id' => 'Data Hdr Xlation ID',
            'data_hdr_xlation_item_source_value' => 'Data Hdr Xlation Item Source Value',
            'data_hdr_xlation_item_target_value' => 'Data Hdr Xlation Item Target Value',
            'ordinal_position_num' => 'Ordinal Position Num',
            'data_hdr_xlation_item_target_table' => 'Data Hdr Xlation Item Target Table',
            'data_hdr_source_value_type_name_source' => 'Data Hdr Source Value Type Name Source',
            'data_hdr_source_value_is_type_name' => 'Data Hdr Source Value Is Type Name',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataHdrXlation()
    {
        return $this->hasOne(\app\models\DataHdrXlation::className(), ['data_hdr_xlation_id' => 'data_hdr_xlation_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\DataHdrXlationItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\DataHdrXlationItemQuery(get_called_class());
    }
}
