<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "zip_code_distinct".
 *
 * @property integer $zip_code_id
 * @property string $country_code_iso2
 * @property string $postal_code
 * @property string $place_name
 * @property string $state_name
 * @property string $state_code
 * @property string $county_name
 * @property string $county_code
 * @property string $community_name
 * @property string $community_code
 * @property string $latitude
 * @property string $longitude
 * @property integer $accuracy
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 */
class ZipCodeDistinct extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accuracy', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['country_code_iso2'], 'string', 'max' => 2],
            [['postal_code', 'state_code', 'county_code', 'community_code'], 'string', 'max' => 20],
            [['place_name'], 'string', 'max' => 180],
            [['state_name', 'community_name'], 'string', 'max' => 100],
            [['county_name', 'latitude', 'longitude'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zip_code_distinct';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zip_code_id' => 'Zip Code ID',
            'country_code_iso2' => 'Country Code Iso2',
            'postal_code' => 'Postal Code',
            'place_name' => 'Place Name',
            'state_name' => 'State Name',
            'state_code' => 'State Code',
            'county_name' => 'County Name',
            'county_code' => 'County Code',
            'community_name' => 'Community Name',
            'community_code' => 'Community Code',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'accuracy' => 'Accuracy',
            'lock' => 'Lock',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\ZipCodeDistinctQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ZipCodeDistinctQuery(get_called_class());
    }
}
