<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTeamStatTest".
 *
 * @property string $team_name
 * @property string $team_age_group
 * @property string $team_division
 * @property string $gender_code
 * @property string $gender_desc
 * @property string $coach_name
 * @property string $player_name
 * @property integer $team_id
 * @property integer $coach_id
 * @property integer $player_id
 * @property integer $coach__person_id
 * @property integer $player__person_id
 * @property integer $test_eval_summary_log_id
 */
class VwTeamStatTest extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'coach_id', 'player_id', 'coach__person_id', 'player__person_id', 'test_eval_summary_log_id'], 'integer'],
            [['team_name'], 'string', 'max' => 75],
            [['team_age_group'], 'string', 'max' => 45],
            [['team_division'], 'string', 'max' => 10],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['coach_name', 'player_name'], 'string', 'max' => 90],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTeamStatTest';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'coach__person_id' => 'Coach  Person ID',
            'player__person_id' => 'Player  Person ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTeamStatTestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTeamStatTestQuery(get_called_class());
    }
}
