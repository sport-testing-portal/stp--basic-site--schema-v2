<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "sport".
 *
 * @property integer $sport_id
 * @property string $sport_name
 * @property string $sport_desc_short
 * @property string $sport_desc_long
 * @property string $high_school_yn
 * @property string $ncaa_yn
 * @property string $olympic_yn
 * @property string $xgame_sport_yn
 * @property string $ncaa_sport_name
 * @property string $olympic_sport_name
 * @property string $xgame_sport_name
 * @property string $ncaa_sport_type
 * @property string $ncaa_sport_season_male
 * @property string $ncaa_sport_season_female
 * @property string $ncaa_sport_season_coed
 * @property string $olympic_sport_season
 * @property string $xgame_sport_season
 * @property string $olympic_sport_gender
 * @property string $xgame_sport_gender
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\CampSession[] $campSessions
 * @property \app\models\CampSport[] $campSports
 * @property \app\models\CoachSport[] $coachSports
 * @property \app\models\GoverningBody[] $governingBodies
 * @property \app\models\PlayerSport[] $playerSports
 * @property \app\models\SchoolSport[] $schoolSports
 * @property \app\models\SportGender[] $sportGenders
 * @property \app\models\SportPosition[] $sportPositions
 * @property \app\models\Team[] $teams
 */
class Sport extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'campSessions',
            'campSports',
            'coachSports',
            'governingBodies',
            'playerSports',
            'schoolSports',
            'sportGenders',
            'sportPositions',
            'teams'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['sport_name', 'ncaa_sport_name', 'olympic_sport_name', 'xgame_sport_name'], 'string', 'max' => 45],
            [['sport_desc_short'], 'string', 'max' => 75],
            [['sport_desc_long'], 'string', 'max' => 175],
            [['high_school_yn', 'ncaa_yn', 'olympic_yn', 'xgame_sport_yn', 'lock'], 'string', 'max' => 1],
            [['ncaa_sport_type', 'olympic_sport_season', 'xgame_sport_season', 'olympic_sport_gender', 'xgame_sport_gender'], 'string', 'max' => 15],
            [['ncaa_sport_season_male', 'ncaa_sport_season_female', 'ncaa_sport_season_coed'], 'string', 'max' => 30],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sport';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sport_id' => 'Sport ID',
            'sport_name' => 'Sport Name',
            'sport_desc_short' => 'Sport Desc Short',
            'sport_desc_long' => 'Sport Desc Long',
            'high_school_yn' => 'High School Yn',
            'ncaa_yn' => 'Ncaa Yn',
            'olympic_yn' => 'Olympic Yn',
            'xgame_sport_yn' => 'Xgame Sport Yn',
            'ncaa_sport_name' => 'Ncaa Sport Name',
            'olympic_sport_name' => 'Olympic Sport Name',
            'xgame_sport_name' => 'Xgame Sport Name',
            'ncaa_sport_type' => 'Ncaa Sport Type',
            'ncaa_sport_season_male' => 'Ncaa Sport Season Male',
            'ncaa_sport_season_female' => 'Ncaa Sport Season Female',
            'ncaa_sport_season_coed' => 'Ncaa Sport Season Coed',
            'olympic_sport_season' => 'Olympic Sport Season',
            'xgame_sport_season' => 'Xgame Sport Season',
            'olympic_sport_gender' => 'Olympic Sport Gender',
            'xgame_sport_gender' => 'Xgame Sport Gender',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSessions()
    {
        return $this->hasMany(\app\models\CampSession::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampSports()
    {
        return $this->hasMany(\app\models\CampSport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachSports()
    {
        return $this->hasMany(\app\models\CoachSport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoverningBodies()
    {
        return $this->hasMany(\app\models\GoverningBody::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerSports()
    {
        return $this->hasMany(\app\models\PlayerSport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolSports()
    {
        return $this->hasMany(\app\models\SchoolSport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportGenders()
    {
        return $this->hasMany(\app\models\SportGender::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportPositions()
    {
        return $this->hasMany(\app\models\SportPosition::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeams()
    {
        return $this->hasMany(\app\models\Team::className(), ['sport_id' => 'sport_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SportQuery(get_called_class());
    }
}
