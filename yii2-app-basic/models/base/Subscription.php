<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "subscription".
 *
 * @property integer $subscription_id
 * @property integer $subscription_type_id
 * @property integer $org_id
 * @property integer $person_id
 * @property integer $subscription_status_type_id
 * @property string $subscription_begin_dt
 * @property string $subscription_end_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Notification[] $notifications
 * @property \app\models\PaymentLog[] $paymentLogs
 * @property \app\models\Org $org
 * @property \app\models\Person $person
 * @property \app\models\SubscriptionStatusType $subscriptionStatusType
 * @property \app\models\SubscriptionType $subscriptionType
 * @property \app\models\SubscriptionLog[] $subscriptionLogs
 */
class Subscription extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'notifications',
            'paymentLogs',
            'org',
            'person',
            'subscriptionStatusType',
            'subscriptionType',
            'subscriptionLogs'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_type_id', 'org_id', 'person_id', 'subscription_status_type_id', 'created_by', 'updated_by'], 'integer'],
            [['subscription_begin_dt', 'subscription_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subscription_id' => 'Subscription ID',
            'subscription_type_id' => 'Subscription Type ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'subscription_status_type_id' => 'Subscription Status Type ID',
            'subscription_begin_dt' => 'Subscription Begin Dt',
            'subscription_end_dt' => 'Subscription End Dt',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(\app\models\Notification::className(), ['subscription_id' => 'subscription_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLogs()
    {
        return $this->hasMany(\app\models\PaymentLog::className(), ['subscription_id' => 'subscription_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionStatusType()
    {
        return $this->hasOne(\app\models\SubscriptionStatusType::className(), ['subscription_status_type_id' => 'subscription_status_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionType()
    {
        return $this->hasOne(\app\models\SubscriptionType::className(), ['subscription_type_id' => 'subscription_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionLogs()
    {
        return $this->hasMany(\app\models\SubscriptionLog::className(), ['subscription_id' => 'subscription_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\SubscriptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\SubscriptionQuery(get_called_class());
    }
}
