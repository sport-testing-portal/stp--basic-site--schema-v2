<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "import_file__sti_special_report_combo".
 *
 * @property integer $import_file__sti_special_report_combo_id
 * @property integer $import_file_id
 * @property integer $event_id
 * @property integer $record_id
 * @property string $name_full
 * @property string $user_id
 * @property string $drill_name
 * @property integer $attempt
 * @property string $splits_str
 * @property string $direction
 * @property string $result
 * @property string $drill_unit
 * @property string $trial_status
 * @property string $test_dt
 * @property string $import_file_name
 * @property string $import_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class ImportFileStiSpecialReportCombo extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['import_file_id', 'event_id', 'record_id', 'created_by', 'updated_by'], 'integer'],
            [['result'], 'number'],
            [['test_dt', 'import_dt', 'created_at', 'updated_at'], 'safe'],
            [['name_full', 'user_id', 'drill_name'], 'string', 'max' => 75],
            [['attempt'], 'string', 'max' => 4],
            [['splits_str', 'direction'], 'string', 'max' => 45],
            [['drill_unit', 'trial_status'], 'string', 'max' => 25],
            [['import_file_name'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import_file__sti_special_report_combo';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'import_file__sti_special_report_combo_id' => 'Import File  Sti Special Report Combo ID',
            'import_file_id' => 'Import File ID',
            'event_id' => 'Event ID',
            'record_id' => 'Record ID',
            'name_full' => 'Name Full',
            'user_id' => 'User ID',
            'drill_name' => 'Drill Name',
            'attempt' => 'Attempt',
            'splits_str' => 'Splits Str',
            'direction' => 'Direction',
            'result' => 'Result',
            'drill_unit' => 'Drill Unit',
            'trial_status' => 'Trial Status',
            'test_dt' => 'Test Dt',
            'import_file_name' => 'Import File Name',
            'import_dt' => 'Import Dt',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\ImportFileStiSpecialReportComboQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ImportFileStiSpecialReportComboQuery(get_called_class());
    }
}
