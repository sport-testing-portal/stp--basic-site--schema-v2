<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTestResult".
 *
 * @property string $person
 * @property string $ptype
 * @property string $gender
 * @property string $test_desc
 * @property string $test_type
 * @property string $split
 * @property string $score
 * @property string $test_units
 * @property integer $attempt
 * @property string $trial_status
 * @property integer $overall_ranking
 * @property integer $positional_ranking
 * @property string $total_overall_ranking
 * @property string $total_positional_ranking
 * @property string $score_url
 * @property string $video_url
 * @property string $test_date
 * @property string $test_date_iso
 * @property string $tester
 * @property integer $person_id
 * @property integer $player_id
 * @property integer $test_eval_summary_log_id
 * @property integer $test_eval_detail_log_id
 * @property integer $source_event_id
 */
class VwTestResult extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['score', 'total_overall_ranking', 'total_positional_ranking'], 'number'],
            [['overall_ranking', 'positional_ranking', 'person_id', 'player_id', 'test_eval_summary_log_id', 'test_eval_detail_log_id', 'source_event_id'], 'integer'],
            [['test_date_iso'], 'safe'],
            [['person'], 'string', 'max' => 90],
            [['ptype', 'test_desc', 'split', 'test_units', 'trial_status'], 'string', 'max' => 45],
            [['gender'], 'string', 'max' => 5],
            [['test_type', 'score_url', 'video_url'], 'string', 'max' => 150],
            [['attempt'], 'string', 'max' => 4],
            [['test_date'], 'string', 'max' => 40],
            [['tester'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTestResult';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person' => 'Person',
            'ptype' => 'Ptype',
            'gender' => 'Gender',
            'test_desc' => 'Test Desc',
            'test_type' => 'Test Type',
            'split' => 'Split',
            'score' => 'Score',
            'test_units' => 'Test Units',
            'attempt' => 'Attempt',
            'trial_status' => 'Trial Status',
            'overall_ranking' => 'Overall Ranking',
            'positional_ranking' => 'Positional Ranking',
            'total_overall_ranking' => 'Total Overall Ranking',
            'total_positional_ranking' => 'Total Positional Ranking',
            'score_url' => 'Score Url',
            'video_url' => 'Video Url',
            'test_date' => 'Test Date',
            'test_date_iso' => 'Test Date Iso',
            'tester' => 'Tester',
            'person_id' => 'Person ID',
            'player_id' => 'Player ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'source_event_id' => 'Source Event ID',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTestResultQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTestResultQuery(get_called_class());
    }
}
