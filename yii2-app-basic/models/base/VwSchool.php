<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwSchool".
 *
 * @property string $org_type
 * @property string $school_name
 * @property string $school_addr1
 * @property string $school_addr2
 * @property string $school_city
 * @property string $school_state
 * @property string $school_postal_code
 * @property string $school_website
 * @property integer $school_unit_id
 * @property integer $conference_id
 * @property string $updated_at
 * @property integer $org_id
 * @property integer $org_type_id
 * @property integer $school_id
 */
class VwSchool extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_type', 'school_name'], 'required'],
            [['school_unit_id', 'conference_id', 'org_id', 'org_type_id', 'school_id'], 'integer'],
            [['updated_at'], 'safe'],
            [['org_type', 'school_state'], 'string', 'max' => 45],
            [['school_name', 'school_addr1', 'school_addr2'], 'string', 'max' => 100],
            [['school_city'], 'string', 'max' => 75],
            [['school_postal_code'], 'string', 'max' => 25],
            [['school_website'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwSchool';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_type' => 'Org Type',
            'school_name' => 'School Name',
            'school_addr1' => 'School Addr1',
            'school_addr2' => 'School Addr2',
            'school_city' => 'School City',
            'school_state' => 'School State',
            'school_postal_code' => 'School Postal Code',
            'school_website' => 'School Website',
            'school_unit_id' => 'School Unit ID',
            'conference_id' => 'Conference ID',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'school_id' => 'School ID',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwSchoolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwSchoolQuery(get_called_class());
    }
}
