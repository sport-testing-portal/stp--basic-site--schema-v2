<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "notification_template_type".
 *
 * @property integer $notification_template_type_id
 * @property string $notification_template_type_name
 * @property string $notification_template_type_desc_short
 * @property string $notification_template_type_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\NotificationTemplate[] $notificationTemplates
 */
class NotificationTemplateType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'notificationTemplates'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['notification_template_type_name'], 'string', 'max' => 45],
            [['notification_template_type_desc_short'], 'string', 'max' => 75],
            [['notification_template_type_desc_long'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification_template_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_template_type_id' => 'Notification Template Type ID',
            'notification_template_type_name' => 'Notification Template Type Name',
            'notification_template_type_desc_short' => 'Notification Template Type Desc Short',
            'notification_template_type_desc_long' => 'Notification Template Type Desc Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationTemplates()
    {
        return $this->hasMany(\app\models\NotificationTemplate::className(), ['notification_template_type_id' => 'notification_template_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\NotificationTemplateTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\NotificationTemplateTypeQuery(get_called_class());
    }
}
