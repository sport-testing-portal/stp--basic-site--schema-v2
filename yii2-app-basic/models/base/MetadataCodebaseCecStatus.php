<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "metadata__codebase_cec_status".
 *
 * @property integer $cec_status_id
 * @property integer $codebase_id
 * @property integer $codebase_cec_id
 * @property string $cec_status
 * @property string $cec_status_tag
 * @property string $cec_status_bfr
 * @property string $cec_status_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\MetadataCodebase $codebase
 * @property \app\models\MetadataCodebaseCec $codebaseCec
 */
class MetadataCodebaseCecStatus extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'codebase',
            'codebaseCec'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['codebase_id', 'codebase_cec_id', 'cec_status'], 'required'],
            [['codebase_id', 'codebase_cec_id', 'created_by', 'updated_by'], 'integer'],
            [['cec_status_at', 'created_at', 'updated_at'], 'safe'],
            [['cec_status', 'cec_status_tag', 'cec_status_bfr'], 'string', 'max' => 45],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metadata__codebase_cec_status';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cec_status_id' => 'Cec Status ID',
            'codebase_id' => 'Codebase ID',
            'codebase_cec_id' => 'Codebase Cec ID',
            'cec_status' => 'Cec Status',
            'cec_status_tag' => 'Cec Status Tag',
            'cec_status_bfr' => 'Cec Status Bfr',
            'cec_status_at' => 'Cec Status At',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodebase()
    {
        return $this->hasOne(\app\models\MetadataCodebase::className(), ['codebase_id' => 'codebase_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCodebaseCec()
    {
        return $this->hasOne(\app\models\MetadataCodebaseCec::className(), ['codebase_cec_id' => 'codebase_cec_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\MetadataCodebaseCecStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\MetadataCodebaseCecStatusQuery(get_called_class());
    }
}
