<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_type".
 *
 * @property integer $test_eval_type_id
 * @property integer $test_eval_provider_id
 * @property integer $test_eval_category_id
 * @property string $test_eval_type_name
 * @property string $test_eval_type_name_sub_type
 * @property string $test_eval_type_desc_short
 * @property string $test_eval_type_desc_long
 * @property integer $test_eval_type_display_order
 * @property string $effective_begin_dt
 * @property string $effective_end_dt
 * @property string $units
 * @property string $format
 * @property integer $splits
 * @property integer $checkpoints
 * @property integer $attempts
 * @property string $qualifier
 * @property string $backend_calculation
 * @property string $metric_equivalent
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalDetailTestDesc[] $testEvalDetailTestDescs
 * @property \app\models\TestEvalLog[] $testEvalLogs
 * @property \app\models\TestEvalSummaryLog[] $testEvalSummaryLogs
 * @property \app\models\TestEvalProvider $testEvalProvider
 * @property \app\models\TestEvalCategory $testEvalCategory
 */
class TestEvalType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalDetailTestDescs',
            'testEvalLogs',
            'testEvalSummaryLogs',
            'testEvalProvider',
            'testEvalCategory'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_eval_provider_id', 'test_eval_category_id', 'test_eval_type_display_order', 'created_by', 'updated_by'], 'integer'],
            [['test_eval_type_name'], 'required'],
            [['effective_begin_dt', 'effective_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['test_eval_type_name', 'test_eval_type_name_sub_type', 'backend_calculation', 'metric_equivalent'], 'string', 'max' => 150],
            [['test_eval_type_desc_short'], 'string', 'max' => 75],
            [['test_eval_type_desc_long'], 'string', 'max' => 175],
            [['units', 'format', 'qualifier'], 'string', 'max' => 45],
            [['splits', 'checkpoints', 'attempts'], 'string', 'max' => 4],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_type_id' => 'Test Eval Type ID',
            'test_eval_provider_id' => 'Test Eval Provider ID',
            'test_eval_category_id' => 'Test Eval Category ID',
            'test_eval_type_name' => 'Test Eval Type Name',
            'test_eval_type_name_sub_type' => 'Test Eval Type Name Sub Type',
            'test_eval_type_desc_short' => 'Test Eval Type Desc Short',
            'test_eval_type_desc_long' => 'Test Eval Type Desc Long',
            'test_eval_type_display_order' => 'Test Eval Type Display Order',
            'effective_begin_dt' => 'Effective Begin Dt',
            'effective_end_dt' => 'Effective End Dt',
            'units' => 'Units',
            'format' => 'Format',
            'splits' => 'Splits',
            'checkpoints' => 'Checkpoints',
            'attempts' => 'Attempts',
            'qualifier' => 'Qualifier',
            'backend_calculation' => 'Backend Calculation',
            'metric_equivalent' => 'Metric Equivalent',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalDetailTestDescs()
    {
        return $this->hasMany(\app\models\TestEvalDetailTestDesc::className(), ['test_eval_type_id' => 'test_eval_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalLogs()
    {
        return $this->hasMany(\app\models\TestEvalLog::className(), ['test_eval_type_id' => 'test_eval_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalSummaryLogs()
    {
        return $this->hasMany(\app\models\TestEvalSummaryLog::className(), ['test_eval_type_id' => 'test_eval_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalProvider()
    {
        return $this->hasOne(\app\models\TestEvalProvider::className(), ['test_eval_provider_id' => 'test_eval_provider_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalCategory()
    {
        return $this->hasOne(\app\models\TestEvalCategory::className(), ['test_eval_category_id' => 'test_eval_category_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalTypeQuery(get_called_class());
    }
}
