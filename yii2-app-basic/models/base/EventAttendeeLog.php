<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "event_attendee_log".
 *
 * @property integer $event_attendee_log_id
 * @property integer $event_id
 * @property integer $person_id
 * @property integer $event_attendee_type_id
 * @property string $event_rsvp_dt
 * @property string $event_attendance_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Event $event
 * @property \app\models\EventAttendeeType $eventAttendeeType
 * @property \app\models\Person $person
 */
class EventAttendeeLog extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'event',
            'eventAttendeeType',
            'person'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'person_id', 'event_attendee_type_id', 'created_by', 'updated_by'], 'integer'],
            [['event_rsvp_dt', 'event_attendance_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_attendee_log';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'event_attendee_log_id' => 'Event Attendee Log ID',
            'event_id' => 'Event ID',
            'person_id' => 'Person ID',
            'event_attendee_type_id' => 'Event Attendee Type ID',
            'event_rsvp_dt' => 'Event Rsvp Dt',
            'event_attendance_dt' => 'Event Attendance Dt',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(\app\models\Event::className(), ['event_id' => 'event_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventAttendeeType()
    {
        return $this->hasOne(\app\models\EventAttendeeType::className(), ['event_attendee_type_id' => 'event_attendee_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerson()
    {
        return $this->hasOne(\app\models\Person::className(), ['person_id' => 'person_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\EventAttendeeLogQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\EventAttendeeLogQuery(get_called_class());
    }
}
