<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "note_type".
 *
 * @property integer $note_type_id
 * @property string $note_type_name
 * @property integer $display_order
 * @property string $note_type_desc_short
 * @property string $note_type_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\Note[] $notes
 */
class NoteType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'notes'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['note_type_name'], 'required'],
            [['display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['note_type_name', 'note_type_desc_short'], 'string', 'max' => 45],
            [['note_type_desc_long'], 'string', 'max' => 255],
            [['note_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'note_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'note_type_id' => 'Note Type ID',
            'note_type_name' => 'Note Type Name',
            'display_order' => 'Display Order',
            'note_type_desc_short' => 'Note Type Desc Short',
            'note_type_desc_long' => 'Note Type Desc Long',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotes()
    {
        return $this->hasMany(\app\models\Note::className(), ['note_type_id' => 'note_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\NoteTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\NoteTypeQuery(get_called_class());
    }
}
