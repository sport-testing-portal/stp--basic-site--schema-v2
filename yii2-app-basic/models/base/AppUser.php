<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "app_user".
 *
 * @property integer $app_user_id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $firstname
 * @property string $lastname
 * @property string $activation_key
 * @property string $created_on
 * @property string $updated_on
 * @property string $last_visit_on
 * @property string $password_set_on
 * @property integer $email_verified
 * @property integer $is_active
 * @property integer $is_disabled
 * @property string $one_time_password_secret
 * @property string $one_time_password_code
 * @property integer $one_time_password_counter
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property \app\models\User $appUser
 * @property \app\models\AppUserDevice[] $appUserDevices
 * @property \app\models\AppUserLocation[] $appUserLocations
 * @property \app\models\AppUserLoginAttempt[] $appUserLoginAttempts
 * @property \app\models\AppUserProfilePicture[] $appUserProfilePictures
 * @property \app\models\AppUserRemoteIdentity[] $appUserRemoteIdentities
 * @property \app\models\AppUserUsedPassword[] $appUserUsedPasswords
 * @property \app\models\Person[] $people
 */
class AppUser extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'appUser',
            'appUserDevices',
            'appUserLocations',
            'appUserLoginAttempts',
            'appUserProfilePictures',
            'appUserRemoteIdentities',
            'appUserUsedPasswords',
            'people'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['created_on', 'updated_on', 'last_visit_on', 'password_set_on', 'created_at', 'updated_at'], 'safe'],
            [['one_time_password_counter', 'created_by', 'updated_by'], 'integer'],
            [['username', 'password', 'email', 'one_time_password_secret', 'one_time_password_code'], 'string', 'max' => 255],
            [['firstname'], 'string', 'max' => 65],
            [['lastname', 'activation_key'], 'string', 'max' => 45],
            [['email_verified', 'is_active', 'is_disabled'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'app_user_id' => 'App User ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'activation_key' => 'Activation Key',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'last_visit_on' => 'Last Visit On',
            'password_set_on' => 'Password Set On',
            'email_verified' => 'Email Verified',
            'is_active' => 'Is Active',
            'is_disabled' => 'Is Disabled',
            'one_time_password_secret' => 'One Time Password Secret',
            'one_time_password_code' => 'One Time Password Code',
            'one_time_password_counter' => 'One Time Password Counter',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserDevices()
    {
        return $this->hasMany(\app\models\AppUserDevice::className(), ['app_user_id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserLocations()
    {
        return $this->hasMany(\app\models\AppUserLocation::className(), ['app_user_id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserLoginAttempts()
    {
        return $this->hasMany(\app\models\AppUserLoginAttempt::className(), ['app_user_id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserProfilePictures()
    {
        return $this->hasMany(\app\models\AppUserProfilePicture::className(), ['app_user_id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserRemoteIdentities()
    {
        return $this->hasMany(\app\models\AppUserRemoteIdentity::className(), ['app_user_id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserUsedPasswords()
    {
        return $this->hasMany(\app\models\AppUserUsedPassword::className(), ['app_user_id' => 'app_user_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPeople()
    {
        return $this->hasMany(\app\models\Person::className(), ['app_user_id' => 'app_user_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\AppUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\AppUserQuery(get_called_class());
    }
}
