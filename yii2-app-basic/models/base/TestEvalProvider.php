<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_provider".
 *
 * @property integer $test_eval_provider_id
 * @property string $test_eval_provider_name
 * @property string $test_eval_provider_desc_short
 * @property string $test_eval_provider_desc_long
 * @property string $test_eval_provider_website_url
 * @property string $test_eval_provider_logo_url
 * @property string $effective_begin_dt
 * @property string $effective_end_dt
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalType[] $testEvalTypes
 */
class TestEvalProvider extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalTypes'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['effective_begin_dt', 'effective_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['test_eval_provider_name', 'test_eval_provider_desc_short'], 'string', 'max' => 75],
            [['test_eval_provider_desc_long', 'test_eval_provider_website_url'], 'string', 'max' => 175],
            [['test_eval_provider_logo_url'], 'string', 'max' => 95],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_provider';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_provider_id' => 'Test Eval Provider ID',
            'test_eval_provider_name' => 'Test Eval Provider Name',
            'test_eval_provider_desc_short' => 'Test Eval Provider Desc Short',
            'test_eval_provider_desc_long' => 'Test Eval Provider Desc Long',
            'test_eval_provider_website_url' => 'Test Eval Provider Website Url',
            'test_eval_provider_logo_url' => 'Test Eval Provider Logo Url',
            'effective_begin_dt' => 'Effective Begin Dt',
            'effective_end_dt' => 'Effective End Dt',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalTypes()
    {
        return $this->hasMany(\app\models\TestEvalType::className(), ['test_eval_provider_id' => 'test_eval_provider_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalProviderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalProviderQuery(get_called_class());
    }
}
