<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "payment_xfer_entity".
 *
 * @property integer $payment_xfer_entity_id
 * @property string $payment_xfer_entity_name
 * @property string $payment_xfer_entity_client_secret
 * @property string $payment_xfer_entity_access_token
 * @property string $payment_xfer_entity_access_url
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PaymentXfer[] $paymentXfers
 */
class PaymentXferEntity extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'paymentXfers'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['payment_xfer_entity_name'], 'string', 'max' => 75],
            [['payment_xfer_entity_client_secret', 'payment_xfer_entity_access_token', 'payment_xfer_entity_access_url'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_xfer_entity';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_xfer_entity_id' => 'Payment Xfer Entity ID',
            'payment_xfer_entity_name' => 'Payment Xfer Entity Name',
            'payment_xfer_entity_client_secret' => 'Payment Xfer Entity Client Secret',
            'payment_xfer_entity_access_token' => 'Payment Xfer Entity Access Token',
            'payment_xfer_entity_access_url' => 'Payment Xfer Entity Access Url',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentXfers()
    {
        return $this->hasMany(\app\models\PaymentXfer::className(), ['payment_xfer_entity_id' => 'payment_xfer_entity_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PaymentXferEntityQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PaymentXferEntityQuery(get_called_class());
    }
}
