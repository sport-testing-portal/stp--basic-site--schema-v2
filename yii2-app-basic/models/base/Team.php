<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "team".
 *
 * @property integer $team_id
 * @property integer $org_id
 * @property integer $school_id
 * @property integer $sport_id
 * @property integer $camp_id
 * @property integer $gender_id
 * @property integer $age_group_id
 * @property string $team_name
 * @property string $team_gender
 * @property integer $team_division_id
 * @property integer $team_league_id
 * @property string $team_division
 * @property string $team_league
 * @property string $organizational_level
 * @property string $team_governing_body
 * @property string $team_age_group
 * @property string $team_website_url
 * @property string $team_schedule_url
 * @property string $team_schedule_uri
 * @property string $team_statistical_highlights
 * @property integer $team_competition_season_id
 * @property string $team_competition_season
 * @property string $team_city
 * @property integer $team_state_id
 * @property integer $team_country_id
 * @property integer $team_wins
 * @property integer $team_losses
 * @property integer $team_draws
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PlayerTeam[] $playerTeams
 * @property \app\models\AgeGroup $ageGroup
 * @property \app\models\Camp $camp
 * @property \app\models\Country $teamCountry
 * @property \app\models\TeamDivision $teamDivision
 * @property \app\models\Gender $gender
 * @property \app\models\TeamLeague $teamLeague
 * @property \app\models\Org $org
 * @property \app\models\School $school
 * @property \app\models\Sport $sport
 * @property \app\models\TeamCoach[] $teamCoaches
 * @property \app\models\TeamPlayer[] $teamPlayers
 * @property \app\models\TestEvalSummaryLog[] $testEvalSummaryLogs
 */
class Team extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'playerTeams',
            'ageGroup',
            'camp',
            'teamCountry',
            'teamDivision',
            'gender',
            'teamLeague',
            'org',
            'school',
            'sport',
            'teamCoaches',
            'teamPlayers',
            'testEvalSummaryLogs'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'school_id', 'sport_id', 'camp_id', 'gender_id', 'age_group_id', 'team_division_id', 'team_league_id', 'team_competition_season_id', 'team_state_id', 'team_country_id', 'team_wins', 'team_losses', 'team_draws', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['team_name', 'team_league', 'team_schedule_url', 'team_city'], 'string', 'max' => 75],
            [['team_gender', 'lock'], 'string', 'max' => 1],
            [['team_division'], 'string', 'max' => 10],
            [['organizational_level', 'team_governing_body', 'team_age_group'], 'string', 'max' => 45],
            [['team_website_url'], 'string', 'max' => 90],
            [['team_schedule_uri', 'team_competition_season'], 'string', 'max' => 60],
            [['team_statistical_highlights'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'team_id' => 'Team ID',
            'org_id' => 'Org ID',
            'school_id' => 'School ID',
            'sport_id' => 'Sport ID',
            'camp_id' => 'Camp ID',
            'gender_id' => 'Gender ID',
            'age_group_id' => 'Age Group ID',
            'team_name' => 'Team Name',
            'team_gender' => 'Team Gender',
            'team_division_id' => 'Team Division ID',
            'team_league_id' => 'Team League ID',
            'team_division' => 'Team Division',
            'team_league' => 'Team League',
            'organizational_level' => 'Organizational Level',
            'team_governing_body' => 'Team Governing Body',
            'team_age_group' => 'Team Age Group',
            'team_website_url' => 'Team Website Url',
            'team_schedule_url' => 'Team Schedule Url',
            'team_schedule_uri' => 'Team Schedule Uri',
            'team_statistical_highlights' => 'Team Statistical Highlights',
            'team_competition_season_id' => 'Team Competition Season ID',
            'team_competition_season' => 'Team Competition Season',
            'team_city' => 'Team City',
            'team_state_id' => 'Team State ID',
            'team_country_id' => 'Team Country ID',
            'team_wins' => 'Team Wins',
            'team_losses' => 'Team Losses',
            'team_draws' => 'Team Draws',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlayerTeams()
    {
        return $this->hasMany(\app\models\PlayerTeam::className(), ['team_id' => 'team_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgeGroup()
    {
        return $this->hasOne(\app\models\AgeGroup::className(), ['age_group_id' => 'age_group_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCamp()
    {
        return $this->hasOne(\app\models\Camp::className(), ['camp_id' => 'camp_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamCountry()
    {
        return $this->hasOne(\app\models\Country::className(), ['country_id' => 'team_country_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamDivision()
    {
        return $this->hasOne(\app\models\TeamDivision::className(), ['team_division_id' => 'team_division_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGender()
    {
        return $this->hasOne(\app\models\Gender::className(), ['gender_id' => 'gender_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamLeague()
    {
        return $this->hasOne(\app\models\TeamLeague::className(), ['team_league_id' => 'team_league_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrg()
    {
        return $this->hasOne(\app\models\Org::className(), ['org_id' => 'org_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\app\models\School::className(), ['school_id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(\app\models\Sport::className(), ['sport_id' => 'sport_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamCoaches()
    {
        return $this->hasMany(\app\models\TeamCoach::className(), ['team_id' => 'team_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamPlayers()
    {
        return $this->hasMany(\app\models\TeamPlayer::className(), ['team_id' => 'team_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalSummaryLogs()
    {
        return $this->hasMany(\app\models\TestEvalSummaryLog::className(), ['team_id' => 'team_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TeamQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TeamQuery(get_called_class());
    }
}
