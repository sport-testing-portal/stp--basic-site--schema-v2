<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "person_certification_type".
 *
 * @property integer $person_certification_type_id
 * @property integer $person_type_id
 * @property string $person_certification_type_name
 * @property string $person_certification_subtype_name
 * @property integer $person_certification_type_display_order
 * @property string $person_certification_type_description_short
 * @property string $person_certification_type_description_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\PersonCertification[] $personCertifications
 */
class PersonCertificationType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'personCertifications'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_type_id', 'person_certification_type_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['person_certification_type_name', 'person_certification_subtype_name', 'person_certification_type_description_short'], 'string', 'max' => 45],
            [['person_certification_type_description_long'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'person_certification_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'person_certification_type_id' => 'Person Certification Type ID',
            'person_type_id' => 'Person Type ID',
            'person_certification_type_name' => 'Person Certification Type Name',
            'person_certification_subtype_name' => 'Person Certification Subtype Name',
            'person_certification_type_display_order' => 'Person Certification Type Display Order',
            'person_certification_type_description_short' => 'Person Certification Type Description Short',
            'person_certification_type_description_long' => 'Person Certification Type Description Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersonCertifications()
    {
        return $this->hasMany(\app\models\PersonCertification::className(), ['person_certification_type_id' => 'person_certification_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\PersonCertificationTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\PersonCertificationTypeQuery(get_called_class());
    }
}
