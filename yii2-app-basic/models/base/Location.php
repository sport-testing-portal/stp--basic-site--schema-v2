<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "location".
 *
 * @property integer $location_id
 * @property string $city
 * @property string $state_or_region
 * @property string $country
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\AppUserLocation[] $appUserLocations
 */
class Location extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'appUserLocations'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['city', 'state_or_region', 'country'], 'string', 'max' => 45],
            [['city', 'state_or_region', 'country'], 'unique', 'targetAttribute' => ['city', 'state_or_region', 'country'], 'message' => 'The combination of City, State Or Region and Country has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id' => 'Location ID',
            'city' => 'City',
            'state_or_region' => 'State Or Region',
            'country' => 'Country',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserLocations()
    {
        return $this->hasMany(\app\models\AppUserLocation::className(), ['location_id' => 'location_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\LocationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\LocationQuery(get_called_class());
    }
}
