<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "org_level".
 *
 * @property integer $org_level_id
 * @property integer $org_type_id
 * @property string $org_level_name
 * @property string $org_level_desc_short
 * @property string $org_level_desc_long
 * @property integer $org_level_display_order
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Org[] $orgs
 * @property \app\models\OrgType $orgType
 */
class OrgLevel extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'orgs',
            'orgType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_type_id', 'org_level_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_level_name', 'org_level_desc_short'], 'string', 'max' => 75],
            [['org_level_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_level';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_level_id' => 'Org Level ID',
            'org_type_id' => 'Org Type ID',
            'org_level_name' => 'Org Level Name',
            'org_level_desc_short' => 'Org Level Desc Short',
            'org_level_desc_long' => 'Org Level Desc Long',
            'org_level_display_order' => 'Org Level Display Order',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgs()
    {
        return $this->hasMany(\app\models\Org::className(), ['org_level_id' => 'org_level_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgType()
    {
        return $this->hasOne(\app\models\OrgType::className(), ['org_type_id' => 'org_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\OrgLevelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OrgLevelQuery(get_called_class());
    }
}
