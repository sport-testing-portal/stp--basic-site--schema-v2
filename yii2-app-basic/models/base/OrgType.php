<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "org_type".
 *
 * @property integer $org_type_id
 * @property string $org_type_name
 * @property string $org_type_sub_type
 * @property string $org_type_desc_short
 * @property string $org_type_desc_long
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Org[] $orgs
 * @property \app\models\OrgLevel[] $orgLevels
 */
class OrgType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'orgs',
            'orgLevels'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['org_type_name', 'org_type_sub_type', 'org_type_desc_short', 'org_type_desc_long'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['org_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'org_type';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_type_id' => 'Org Type ID',
            'org_type_name' => 'Org Type Name',
            'org_type_sub_type' => 'Org Type Sub Type',
            'org_type_desc_short' => 'Org Type Desc Short',
            'org_type_desc_long' => 'Org Type Desc Long',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgs()
    {
        return $this->hasMany(\app\models\Org::className(), ['org_type_id' => 'org_type_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgLevels()
    {
        return $this->hasMany(\app\models\OrgLevel::className(), ['org_type_id' => 'org_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\OrgTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\OrgTypeQuery(get_called_class());
    }
}
