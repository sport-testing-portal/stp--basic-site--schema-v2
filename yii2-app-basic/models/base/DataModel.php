<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "data_model".
 *
 * @property integer $data_model_id
 * @property string $table_name
 * @property string $table_structure_digest
 * @property string $data_model_digest
 * @property string $data_model_digest_created_at
 * @property string $data_model_created_at
 * @property string $dupe_detection_field_list
 * @property string $trigger__bfr_insert_uri
 * @property string $trigger__bfr_update_uri
 * @property string $trigger__bfr_template_cec_ver
 * @property string $data_model_cec_ver
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 */
class DataModel extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data_model_digest_created_at', 'data_model_created_at', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['table_name'], 'string', 'max' => 75],
            [['table_structure_digest', 'data_model_digest'], 'string', 'max' => 40],
            [['dupe_detection_field_list'], 'string', 'max' => 250],
            [['trigger__bfr_insert_uri', 'trigger__bfr_update_uri'], 'string', 'max' => 150],
            [['trigger__bfr_template_cec_ver', 'data_model_cec_ver'], 'string', 'max' => 10],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'data_model';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'data_model_id' => 'Data Model ID',
            'table_name' => 'Table Name',
            'table_structure_digest' => 'Table Structure Digest',
            'data_model_digest' => 'Data Model Digest',
            'data_model_digest_created_at' => 'Data Model Digest Created At',
            'data_model_created_at' => 'Data Model Created At',
            'dupe_detection_field_list' => 'Dupe Detection Field List',
            'trigger__bfr_insert_uri' => 'Trigger  Bfr Insert Uri',
            'trigger__bfr_update_uri' => 'Trigger  Bfr Update Uri',
            'trigger__bfr_template_cec_ver' => 'Trigger  Bfr Template Cec Ver',
            'data_model_cec_ver' => 'Data Model Cec Ver',
            'lock' => 'Lock',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\DataModelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\DataModelQuery(get_called_class());
    }
}
