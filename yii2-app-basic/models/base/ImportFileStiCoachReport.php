<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "import_file__sti_coach_report".
 *
 * @property integer $import_file__sti_coach_report_id
 * @property integer $import_file_id
 * @property string $field_0001
 * @property string $field_0002
 * @property string $field_0003
 * @property string $field_0004
 * @property string $field_0005
 * @property string $field_0006
 * @property string $field_0007
 * @property string $field_0008
 * @property string $field_0009
 * @property string $field_0010
 * @property string $field_0011
 * @property string $field_0012
 * @property string $field_0013
 * @property string $field_0014
 * @property string $field_0015
 * @property string $field_0016
 * @property string $field_0017
 * @property string $field_0018
 * @property string $field_0019
 * @property string $field_0020
 * @property string $field_0021
 * @property string $field_0022
 * @property string $field_0023
 * @property string $field_0024
 * @property string $field_0025
 * @property string $field_0026
 * @property string $field_0027
 * @property string $field_0028
 * @property string $field_0029
 * @property string $field_0030
 * @property string $field_0031
 * @property string $field_0032
 * @property string $field_0033
 * @property string $field_0034
 * @property string $field_0035
 * @property string $field_0036
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \app\models\ImportFile $importFile
 */
class ImportFileStiCoachReport extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'importFile'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['import_file_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['field_0001', 'field_0002', 'field_0003', 'field_0004', 'field_0005', 'field_0006', 'field_0007', 'field_0008', 'field_0009', 'field_0010', 'field_0011', 'field_0012', 'field_0013', 'field_0014', 'field_0015', 'field_0016', 'field_0017', 'field_0018', 'field_0019', 'field_0020', 'field_0021', 'field_0022', 'field_0023', 'field_0024', 'field_0025', 'field_0026', 'field_0027', 'field_0028', 'field_0029', 'field_0030', 'field_0031', 'field_0032', 'field_0033', 'field_0034', 'field_0035', 'field_0036'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'import_file__sti_coach_report';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'import_file__sti_coach_report_id' => 'Import File  Sti Coach Report ID',
            'import_file_id' => 'Import File ID',
            'field_0001' => 'Field 0001',
            'field_0002' => 'Field 0002',
            'field_0003' => 'Field 0003',
            'field_0004' => 'Field 0004',
            'field_0005' => 'Field 0005',
            'field_0006' => 'Field 0006',
            'field_0007' => 'Field 0007',
            'field_0008' => 'Field 0008',
            'field_0009' => 'Field 0009',
            'field_0010' => 'Field 0010',
            'field_0011' => 'Field 0011',
            'field_0012' => 'Field 0012',
            'field_0013' => 'Field 0013',
            'field_0014' => 'Field 0014',
            'field_0015' => 'Field 0015',
            'field_0016' => 'Field 0016',
            'field_0017' => 'Field 0017',
            'field_0018' => 'Field 0018',
            'field_0019' => 'Field 0019',
            'field_0020' => 'Field 0020',
            'field_0021' => 'Field 0021',
            'field_0022' => 'Field 0022',
            'field_0023' => 'Field 0023',
            'field_0024' => 'Field 0024',
            'field_0025' => 'Field 0025',
            'field_0026' => 'Field 0026',
            'field_0027' => 'Field 0027',
            'field_0028' => 'Field 0028',
            'field_0029' => 'Field 0029',
            'field_0030' => 'Field 0030',
            'field_0031' => 'Field 0031',
            'field_0032' => 'Field 0032',
            'field_0033' => 'Field 0033',
            'field_0034' => 'Field 0034',
            'field_0035' => 'Field 0035',
            'field_0036' => 'Field 0036',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportFile()
    {
        return $this->hasOne(\app\models\ImportFile::className(), ['import_file_id' => 'import_file_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\ImportFileStiCoachReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\ImportFileStiCoachReportQuery(get_called_class());
    }
}
