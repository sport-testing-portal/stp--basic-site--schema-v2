<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "vwTeam_leftouter".
 *
 * @property string $org_name
 * @property string $org_type_name
 * @property string $team_name
 * @property string $team_age_group
 * @property string $team_division
 * @property string $organizational_level
 * @property string $team_governing_body
 * @property string $team_website_url
 * @property string $team_schedule_url
 * @property string $team_schedule_uri
 * @property string $team_statistical_highlights
 * @property string $gender_code
 * @property string $gender_desc
 * @property string $coach_name
 * @property string $player_name
 * @property string $sport_name
 * @property integer $org_id
 * @property integer $org_type_id
 * @property integer $team_id
 * @property integer $coach_id
 * @property string $coach__person_name_full
 * @property string $coach__person_email_work
 * @property integer $coach__person_id
 * @property integer $coach__created_by
 * @property integer $player_id
 * @property integer $player__created_by
 * @property integer $team_player_id
 * @property integer $team_player__created_by
 * @property integer $team_coach_id
 * @property integer $team_coach__created_by
 * @property integer $player__person_id
 * @property integer $player__person_org_id
 * @property string $team_coach_player_composite_key
 * @property string $team_created_by_username
 * @property string $team_updated_by_username
 * @property string $team_updated_by_user_flname
 * @property string $team_updated_by_user_email
 * @property integer $team__created_by
 * @property integer $team__updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $team__created_at
 * @property string $team__updated_at
 */
class VwTeamLeftouter extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['org_id', 'org_type_id', 'team_id', 'coach_id', 'coach__person_id', 'coach__created_by', 'player_id', 'player__created_by', 'team_player_id', 'team_player__created_by', 'team_coach_id', 'team_coach__created_by', 'player__person_id', 'player__person_org_id', 'team__created_by', 'team__updated_by'], 'integer'],
            [['created_at', 'updated_at', 'team__created_at', 'team__updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 100],
            [['org_type_name', 'team_age_group', 'organizational_level', 'team_governing_body', 'sport_name'], 'string', 'max' => 45],
            [['team_name', 'team_schedule_url'], 'string', 'max' => 75],
            [['team_division'], 'string', 'max' => 10],
            [['team_website_url', 'coach_name', 'player_name', 'coach__person_name_full'], 'string', 'max' => 90],
            [['team_schedule_uri', 'coach__person_email_work'], 'string', 'max' => 60],
            [['team_statistical_highlights'], 'string', 'max' => 300],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['team_coach_player_composite_key'], 'string', 'max' => 35],
            [['team_created_by_username', 'team_updated_by_username', 'team_updated_by_user_email'], 'string', 'max' => 255],
            [['team_updated_by_user_flname'], 'string', 'max' => 131],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwTeam_leftouter';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'org_name' => 'Org Name',
            'org_type_name' => 'Org Type Name',
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'organizational_level' => 'Organizational Level',
            'team_governing_body' => 'Team Governing Body',
            'team_website_url' => 'Team Website Url',
            'team_schedule_url' => 'Team Schedule Url',
            'team_schedule_uri' => 'Team Schedule Uri',
            'team_statistical_highlights' => 'Team Statistical Highlights',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'sport_name' => 'Sport Name',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'coach__person_name_full' => 'Coach  Person Name Full',
            'coach__person_email_work' => 'Coach  Person Email Work',
            'coach__person_id' => 'Coach  Person ID',
            'coach__created_by' => 'Coach  Created By',
            'player_id' => 'Player ID',
            'player__created_by' => 'Player  Created By',
            'team_player_id' => 'Team Player ID',
            'team_player__created_by' => 'Team Player  Created By',
            'team_coach_id' => 'Team Coach ID',
            'team_coach__created_by' => 'Team Coach  Created By',
            'player__person_id' => 'Player  Person ID',
            'player__person_org_id' => 'Player  Person Org ID',
            'team_coach_player_composite_key' => 'Team Coach Player Composite Key',
            'team_created_by_username' => 'Team Created By Username',
            'team_updated_by_username' => 'Team Updated By Username',
            'team_updated_by_user_flname' => 'Team Updated By User Flname',
            'team_updated_by_user_email' => 'Team Updated By User Email',
            'team__created_by' => 'Team  Created By',
            'team__updated_by' => 'Team  Updated By',
            'team__created_at' => 'Team  Created At',
            'team__updated_at' => 'Team  Updated At',
        ];
    }

    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\VwTeamLeftouterQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\VwTeamLeftouterQuery(get_called_class());
    }
}
