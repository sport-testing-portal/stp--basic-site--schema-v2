<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "governing_body".
 *
 * @property integer $governing_body_id
 * @property integer $sport_id
 * @property string $governing_body_name
 * @property string $governing_body_short_desc
 * @property string $governing_body_long_desc
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\Sport $sport
 */
class GoverningBody extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'sport'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sport_id', 'created_by', 'updated_by'], 'integer'],
            [['governing_body_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['governing_body_name', 'governing_body_short_desc'], 'string', 'max' => 45],
            [['governing_body_long_desc'], 'string', 'max' => 300],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'governing_body';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'governing_body_id' => 'Governing Body ID',
            'sport_id' => 'Sport ID',
            'governing_body_name' => 'Governing Body Name',
            'governing_body_short_desc' => 'Governing Body Short Desc',
            'governing_body_long_desc' => 'Governing Body Long Desc',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSport()
    {
        return $this->hasOne(\app\models\Sport::className(), ['sport_id' => 'sport_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\GoverningBodyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\GoverningBodyQuery(get_called_class());
    }
}
