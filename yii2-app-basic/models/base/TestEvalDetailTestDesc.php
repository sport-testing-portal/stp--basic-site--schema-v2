<?php

namespace app\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "test_eval_detail_test_desc".
 *
 * @property integer $test_eval_detail_test_desc_id
 * @property integer $test_eval_type_id
 * @property string $test_eval_detail_test_desc
 * @property integer $test_eval_detail_test_desc_display_order
 * @property string $rating_bias
 * @property string $units
 * @property string $format
 * @property integer $splits
 * @property integer $checkpoints
 * @property integer $attempts
 * @property string $qualifier
 * @property string $backend_calculation
 * @property string $metric_equivalent
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $lock
 *
 * @property \app\models\TestEvalDetailLog[] $testEvalDetailLogs
 * @property \app\models\TestEvalType $testEvalType
 */
class TestEvalDetailTestDesc extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'testEvalDetailLogs',
            'testEvalType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_eval_type_id', 'test_eval_detail_test_desc_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['test_eval_detail_test_desc', 'units', 'format', 'qualifier'], 'string', 'max' => 45],
            [['rating_bias'], 'string', 'max' => 5],
            [['splits', 'checkpoints', 'attempts'], 'string', 'max' => 4],
            [['backend_calculation', 'metric_equivalent'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test_eval_detail_test_desc';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'test_eval_detail_test_desc_id' => 'Test Eval Detail Test Desc ID',
            'test_eval_type_id' => 'Test Eval Type ID',
            'test_eval_detail_test_desc' => 'Test Eval Detail Test Desc',
            'test_eval_detail_test_desc_display_order' => 'Test Eval Detail Test Desc Display Order',
            'rating_bias' => 'Rating Bias',
            'units' => 'Units',
            'format' => 'Format',
            'splits' => 'Splits',
            'checkpoints' => 'Checkpoints',
            'attempts' => 'Attempts',
            'qualifier' => 'Qualifier',
            'backend_calculation' => 'Backend Calculation',
            'metric_equivalent' => 'Metric Equivalent',
            'lock' => 'Lock',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalDetailLogs()
    {
        return $this->hasMany(\app\models\TestEvalDetailLog::className(), ['test_eval_detail_test_desc_id' => 'test_eval_detail_test_desc_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTestEvalType()
    {
        return $this->hasOne(\app\models\TestEvalType::className(), ['test_eval_type_id' => 'test_eval_type_id']);
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \app\models\TestEvalDetailTestDescQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\TestEvalDetailTestDescQuery(get_called_class());
    }
}
