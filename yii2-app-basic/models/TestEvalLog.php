<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalLog as BaseTestEvalLog;

/**
 * This is the model class for table "test_eval_log".
 */
class TestEvalLog extends BaseTestEvalLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_eval_type_id', 'test_eval_summary_log_id', 'rfid', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_log_id' => 'Test Eval Log ID',
            'test_eval_type_id' => 'Test Eval Type ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'rfid' => 'Rfid',
            'lock' => 'Lock',
        ];
    }
}
