<?php

namespace app\models;

use Yii;
use \app\models\base\VwOrgTeamLeftouter as BaseVwOrgTeamLeftouter;

/**
 * This is the model class for table "vwOrgTeam_leftouter".
 */
class VwOrgTeamLeftouter extends BaseVwOrgTeamLeftouter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_name'], 'required'],
            [['org_id', 'org_type_id', 'team_id', 'coach_id', 'player_id', 'coach__person_id', 'player__person_id', 'player__person_org_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 100],
            [['org_type_name', 'team_age_group', 'sport_name'], 'string', 'max' => 45],
            [['team_name'], 'string', 'max' => 75],
            [['team_division'], 'string', 'max' => 10],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['coach_name', 'player_name'], 'string', 'max' => 90],
            [['team_created_by_username', 'team_updated_by_username'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_name' => 'Org Name',
            'org_type_name' => 'Org Type Name',
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'sport_name' => 'Sport Name',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'coach__person_id' => 'Coach  Person ID',
            'player__person_id' => 'Player  Person ID',
            'player__person_org_id' => 'Player  Person Org ID',
            'team_created_by_username' => 'Team Created By Username',
            'team_updated_by_username' => 'Team Updated By Username',
        ];
    }
}
