<?php

namespace app\models;

use Yii;
use \app\models\base\CoachSport as BaseCoachSport;

/**
 * This is the model class for table "coach_sport".
 */
class CoachSport extends BaseCoachSport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['coach_id', 'sport_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'coach_sport_id' => 'Coach Sport ID',
            'coach_id' => 'Coach ID',
            'sport_id' => 'Sport ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
}
