<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserDevice as BaseAppUserDevice;

/**
 * This is the model class for table "app_user_device".
 */
class AppUserDevice extends BaseAppUserDevice
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['app_user_id', 'user_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['device_description'], 'string', 'max' => 253],
            [['screen_dimensions'], 'string', 'max' => 25],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_device_id' => 'App User Device ID',
            'app_user_id' => 'App User ID',
            'user_id' => 'User ID',
            'device_description' => 'Device Description',
            'screen_dimensions' => 'Screen Dimensions',
            'lock' => 'Lock',
        ];
    }
}
