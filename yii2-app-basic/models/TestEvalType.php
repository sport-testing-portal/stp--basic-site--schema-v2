<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalType as BaseTestEvalType;

/**
 * This is the model class for table "test_eval_type".
 */
class TestEvalType extends BaseTestEvalType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_eval_provider_id', 'test_eval_category_id', 'test_eval_type_display_order', 'created_by', 'updated_by'], 'integer'],
            [['test_eval_type_name'], 'required'],
            [['effective_begin_dt', 'effective_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['test_eval_type_name', 'test_eval_type_name_sub_type', 'backend_calculation', 'metric_equivalent'], 'string', 'max' => 150],
            [['test_eval_type_desc_short'], 'string', 'max' => 75],
            [['test_eval_type_desc_long'], 'string', 'max' => 175],
            [['units', 'format', 'qualifier'], 'string', 'max' => 45],
            [['splits', 'checkpoints', 'attempts'], 'string', 'max' => 4],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_type_id' => 'Test Eval Type ID',
            'test_eval_provider_id' => 'Test Eval Provider ID',
            'test_eval_category_id' => 'Test Eval Category ID',
            'test_eval_type_name' => 'Test Eval Type Name',
            'test_eval_type_name_sub_type' => 'Test Eval Type Name Sub Type',
            'test_eval_type_desc_short' => 'Test Eval Type Desc Short',
            'test_eval_type_desc_long' => 'Test Eval Type Desc Long',
            'test_eval_type_display_order' => 'Test Eval Type Display Order',
            'effective_begin_dt' => 'Effective Begin Dt',
            'effective_end_dt' => 'Effective End Dt',
            'units' => 'Units',
            'format' => 'Format',
            'splits' => 'Splits',
            'checkpoints' => 'Checkpoints',
            'attempts' => 'Attempts',
            'qualifier' => 'Qualifier',
            'backend_calculation' => 'Backend Calculation',
            'metric_equivalent' => 'Metric Equivalent',
            'lock' => 'Lock',
        ];
    }
}
