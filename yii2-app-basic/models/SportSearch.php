<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sport;

/**
 * app\models\SportSearch represents the model behind the search form about `app\models\Sport`.
 */
 class SportSearch extends Sport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sport_id', 'created_by', 'updated_by'], 'integer'],
            [['sport_name', 'sport_desc_short', 'sport_desc_long', 'high_school_yn', 'ncaa_yn', 'olympic_yn', 'xgame_sport_yn', 'ncaa_sport_name', 'olympic_sport_name', 'xgame_sport_name', 'ncaa_sport_type', 'ncaa_sport_season_male', 'ncaa_sport_season_female', 'ncaa_sport_season_coed', 'olympic_sport_season', 'xgame_sport_season', 'olympic_sport_gender', 'xgame_sport_gender', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sport::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'sport_id' => $this->sport_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'sport_name', $this->sport_name])
            ->andFilterWhere(['like', 'sport_desc_short', $this->sport_desc_short])
            ->andFilterWhere(['like', 'sport_desc_long', $this->sport_desc_long])
            ->andFilterWhere(['like', 'high_school_yn', $this->high_school_yn])
            ->andFilterWhere(['like', 'ncaa_yn', $this->ncaa_yn])
            ->andFilterWhere(['like', 'olympic_yn', $this->olympic_yn])
            ->andFilterWhere(['like', 'xgame_sport_yn', $this->xgame_sport_yn])
            ->andFilterWhere(['like', 'ncaa_sport_name', $this->ncaa_sport_name])
            ->andFilterWhere(['like', 'olympic_sport_name', $this->olympic_sport_name])
            ->andFilterWhere(['like', 'xgame_sport_name', $this->xgame_sport_name])
            ->andFilterWhere(['like', 'ncaa_sport_type', $this->ncaa_sport_type])
            ->andFilterWhere(['like', 'ncaa_sport_season_male', $this->ncaa_sport_season_male])
            ->andFilterWhere(['like', 'ncaa_sport_season_female', $this->ncaa_sport_season_female])
            ->andFilterWhere(['like', 'ncaa_sport_season_coed', $this->ncaa_sport_season_coed])
            ->andFilterWhere(['like', 'olympic_sport_season', $this->olympic_sport_season])
            ->andFilterWhere(['like', 'xgame_sport_season', $this->xgame_sport_season])
            ->andFilterWhere(['like', 'olympic_sport_gender', $this->olympic_sport_gender])
            ->andFilterWhere(['like', 'xgame_sport_gender', $this->xgame_sport_gender])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
