<?php

namespace app\models;

use Yii;
use \app\models\base\VwCoachTeamAssignmentStatus as BaseVwCoachTeamAssignmentStatus;

/**
 * This is the model class for table "vwCoach_Team_Assignment_Status".
 */
class VwCoachTeamAssignmentStatus extends BaseVwCoachTeamAssignmentStatus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'org_id', 'coach_id', 'app_user_id', 'person_type_id', 'coach_type_id', 'gender_id', 'person_weight', 'created_by', 'updated_by', 'coach_created_by', 'coach_updated_by'], 'integer'],
            [['person_date_of_birth', 'created_at', 'updated_at'], 'safe'],
            [['coach_team_assignment_status', 'person_tshirt_size'], 'string', 'max' => 10],
            [['person_type_name', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region'], 'string', 'max' => 45],
            [['coach_type_name', 'person_city'], 'string', 'max' => 75],
            [['person_name_prefix', 'person_postal_code'], 'string', 'max' => 25],
            [['person_name_full'], 'string', 'max' => 90],
            [['gender_code', 'person_height'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['person_email_personal', 'person_email_work'], 'string', 'max' => 60],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'coach_info_source_scrape_url'], 'string', 'max' => 100],
            [['person_country_code'], 'string', 'max' => 3],
            [['person_created_by_username', 'person_updated_by_username', 'coach_created_by_username', 'coach_updated_by_username'], 'string', 'max' => 255],
            [['coach_specialty'], 'string', 'max' => 95],
            [['coach_certifications', 'coach_qrcode_uri'], 'string', 'max' => 150],
            [['coach_comments'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'coach_team_assignment_status' => 'Coach Team Assignment Status',
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'coach_id' => 'Coach ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'coach_type_id' => 'Coach Type ID',
            'coach_type_name' => 'Coach Type Name',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'gender_id' => 'Gender ID',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'person_created_by_username' => 'Person Created By Username',
            'person_updated_by_username' => 'Person Updated By Username',
            'coach_specialty' => 'Coach Specialty',
            'coach_certifications' => 'Coach Certifications',
            'coach_comments' => 'Coach Comments',
            'coach_qrcode_uri' => 'Coach Qrcode Uri',
            'coach_info_source_scrape_url' => 'Coach Info Source Scrape Url',
            'coach_created_by_username' => 'Coach Created By Username',
            'coach_created_by' => 'Coach Created By',
            'coach_updated_by_username' => 'Coach Updated By Username',
            'coach_updated_by' => 'Coach Updated By',
        ];
    }
}
