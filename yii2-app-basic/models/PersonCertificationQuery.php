<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PersonCertification]].
 *
 * @see PersonCertification
 */
class PersonCertificationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PersonCertification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PersonCertification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
