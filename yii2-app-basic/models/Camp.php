<?php

namespace app\models;

use Yii;
use \app\models\base\Camp as BaseCamp;

/**
 * This is the model class for table "camp".
 */
class Camp extends BaseCamp
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'camp_cost_regular', 'camp_cost_early_registration', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['camp_name'], 'string', 'max' => 110],
            [['camp_specialty', 'camp_session_desc', 'camp_scholarship_application_info', 'camp_scholarship_application_request', 'camp_organizer_description'], 'string', 'max' => 45],
            [['camp_team_discounts_available_yn', 'camp_scholarships_available_yn', 'lock'], 'string', 'max' => 1],
            [['camp_website_url', 'camp_session_url', 'camp_registration_url', 'camp_twitter_url', 'camp_facebook_url'], 'string', 'max' => 175],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_id' => 'Camp ID',
            'org_id' => 'Org ID',
            'camp_name' => 'Camp Name',
            'camp_specialty' => 'Camp Specialty',
            'camp_cost_regular' => 'Camp Cost Regular',
            'camp_cost_early_registration' => 'Camp Cost Early Registration',
            'camp_team_discounts_available_yn' => 'Camp Team Discounts Available Yn',
            'camp_scholarships_available_yn' => 'Camp Scholarships Available Yn',
            'camp_session_desc' => 'Camp Session Desc',
            'camp_website_url' => 'Camp Website Url',
            'camp_session_url' => 'Camp Session Url',
            'camp_registration_url' => 'Camp Registration Url',
            'camp_twitter_url' => 'Camp Twitter Url',
            'camp_facebook_url' => 'Camp Facebook Url',
            'camp_scholarship_application_info' => 'Camp Scholarship Application Info',
            'camp_scholarship_application_request' => 'Camp Scholarship Application Request',
            'camp_organizer_description' => 'Camp Organizer Description',
            'lock' => 'Lock',
        ];
    }
}
