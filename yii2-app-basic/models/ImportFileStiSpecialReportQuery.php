<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ImportFileStiSpecialReport]].
 *
 * @see ImportFileStiSpecialReport
 */
class ImportFileStiSpecialReportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ImportFileStiSpecialReport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImportFileStiSpecialReport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
