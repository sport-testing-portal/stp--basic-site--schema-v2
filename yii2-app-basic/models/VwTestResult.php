<?php

namespace app\models;

use Yii;
use \app\models\base\VwTestResult as BaseVwTestResult;

/**
 * This is the model class for table "vwTestResult".
 */
class VwTestResult extends BaseVwTestResult
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['score', 'total_overall_ranking', 'total_positional_ranking'], 'number'],
            [['overall_ranking', 'positional_ranking', 'person_id', 'player_id', 'test_eval_summary_log_id', 'test_eval_detail_log_id', 'source_event_id'], 'integer'],
            [['test_date_iso'], 'safe'],
            [['person'], 'string', 'max' => 90],
            [['ptype', 'test_desc', 'split', 'test_units', 'trial_status'], 'string', 'max' => 45],
            [['gender'], 'string', 'max' => 5],
            [['test_type', 'score_url', 'video_url'], 'string', 'max' => 150],
            [['attempt'], 'string', 'max' => 4],
            [['test_date'], 'string', 'max' => 40],
            [['tester'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person' => 'Person',
            'ptype' => 'Ptype',
            'gender' => 'Gender',
            'test_desc' => 'Test Desc',
            'test_type' => 'Test Type',
            'split' => 'Split',
            'score' => 'Score',
            'test_units' => 'Test Units',
            'attempt' => 'Attempt',
            'trial_status' => 'Trial Status',
            'overall_ranking' => 'Overall Ranking',
            'positional_ranking' => 'Positional Ranking',
            'total_overall_ranking' => 'Total Overall Ranking',
            'total_positional_ranking' => 'Total Positional Ranking',
            'score_url' => 'Score Url',
            'video_url' => 'Video Url',
            'test_date' => 'Test Date',
            'test_date_iso' => 'Test Date Iso',
            'tester' => 'Tester',
            'person_id' => 'Person ID',
            'player_id' => 'Player ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'test_eval_detail_log_id' => 'Test Eval Detail Log ID',
            'source_event_id' => 'Source Event ID',
        ];
    }
}
