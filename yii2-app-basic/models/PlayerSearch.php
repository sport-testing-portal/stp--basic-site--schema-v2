<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Player;

/**
 * app\models\PlayerSearch represents the model behind the search form about `app\models\Player`.
 */
 class PlayerSearch extends Player
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['player_id', 'person_id', 'age_group_id', 'player_team_player_id', 'player_sport_position_id', 'player_sport_position2_id', 'created_by', 'updated_by'], 'integer'],
            [['player_access_code', 'player_waiver_minor_dt', 'player_waiver_adult_dt', 'player_parent_email', 'player_sport_preference', 'player_sport_position_preference', 'player_shot_side_preference', 'player_dominant_side', 'player_dominant_foot', 'player_statistical_highlights', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Player::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'player_id' => $this->player_id,
            'person_id' => $this->person_id,
            'age_group_id' => $this->age_group_id,
            'player_team_player_id' => $this->player_team_player_id,
            'player_sport_position_id' => $this->player_sport_position_id,
            'player_sport_position2_id' => $this->player_sport_position2_id,
            'player_waiver_minor_dt' => $this->player_waiver_minor_dt,
            'player_waiver_adult_dt' => $this->player_waiver_adult_dt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'player_access_code', $this->player_access_code])
            ->andFilterWhere(['like', 'player_parent_email', $this->player_parent_email])
            ->andFilterWhere(['like', 'player_sport_preference', $this->player_sport_preference])
            ->andFilterWhere(['like', 'player_sport_position_preference', $this->player_sport_position_preference])
            ->andFilterWhere(['like', 'player_shot_side_preference', $this->player_shot_side_preference])
            ->andFilterWhere(['like', 'player_dominant_side', $this->player_dominant_side])
            ->andFilterWhere(['like', 'player_dominant_foot', $this->player_dominant_foot])
            ->andFilterWhere(['like', 'player_statistical_highlights', $this->player_statistical_highlights])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
