<?php

namespace app\models;

use Yii;
use \app\models\base\AuthItemChild as BaseAuthItemChild;

/**
 * This is the model class for table "AuthItemChild".
 */
class AuthItemChild extends BaseAuthItemChild
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'string', 'max' => 64],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'parent' => 'Parent',
            'child' => 'Child',
        ];
    }
}
