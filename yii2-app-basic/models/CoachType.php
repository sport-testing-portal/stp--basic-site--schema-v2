<?php

namespace app\models;

use Yii;
use \app\models\base\CoachType as BaseCoachType;

/**
 * This is the model class for table "coach_type".
 */
class CoachType extends BaseCoachType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['coach_type_name'], 'string', 'max' => 75],
            [['coach_type_desc_short'], 'string', 'max' => 45],
            [['coach_type_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['coach_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'coach_type_id' => 'Coach Type ID',
            'coach_type_name' => 'Coach Type Name',
            'display_order' => 'Display Order',
            'coach_type_desc_short' => 'Coach Type Desc Short',
            'coach_type_desc_long' => 'Coach Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
