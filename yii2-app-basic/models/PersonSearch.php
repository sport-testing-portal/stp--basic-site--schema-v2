<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Person;

/**
 * app\models\PersonSearch represents the model behind the search form about `app\models\Person`.
 */
 class PersonSearch extends Person
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['person_id', 'org_id', 'app_user_id', 'person_type_id', 'user_id', 'gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'created_by', 'updated_by'], 'integer'],
            [['person_name_prefix', 'person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_name_full', 'person_phone_personal', 'person_email_personal', 'person_phone_work', 'person_email_work', 'person_position_work', 'person_image_headshot_url', 'person_name_nickname', 'person_date_of_birth', 'person_height', 'person_tshirt_size', 'person_college_commitment_status', 'person_addr_1', 'person_addr_2', 'person_addr_3', 'person_city', 'person_postal_code', 'person_country', 'person_country_code', 'person_state_or_region', 'org_affiliation_begin_dt', 'org_affiliation_end_dt', 'person_website', 'person_profile_url', 'person_profile_uri', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Person::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'person_id' => $this->person_id,
            'org_id' => $this->org_id,
            'app_user_id' => $this->app_user_id,
            'person_type_id' => $this->person_type_id,
            'user_id' => $this->user_id,
            'gender_id' => $this->gender_id,
            'person_date_of_birth' => $this->person_date_of_birth,
            'person_weight' => $this->person_weight,
            'person_high_school__graduation_year' => $this->person_high_school__graduation_year,
            'person_college_graduation_year' => $this->person_college_graduation_year,
            'org_affiliation_begin_dt' => $this->org_affiliation_begin_dt,
            'org_affiliation_end_dt' => $this->org_affiliation_end_dt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'person_name_prefix', $this->person_name_prefix])
            ->andFilterWhere(['like', 'person_name_first', $this->person_name_first])
            ->andFilterWhere(['like', 'person_name_middle', $this->person_name_middle])
            ->andFilterWhere(['like', 'person_name_last', $this->person_name_last])
            ->andFilterWhere(['like', 'person_name_suffix', $this->person_name_suffix])
            ->andFilterWhere(['like', 'person_name_full', $this->person_name_full])
            ->andFilterWhere(['like', 'person_phone_personal', $this->person_phone_personal])
            ->andFilterWhere(['like', 'person_email_personal', $this->person_email_personal])
            ->andFilterWhere(['like', 'person_phone_work', $this->person_phone_work])
            ->andFilterWhere(['like', 'person_email_work', $this->person_email_work])
            ->andFilterWhere(['like', 'person_position_work', $this->person_position_work])
            ->andFilterWhere(['like', 'person_image_headshot_url', $this->person_image_headshot_url])
            ->andFilterWhere(['like', 'person_name_nickname', $this->person_name_nickname])
            ->andFilterWhere(['like', 'person_height', $this->person_height])
            ->andFilterWhere(['like', 'person_tshirt_size', $this->person_tshirt_size])
            ->andFilterWhere(['like', 'person_college_commitment_status', $this->person_college_commitment_status])
            ->andFilterWhere(['like', 'person_addr_1', $this->person_addr_1])
            ->andFilterWhere(['like', 'person_addr_2', $this->person_addr_2])
            ->andFilterWhere(['like', 'person_addr_3', $this->person_addr_3])
            ->andFilterWhere(['like', 'person_city', $this->person_city])
            ->andFilterWhere(['like', 'person_postal_code', $this->person_postal_code])
            ->andFilterWhere(['like', 'person_country', $this->person_country])
            ->andFilterWhere(['like', 'person_country_code', $this->person_country_code])
            ->andFilterWhere(['like', 'person_state_or_region', $this->person_state_or_region])
            ->andFilterWhere(['like', 'person_website', $this->person_website])
            ->andFilterWhere(['like', 'person_profile_url', $this->person_profile_url])
            ->andFilterWhere(['like', 'person_profile_uri', $this->person_profile_uri])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
