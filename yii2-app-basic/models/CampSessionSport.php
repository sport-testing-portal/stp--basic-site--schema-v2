<?php

namespace app\models;

use Yii;
use \app\models\base\CampSessionSport as BaseCampSessionSport;

/**
 * This is the model class for table "camp_session_sport".
 */
class CampSessionSport extends BaseCampSessionSport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['camp_session_id', 'camp_sport_id', 'gender_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'camp_session_sport_id' => 'Camp Session Sport ID',
            'camp_session_id' => 'Camp Session ID',
            'camp_sport_id' => 'Camp Sport ID',
            'gender_id' => 'Gender ID',
            'lock' => 'Lock',
        ];
    }
}
