<?php

namespace app\models;

use Yii;
use \app\models\base\Event as BaseEvent;

/**
 * This is the model class for table "event".
 */
class Event extends BaseEvent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['event_type_id', 'created_by', 'updated_by'], 'integer'],
            [['event_scheduled_dt', 'created_at', 'updated_at'], 'safe'],
            [['event_participation_fee'], 'number'],
            [['event_name'], 'string', 'max' => 150],
            [['event_access_code'], 'string', 'max' => 25],
            [['event_location_name'], 'string', 'max' => 100],
            [['event_location_map_url'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_id' => 'Event ID',
            'event_type_id' => 'Event Type ID',
            'event_name' => 'Event Name',
            'event_access_code' => 'Event Access Code',
            'event_location_name' => 'Event Location Name',
            'event_location_map_url' => 'Event Location Map Url',
            'event_scheduled_dt' => 'Event Scheduled Dt',
            'event_participation_fee' => 'Event Participation Fee',
            'lock' => 'Lock',
        ];
    }
}
