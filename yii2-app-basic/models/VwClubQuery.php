<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwClub]].
 *
 * @see VwClub
 */
class VwClubQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwClub[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwClub|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
