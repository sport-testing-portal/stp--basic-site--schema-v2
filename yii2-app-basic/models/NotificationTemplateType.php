<?php

namespace app\models;

use Yii;
use \app\models\base\NotificationTemplateType as BaseNotificationTemplateType;

/**
 * This is the model class for table "notification_template_type".
 */
class NotificationTemplateType extends BaseNotificationTemplateType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['notification_template_type_name'], 'string', 'max' => 45],
            [['notification_template_type_desc_short'], 'string', 'max' => 75],
            [['notification_template_type_desc_long'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'notification_template_type_id' => 'Notification Template Type ID',
            'notification_template_type_name' => 'Notification Template Type Name',
            'notification_template_type_desc_short' => 'Notification Template Type Desc Short',
            'notification_template_type_desc_long' => 'Notification Template Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
