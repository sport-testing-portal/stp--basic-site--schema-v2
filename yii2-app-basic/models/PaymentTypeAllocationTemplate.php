<?php

namespace app\models;

use Yii;
use \app\models\base\PaymentTypeAllocationTemplate as BasePaymentTypeAllocationTemplate;

/**
 * This is the model class for table "payment_type_allocation_template".
 */
class PaymentTypeAllocationTemplate extends BasePaymentTypeAllocationTemplate
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['payment_type_allocation_template_name'], 'string', 'max' => 100],
            [['payment_type_allocation_template_desc_short'], 'string', 'max' => 45],
            [['payment_type_allocation_template_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_type_allocation_template_id' => 'Payment Type Allocation Template ID',
            'payment_type_allocation_template_name' => 'Payment Type Allocation Template Name',
            'payment_type_allocation_template_desc_short' => 'Payment Type Allocation Template Desc Short',
            'payment_type_allocation_template_desc_long' => 'Payment Type Allocation Template Desc Long',
            'lock' => 'Lock',
        ];
    }
}
