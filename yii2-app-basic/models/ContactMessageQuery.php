<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[ContactMessage]].
 *
 * @see ContactMessage
 */
class ContactMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return ContactMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ContactMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
