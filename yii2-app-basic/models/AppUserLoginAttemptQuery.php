<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppUserLoginAttempt]].
 *
 * @see AppUserLoginAttempt
 */
class AppUserLoginAttemptQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AppUserLoginAttempt[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AppUserLoginAttempt|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
