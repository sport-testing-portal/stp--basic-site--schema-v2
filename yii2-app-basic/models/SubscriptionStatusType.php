<?php

namespace app\models;

use Yii;
use \app\models\base\SubscriptionStatusType as BaseSubscriptionStatusType;

/**
 * This is the model class for table "subscription_status_type".
 */
class SubscriptionStatusType extends BaseSubscriptionStatusType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['subscription_status_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['subscription_status_type_name'], 'string', 'max' => 45],
            [['subscription_status_type_desc_short'], 'string', 'max' => 75],
            [['subscription_status_type_desc_long'], 'string', 'max' => 175],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'subscription_status_type_id' => 'Subscription Status Type ID',
            'subscription_status_type_name' => 'Subscription Status Type Name',
            'subscription_status_type_desc_short' => 'Subscription Status Type Desc Short',
            'subscription_status_type_desc_long' => 'Subscription Status Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
