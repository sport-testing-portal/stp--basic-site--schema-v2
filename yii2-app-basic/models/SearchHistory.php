<?php

namespace app\models;

use Yii;
use \app\models\base\SearchHistory as BaseSearchHistory;

/**
 * This is the model class for table "search_history".
 */
class SearchHistory extends BaseSearchHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['search_criteria_template_id', 'person_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['user_group_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'search_history_id' => 'Search History ID',
            'search_criteria_template_id' => 'Search Criteria Template ID',
            'person_id' => 'Person ID',
            'user_group_name' => 'User Group Name',
            'lock' => 'Lock',
        ];
    }
}
