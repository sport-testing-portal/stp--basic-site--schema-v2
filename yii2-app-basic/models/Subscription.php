<?php

namespace app\models;

use Yii;
use \app\models\base\Subscription as BaseSubscription;

/**
 * This is the model class for table "subscription".
 */
class Subscription extends BaseSubscription
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['subscription_type_id', 'org_id', 'person_id', 'subscription_status_type_id', 'created_by', 'updated_by'], 'integer'],
            [['subscription_begin_dt', 'subscription_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'subscription_id' => 'Subscription ID',
            'subscription_type_id' => 'Subscription Type ID',
            'org_id' => 'Org ID',
            'person_id' => 'Person ID',
            'subscription_status_type_id' => 'Subscription Status Type ID',
            'subscription_begin_dt' => 'Subscription Begin Dt',
            'subscription_end_dt' => 'Subscription End Dt',
            'lock' => 'Lock',
        ];
    }
}
