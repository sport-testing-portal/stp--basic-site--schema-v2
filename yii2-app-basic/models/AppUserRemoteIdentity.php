<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserRemoteIdentity as BaseAppUserRemoteIdentity;

/**
 * This is the model class for table "app_user_remote_identity".
 */
class AppUserRemoteIdentity extends BaseAppUserRemoteIdentity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['app_user_id', 'provider', 'identifier'], 'required'],
            [['app_user_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'last_used_on', 'created_at', 'updated_at'], 'safe'],
            [['provider', 'identifier'], 'string', 'max' => 100],
            [['app_user_id', 'provider', 'identifier'], 'unique', 'targetAttribute' => ['app_user_id', 'provider', 'identifier'], 'message' => 'The combination of App User ID, Provider and Identifier has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_remote_identity_id' => 'App User Remote Identity ID',
            'app_user_id' => 'App User ID',
            'provider' => 'Provider',
            'identifier' => 'Identifier',
            'created_on' => 'Created On',
            'last_used_on' => 'Last Used On',
        ];
    }
}
