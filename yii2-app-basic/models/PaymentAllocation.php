<?php

namespace app\models;

use Yii;
use \app\models\base\PaymentAllocation as BasePaymentAllocation;

/**
 * This is the model class for table "payment_allocation".
 */
class PaymentAllocation extends BasePaymentAllocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['payment_allocation_id'], 'required'],
            [['payment_allocation_id', 'payment_log_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_allocation_id' => 'Payment Allocation ID',
            'payment_log_id' => 'Payment Log ID',
            'lock' => 'Lock',
        ];
    }
}
