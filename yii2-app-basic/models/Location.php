<?php

namespace app\models;

use Yii;
use \app\models\base\Location as BaseLocation;

/**
 * This is the model class for table "location".
 */
class Location extends BaseLocation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['city', 'state_or_region', 'country'], 'string', 'max' => 45],
            [['city', 'state_or_region', 'country'], 'unique', 'targetAttribute' => ['city', 'state_or_region', 'country'], 'message' => 'The combination of City, State Or Region and Country has already been taken.'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'location_id' => 'Location ID',
            'city' => 'City',
            'state_or_region' => 'State Or Region',
            'country' => 'Country',
        ];
    }
}
