<?php

namespace app\models;

use Yii;
use \app\models\base\VwSiteSecurityRoles as BaseVwSiteSecurityRoles;

/**
 * This is the model class for table "vwSiteSecurityRoles".
 */
class VwSiteSecurityRoles extends BaseVwSiteSecurityRoles
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'type'], 'required'],
            [['type'], 'integer'],
            [['description', 'bizrule', 'data'], 'string'],
            [['name'], 'string', 'max' => 64],
            [['type_name'], 'string', 'max' => 13],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'name' => 'Name',
            'type' => 'Type',
            'description' => 'Description',
            'bizrule' => 'Bizrule',
            'data' => 'Data',
            'type_name' => 'Type Name',
        ];
    }
}
