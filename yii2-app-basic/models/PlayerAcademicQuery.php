<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerAcademic]].
 *
 * @see PlayerAcademic
 */
class PlayerAcademicQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerAcademic[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerAcademic|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
