<?php

namespace app\models;

use Yii;
use \app\models\base\SearchCriteriaTemplateItem as BaseSearchCriteriaTemplateItem;

/**
 * This is the model class for table "search_criteria_template_item".
 */
class SearchCriteriaTemplateItem extends BaseSearchCriteriaTemplateItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['search_criteria_template_id', 'search_criteria_id'], 'required'],
            [['search_criteria_template_id', 'search_criteria_id', 'search_criteria_default_value', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'search_criteria_template_item_id' => 'Search Criteria Template Item ID',
            'search_criteria_template_id' => 'Search Criteria Template ID',
            'search_criteria_id' => 'Search Criteria ID',
            'search_criteria_default_value' => 'Search Criteria Default Value',
            'lock' => 'Lock',
        ];
    }
}
