<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwSchool]].
 *
 * @see VwSchool
 */
class VwSchoolQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwSchool[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwSchool|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
