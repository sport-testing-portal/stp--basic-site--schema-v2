<?php

namespace app\models;

use Yii;
use \app\models\base\ImportFileStiSpecialReport as BaseImportFileStiSpecialReport;

/**
 * This is the model class for table "import_file__sti_special_report".
 */
class ImportFileStiSpecialReport extends BaseImportFileStiSpecialReport
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['import_file_id', 'event_id', 'record_id', 'created_by', 'updated_by'], 'integer'],
            [['result'], 'number'],
            [['test_dt', 'import_dt', 'created_at', 'updated_at'], 'safe'],
            [['name_full', 'user_id', 'drill_name'], 'string', 'max' => 75],
            [['attempt'], 'string', 'max' => 4],
            [['splits_str', 'direction'], 'string', 'max' => 45],
            [['drill_unit', 'trial_status'], 'string', 'max' => 25],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'import_file__sti_special_report_id' => 'Import File  Sti Special Report ID',
            'import_file_id' => 'Import File ID',
            'event_id' => 'Event ID',
            'record_id' => 'Record ID',
            'name_full' => 'Name Full',
            'user_id' => 'User ID',
            'drill_name' => 'Drill Name',
            'attempt' => 'Attempt',
            'splits_str' => 'Splits Str',
            'direction' => 'Direction',
            'result' => 'Result',
            'drill_unit' => 'Drill Unit',
            'trial_status' => 'Trial Status',
            'test_dt' => 'Test Dt',
            'import_dt' => 'Import Dt',
        ];
    }
}
