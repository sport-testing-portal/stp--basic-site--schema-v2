<?php

namespace app\models;

use Yii;
use \app\models\base\VwTeamPlayer as BaseVwTeamPlayer;

/**
 * This is the model class for table "vwTeamPlayer".
 */
class VwTeamPlayer extends BaseVwTeamPlayer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'team_league_id', 'team_division_id', 'team_player_id', 'coach_id', 'team_play_sport_position_id', 'team_play_sport_position2_id', 'org_id', 'team_id', 'player_id', 'player__person_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 100],
            [['team_name', 'team_league', 'team_division_name', 'team_city'], 'string', 'max' => 75],
            [['age_group_name', 'team_league_name', 'organizational_level', 'coach_name', 'primary_sport_position', 'secondary_sport_position'], 'string', 'max' => 45],
            [['team_division', 'team_play_begin_dt', 'team_play_end_dt'], 'string', 'max' => 10],
            [['team_state_name'], 'string', 'max' => 32],
            [['team_country_name'], 'string', 'max' => 80],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['player_name'], 'string', 'max' => 90],
            [['team_created_by_username', 'team_updated_by_username'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'org_name' => 'Org Name',
            'team_name' => 'Team Name',
            'age_group_name' => 'Age Group Name',
            'team_division' => 'Team Division',
            'team_league' => 'Team League',
            'team_league_id' => 'Team League ID',
            'team_league_name' => 'Team League Name',
            'team_division_id' => 'Team Division ID',
            'team_division_name' => 'Team Division Name',
            'organizational_level' => 'Organizational Level',
            'team_city' => 'Team City',
            'team_state_name' => 'Team State Name',
            'team_country_name' => 'Team Country Name',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'player_name' => 'Player Name',
            'team_player_id' => 'Team Player ID',
            'coach_name' => 'Coach Name',
            'coach_id' => 'Coach ID',
            'team_play_begin_dt' => 'Team Play Begin Dt',
            'team_play_end_dt' => 'Team Play End Dt',
            'team_play_sport_position_id' => 'Team Play Sport Position ID',
            'team_play_sport_position2_id' => 'Team Play Sport Position2 ID',
            'primary_sport_position' => 'Primary Sport Position',
            'secondary_sport_position' => 'Secondary Sport Position',
            'org_id' => 'Org ID',
            'team_id' => 'Team ID',
            'player_id' => 'Player ID',
            'player__person_id' => 'Player  Person ID',
            'team_created_by_username' => 'Team Created By Username',
            'team_updated_by_username' => 'Team Updated By Username',
        ];
    }
}
