<?php

namespace app\models;

use Yii;
use \app\models\base\PersonType as BasePersonType;

/**
 * This is the model class for table "person_type".
 */
class PersonType extends BasePersonType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['person_type_name', 'person_type_desc_short', 'person_type_desc_long'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['person_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_type_id' => 'Person Type ID',
            'person_type_name' => 'Person Type Name',
            'person_type_desc_short' => 'Person Type Desc Short',
            'person_type_desc_long' => 'Person Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
