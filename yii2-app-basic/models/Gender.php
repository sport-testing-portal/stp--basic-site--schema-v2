<?php

namespace app\models;

use Yii;
use \app\models\base\Gender as BaseGender;

/**
 * This is the model class for table "gender".
 */
class Gender extends BaseGender
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['gender_desc'], 'string', 'max' => 30],
            [['gender_code'], 'string', 'max' => 5],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'gender_id' => 'Gender ID',
            'gender_desc' => 'Gender Desc',
            'gender_code' => 'Gender Code',
            'lock' => 'Lock',
        ];
    }
}
