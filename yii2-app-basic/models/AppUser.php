<?php

namespace app\models;

use Yii;
use \app\models\base\AppUser as BaseAppUser;

/**
 * This is the model class for table "app_user".
 */
class AppUser extends BaseAppUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['username'], 'required'],
            [['created_on', 'updated_on', 'last_visit_on', 'password_set_on', 'created_at', 'updated_at'], 'safe'],
            [['one_time_password_counter', 'created_by', 'updated_by'], 'integer'],
            [['username', 'password', 'email', 'one_time_password_secret', 'one_time_password_code'], 'string', 'max' => 255],
            [['firstname'], 'string', 'max' => 65],
            [['lastname', 'activation_key'], 'string', 'max' => 45],
            [['email_verified', 'is_active', 'is_disabled'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_id' => 'App User ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'activation_key' => 'Activation Key',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'last_visit_on' => 'Last Visit On',
            'password_set_on' => 'Password Set On',
            'email_verified' => 'Email Verified',
            'is_active' => 'Is Active',
            'is_disabled' => 'Is Disabled',
            'one_time_password_secret' => 'One Time Password Secret',
            'one_time_password_code' => 'One Time Password Code',
            'one_time_password_counter' => 'One Time Password Counter',
        ];
    }
}
