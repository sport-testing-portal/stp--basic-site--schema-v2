<?php

namespace app\models;

use Yii;
use \app\models\base\VwSiteTaskGroupMembers as BaseVwSiteTaskGroupMembers;

/**
 * This is the model class for table "vwSiteTaskGroupMembers".
 */
class VwSiteTaskGroupMembers extends BaseVwSiteTaskGroupMembers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['site_task_group', 'site_operation'], 'required'],
            [['site_task_group', 'site_operation'], 'string', 'max' => 64],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'site_task_group' => 'Site Task Group',
            'site_operation' => 'Site Operation',
        ];
    }
}
