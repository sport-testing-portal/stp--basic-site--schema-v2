<?php

namespace app\models;

use Yii;
use \app\models\base\PersonCertificationType as BasePersonCertificationType;

/**
 * This is the model class for table "person_certification_type".
 */
class PersonCertificationType extends BasePersonCertificationType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_type_id', 'person_certification_type_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['person_certification_type_name', 'person_certification_subtype_name', 'person_certification_type_description_short'], 'string', 'max' => 45],
            [['person_certification_type_description_long'], 'string', 'max' => 253],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_certification_type_id' => 'Person Certification Type ID',
            'person_type_id' => 'Person Type ID',
            'person_certification_type_name' => 'Person Certification Type Name',
            'person_certification_subtype_name' => 'Person Certification Subtype Name',
            'person_certification_type_display_order' => 'Person Certification Type Display Order',
            'person_certification_type_description_short' => 'Person Certification Type Description Short',
            'person_certification_type_description_long' => 'Person Certification Type Description Long',
            'lock' => 'Lock',
        ];
    }
}
