<?php

namespace app\models;

use Yii;
use \app\models\base\Season as BaseSeason;

/**
 * This is the model class for table "season".
 */
class Season extends BaseSeason
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['season'], 'string', 'max' => 45],
            [['season_comment'], 'string', 'max' => 200],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'season_id' => 'Season ID',
            'season' => 'Season',
            'season_comment' => 'Season Comment',
            'lock' => 'Lock',
        ];
    }
}
