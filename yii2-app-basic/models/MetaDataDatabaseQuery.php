<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MetaDataDatabase]].
 *
 * @see MetaDataDatabase
 */
class MetaDataDatabaseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return MetaDataDatabase[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return MetaDataDatabase|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
