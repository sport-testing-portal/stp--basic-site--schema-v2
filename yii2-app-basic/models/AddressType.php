<?php

namespace app\models;

use Yii;
use \app\models\base\AddressType as BaseAddressType;

/**
 * This is the model class for table "address_type".
 */
class AddressType extends BaseAddressType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['address_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['address_type_name', 'address_type_description_short'], 'string', 'max' => 45],
            [['address_type_description_long'], 'string', 'max' => 150],
            [['address_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'address_type_id' => 'Address Type ID',
            'address_type_name' => 'Address Type Name',
            'address_type_description_short' => 'Address Type Description Short',
            'address_type_description_long' => 'Address Type Description Long',
        ];
    }
}
