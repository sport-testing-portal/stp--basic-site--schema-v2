<?php

namespace app\models;

use Yii;
use \app\models\base\ContactMessage as BaseContactMessage;

/**
 * This is the model class for table "contact_message".
 */
class ContactMessage extends BaseContactMessage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['contact_message_type_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['contact_message_from', 'contact_message_to', 'contact_message_subject', 'contact_message_body'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'contact_message_id' => 'Contact Message ID',
            'contact_message_type_id' => 'Contact Message Type ID',
            'contact_message_from' => 'Contact Message From',
            'contact_message_to' => 'Contact Message To',
            'contact_message_subject' => 'Contact Message Subject',
            'contact_message_body' => 'Contact Message Body',
            'lock' => 'Lock',
        ];
    }
}
