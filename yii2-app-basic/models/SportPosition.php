<?php

namespace app\models;

use Yii;
use \app\models\base\SportPosition as BaseSportPosition;

/**
 * This is the model class for table "sport_position".
 */
class SportPosition extends BaseSportPosition
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sport_id', 'display_order', 'created_by', 'updated_by'], 'integer'],
            [['sport_position_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['sport_position_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'sport_position_id' => 'Sport Position ID',
            'sport_id' => 'Sport ID',
            'sport_position_name' => 'Sport Position Name',
            'display_order' => 'Display Order',
            'lock' => 'Lock',
        ];
    }
}
