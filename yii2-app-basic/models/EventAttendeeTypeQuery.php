<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[EventAttendeeType]].
 *
 * @see EventAttendeeType
 */
class EventAttendeeTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return EventAttendeeType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return EventAttendeeType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
