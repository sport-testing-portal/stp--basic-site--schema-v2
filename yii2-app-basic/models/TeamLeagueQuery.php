<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TeamLeague]].
 *
 * @see TeamLeague
 */
class TeamLeagueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TeamLeague[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TeamLeague|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
