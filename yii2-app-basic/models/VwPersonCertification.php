<?php

namespace app\models;

use Yii;
use \app\models\base\VwPersonCertification as BaseVwPersonCertification;

/**
 * This is the model class for table "vwPersonCertification".
 */
class VwPersonCertification extends BaseVwPersonCertification
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_certification_id', 'person_id', 'person_certification_type_id', 'person_certification_year', 'person_type_id', 'person_certification_type_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['vwPersonCertification_id'], 'string', 'max' => 23],
            [['person'], 'string', 'max' => 90],
            [['person_type', 'person_certification_type_name', 'person_certification_subtype_name', 'person_certification_type_description_short'], 'string', 'max' => 45],
            [['person_certification_type_description_long'], 'string', 'max' => 253],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'vwPersonCertification_id' => 'Vw Person Certification ID',
            'person' => 'Person',
            'person_type' => 'Person Type',
            'person_certification_id' => 'Person Certification ID',
            'person_id' => 'Person ID',
            'person_certification_type_id' => 'Person Certification Type ID',
            'person_certification_year' => 'Person Certification Year',
            'person_type_id' => 'Person Type ID',
            'person_certification_type_name' => 'Person Certification Type Name',
            'person_certification_subtype_name' => 'Person Certification Subtype Name',
            'person_certification_type_display_order' => 'Person Certification Type Display Order',
            'person_certification_type_description_short' => 'Person Certification Type Description Short',
            'person_certification_type_description_long' => 'Person Certification Type Description Long',
        ];
    }
}
