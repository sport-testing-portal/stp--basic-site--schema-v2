<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalSummaryLog as BaseTestEvalSummaryLog;

/**
 * This is the model class for table "test_eval_summary_log".
 */
class TestEvalSummaryLog extends BaseTestEvalSummaryLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['person_id', 'team_id', 'test_eval_type_id', 'source_event_id', 'created_by', 'updated_by'], 'integer'],
            [['test_eval_overall_ranking', 'test_eval_positional_ranking', 'test_eval_total_score', 'test_eval_avg_score'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
            'person_id' => 'Person ID',
            'team_id' => 'Team ID',
            'test_eval_type_id' => 'Test Eval Type ID',
            'source_event_id' => 'Source Event ID',
            'test_eval_overall_ranking' => 'Test Eval Overall Ranking',
            'test_eval_positional_ranking' => 'Test Eval Positional Ranking',
            'test_eval_total_score' => 'Test Eval Total Score',
            'test_eval_avg_score' => 'Test Eval Avg Score',
            'lock' => 'Lock',
        ];
    }
}
