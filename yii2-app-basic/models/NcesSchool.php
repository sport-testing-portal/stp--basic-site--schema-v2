<?php

namespace app\models;

use Yii;
use \app\models\base\NcesSchool as BaseNcesSchool;

/**
 * This is the model class for table "nces_school".
 */
class NcesSchool extends BaseNcesSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['nces_school_unit_id', 'nces_school_ope_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nces_school_name', 'nces_school_website', 'nces_school_addr1'], 'string', 'max' => 125],
            [['nces_school_addr2', 'nces_school_state'], 'string', 'max' => 45],
            [['nces_school_city', 'nces_school_admin_title'], 'string', 'max' => 65],
            [['nces_school_zipcode'], 'string', 'max' => 15],
            [['nces_school_phone_general'], 'string', 'max' => 25],
            [['nces_school_admin'], 'string', 'max' => 75],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'nces_school_id' => 'Nces School ID',
            'nces_school_name' => 'Nces School Name',
            'nces_school_website' => 'Nces School Website',
            'nces_school_unit_id' => 'Nces School Unit ID',
            'nces_school_addr1' => 'Nces School Addr1',
            'nces_school_addr2' => 'Nces School Addr2',
            'nces_school_ope_id' => 'Nces School Ope ID',
            'nces_school_city' => 'Nces School City',
            'nces_school_state' => 'Nces School State',
            'nces_school_zipcode' => 'Nces School Zipcode',
            'nces_school_phone_general' => 'Nces School Phone General',
            'nces_school_admin' => 'Nces School Admin',
            'nces_school_admin_title' => 'Nces School Admin Title',
        ];
    }
}
