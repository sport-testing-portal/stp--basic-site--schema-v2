<?php

namespace app\models;

use Yii;
use \app\models\base\Conference as BaseConference;

/**
 * This is the model class for table "conference".
 */
class Conference extends BaseConference
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['conference_name'], 'string', 'max' => 75],
            [['ncaa_division'], 'string', 'max' => 5],
            [['lock'], 'string', 'max' => 1],
            [['conference_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'conference_id' => 'Conference ID',
            'conference_name' => 'Conference Name',
            'ncaa_division' => 'Ncaa Division',
            'lock' => 'Lock',
        ];
    }
}
