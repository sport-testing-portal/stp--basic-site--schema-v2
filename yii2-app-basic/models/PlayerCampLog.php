<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerCampLog as BasePlayerCampLog;

/**
 * This is the model class for table "player_camp_log".
 */
class PlayerCampLog extends BasePlayerCampLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_id', 'camp_id', 'camp_session_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_camp_log_id' => 'Player Camp Log ID',
            'player_id' => 'Player ID',
            'camp_id' => 'Camp ID',
            'camp_session_id' => 'Camp Session ID',
            'lock' => 'Lock',
        ];
    }
}
