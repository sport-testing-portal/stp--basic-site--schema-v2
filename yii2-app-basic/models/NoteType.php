<?php

namespace app\models;

use Yii;
use \app\models\base\NoteType as BaseNoteType;

/**
 * This is the model class for table "note_type".
 */
class NoteType extends BaseNoteType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['note_type_name'], 'required'],
            [['display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['note_type_name', 'note_type_desc_short'], 'string', 'max' => 45],
            [['note_type_desc_long'], 'string', 'max' => 255],
            [['note_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'note_type_id' => 'Note Type ID',
            'note_type_name' => 'Note Type Name',
            'display_order' => 'Display Order',
            'note_type_desc_short' => 'Note Type Desc Short',
            'note_type_desc_long' => 'Note Type Desc Long',
        ];
    }
}
