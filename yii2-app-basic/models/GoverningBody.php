<?php

namespace app\models;

use Yii;
use \app\models\base\GoverningBody as BaseGoverningBody;

/**
 * This is the model class for table "governing_body".
 */
class GoverningBody extends BaseGoverningBody
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sport_id', 'created_by', 'updated_by'], 'integer'],
            [['governing_body_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['governing_body_name', 'governing_body_short_desc'], 'string', 'max' => 45],
            [['governing_body_long_desc'], 'string', 'max' => 300],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'governing_body_id' => 'Governing Body ID',
            'sport_id' => 'Sport ID',
            'governing_body_name' => 'Governing Body Name',
            'governing_body_short_desc' => 'Governing Body Short Desc',
            'governing_body_long_desc' => 'Governing Body Long Desc',
            'lock' => 'Lock',
        ];
    }
}
