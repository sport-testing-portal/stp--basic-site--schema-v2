<?php

namespace app\models;

use Yii;
use \app\models\base\OrgType as BaseOrgType;

/**
 * This is the model class for table "org_type".
 */
class OrgType extends BaseOrgType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['org_type_name', 'org_type_sub_type', 'org_type_desc_short', 'org_type_desc_long'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['org_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_type_id' => 'Org Type ID',
            'org_type_name' => 'Org Type Name',
            'org_type_sub_type' => 'Org Type Sub Type',
            'org_type_desc_short' => 'Org Type Desc Short',
            'org_type_desc_long' => 'Org Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
