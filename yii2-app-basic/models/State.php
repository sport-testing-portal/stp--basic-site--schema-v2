<?php

namespace app\models;

use Yii;
use \app\models\base\State as BaseState;

/**
 * This is the model class for table "state".
 */
class State extends BaseState
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['state_name'], 'required'],
            [['state_name'], 'string', 'max' => 32],
            [['state_code'], 'string', 'max' => 8],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'state_id' => 'State ID',
            'state_name' => 'State Name',
            'state_code' => 'State Code',
        ];
    }
}
