<?php

namespace app\models;

use Yii;
use \app\models\base\SchoolSat as BaseSchoolSat;

/**
 * This is the model class for table "school_sat".
 */
class SchoolSat extends BaseSchoolSat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'school_unit_id', 'school_sat_i_verbal_25_pct', 'school_sat_i_verbal_75_pct', 'school_sat_i_math_25_pct', 'school_sat_i_math_75_pct', 'school_sat_student_submit_cnt', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['school_sat_student_submit_pct'], 'string', 'max' => 5],
            [['school_sat_report_period'], 'string', 'max' => 12],
            [['lock'], 'string', 'max' => 1],
            [['school_unit_id'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'school_sat_id' => 'School Sat ID',
            'school_id' => 'School ID',
            'school_unit_id' => 'School Unit ID',
            'school_sat_i_verbal_25_pct' => 'School Sat I Verbal 25 Pct',
            'school_sat_i_verbal_75_pct' => 'School Sat I Verbal 75 Pct',
            'school_sat_i_math_25_pct' => 'School Sat I Math 25 Pct',
            'school_sat_i_math_75_pct' => 'School Sat I Math 75 Pct',
            'school_sat_student_submit_cnt' => 'School Sat Student Submit Cnt',
            'school_sat_student_submit_pct' => 'School Sat Student Submit Pct',
            'school_sat_report_period' => 'School Sat Report Period',
            'lock' => 'Lock',
        ];
    }
}
