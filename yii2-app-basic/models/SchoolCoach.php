<?php

namespace app\models;

use Yii;
use \app\models\base\SchoolCoach as BaseSchoolCoach;

/**
 * This is the model class for table "school_coach".
 */
class SchoolCoach extends BaseSchoolCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'coach_id'], 'required'],
            [['school_id', 'coach_id', 'updated_by', 'created_by'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'school_coach_id' => 'School Coach ID',
            'school_id' => 'School ID',
            'coach_id' => 'Coach ID',
            'lock' => 'Lock',
        ];
    }
}
