<?php

namespace app\models;

use Yii;
use \app\models\base\VwTeamStrict as BaseVwTeamStrict;

/**
 * This is the model class for table "vwTeamStrict".
 */
class VwTeamStrict extends BaseVwTeamStrict
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'team_id', 'coach_id', 'player_id', 'coach__person_id', 'player__person_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 100],
            [['team_name'], 'string', 'max' => 75],
            [['team_age_group'], 'string', 'max' => 45],
            [['team_division'], 'string', 'max' => 10],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['coach_name', 'player_name'], 'string', 'max' => 90],
            [['team_created_by_username', 'team_updated_by_username'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_name' => 'Org Name',
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'org_id' => 'Org ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'coach__person_id' => 'Coach  Person ID',
            'player__person_id' => 'Player  Person ID',
            'team_created_by_username' => 'Team Created By Username',
            'team_updated_by_username' => 'Team Updated By Username',
        ];
    }
}
