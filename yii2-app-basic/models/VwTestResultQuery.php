<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTestResult]].
 *
 * @see VwTestResult
 */
class VwTestResultQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTestResult[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTestResult|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
