<?php

namespace app\models;

use Yii;
use \app\models\base\Org as BaseOrg;

/**
 * This is the model class for table "org".
 */
class Org extends BaseOrg
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_type_id', 'org_level_id', 'created_by', 'updated_by'], 'integer'],
            [['org_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['org_name', 'org_addr1', 'org_addr2', 'org_addr3'], 'string', 'max' => 100],
            [['org_governing_body', 'org_state_or_region'], 'string', 'max' => 45],
            [['org_ncaa_clearing_house_id', 'org_phone_main', 'org_postal_code'], 'string', 'max' => 25],
            [['org_website_url', 'org_twitter_url', 'org_facebook_url'], 'string', 'max' => 150],
            [['org_email_main', 'org_city'], 'string', 'max' => 75],
            [['org_country_code_iso3'], 'string', 'max' => 3],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'org_level_id' => 'Org Level ID',
            'org_name' => 'Org Name',
            'org_governing_body' => 'Org Governing Body',
            'org_ncaa_clearing_house_id' => 'Org Ncaa Clearing House ID',
            'org_website_url' => 'Org Website Url',
            'org_twitter_url' => 'Org Twitter Url',
            'org_facebook_url' => 'Org Facebook Url',
            'org_phone_main' => 'Org Phone Main',
            'org_email_main' => 'Org Email Main',
            'org_addr1' => 'Org Addr1',
            'org_addr2' => 'Org Addr2',
            'org_addr3' => 'Org Addr3',
            'org_city' => 'Org City',
            'org_state_or_region' => 'Org State Or Region',
            'org_postal_code' => 'Org Postal Code',
            'org_country_code_iso3' => 'Org Country Code Iso3',
            'lock' => 'Lock',
        ];
    }
}
