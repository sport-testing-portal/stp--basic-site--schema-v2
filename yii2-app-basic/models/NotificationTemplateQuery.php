<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NotificationTemplate]].
 *
 * @see NotificationTemplate
 */
class NotificationTemplateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NotificationTemplate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NotificationTemplate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
