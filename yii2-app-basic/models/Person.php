<?php

namespace app\models;

use Yii;
use \app\models\base\Person as BasePerson;

/**
 * This is the model class for table "person".
 */
class Person extends BasePerson
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'app_user_id', 'person_type_id', 'user_id', 'gender_id', 'person_weight', 'person_high_school__graduation_year', 'person_college_graduation_year', 'created_by', 'updated_by'], 'integer'],
            [['person_date_of_birth', 'org_affiliation_begin_dt', 'org_affiliation_end_dt', 'created_at', 'updated_at'], 'safe'],
            [['person_name_prefix', 'person_college_commitment_status', 'person_postal_code'], 'string', 'max' => 25],
            [['person_name_first', 'person_name_middle', 'person_name_last', 'person_name_suffix', 'person_phone_personal', 'person_phone_work', 'person_position_work', 'person_name_nickname', 'person_country', 'person_state_or_region'], 'string', 'max' => 45],
            [['person_name_full', 'person_profile_url'], 'string', 'max' => 90],
            [['person_email_personal', 'person_email_work', 'person_profile_uri'], 'string', 'max' => 60],
            [['person_image_headshot_url', 'person_addr_1', 'person_addr_2', 'person_addr_3'], 'string', 'max' => 100],
            [['person_height'], 'string', 'max' => 5],
            [['person_tshirt_size'], 'string', 'max' => 10],
            [['person_city'], 'string', 'max' => 75],
            [['person_country_code'], 'string', 'max' => 3],
            [['person_website'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['user_id'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'person_id' => 'Person ID',
            'org_id' => 'Org ID',
            'app_user_id' => 'App User ID',
            'person_type_id' => 'Person Type ID',
            'user_id' => 'User ID',
            'person_name_prefix' => 'Person Name Prefix',
            'person_name_first' => 'Person Name First',
            'person_name_middle' => 'Person Name Middle',
            'person_name_last' => 'Person Name Last',
            'person_name_suffix' => 'Person Name Suffix',
            'person_name_full' => 'Person Name Full',
            'person_phone_personal' => 'Person Phone Personal',
            'person_email_personal' => 'Person Email Personal',
            'person_phone_work' => 'Person Phone Work',
            'person_email_work' => 'Person Email Work',
            'person_position_work' => 'Person Position Work',
            'gender_id' => 'Gender ID',
            'person_image_headshot_url' => 'Person Image Headshot Url',
            'person_name_nickname' => 'Person Name Nickname',
            'person_date_of_birth' => 'Person Date Of Birth',
            'person_height' => 'Person Height',
            'person_weight' => 'Person Weight',
            'person_tshirt_size' => 'Person Tshirt Size',
            'person_high_school__graduation_year' => 'Person High School  Graduation Year',
            'person_college_graduation_year' => 'Person College Graduation Year',
            'person_college_commitment_status' => 'Person College Commitment Status',
            'person_addr_1' => 'Person Addr 1',
            'person_addr_2' => 'Person Addr 2',
            'person_addr_3' => 'Person Addr 3',
            'person_city' => 'Person City',
            'person_postal_code' => 'Person Postal Code',
            'person_country' => 'Person Country',
            'person_country_code' => 'Person Country Code',
            'person_state_or_region' => 'Person State Or Region',
            'org_affiliation_begin_dt' => 'Org Affiliation Begin Dt',
            'org_affiliation_end_dt' => 'Org Affiliation End Dt',
            'person_website' => 'Person Website',
            'person_profile_url' => 'Person Profile Url',
            'person_profile_uri' => 'Person Profile Uri',
            'lock' => 'Lock',
        ];
    }
}
