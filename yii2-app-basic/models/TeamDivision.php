<?php

namespace app\models;

use Yii;
use \app\models\base\TeamDivision as BaseTeamDivision;

/**
 * This is the model class for table "team_division".
 */
class TeamDivision extends BaseTeamDivision
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['team_division_name'], 'required'],
            [['team_division_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['team_division_name', 'team_division_desc_short'], 'string', 'max' => 75],
            [['team_division_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'team_division_id' => 'Team Division ID',
            'team_division_name' => 'Team Division Name',
            'team_division_desc_short' => 'Team Division Desc Short',
            'team_division_desc_long' => 'Team Division Desc Long',
            'team_division_display_order' => 'Team Division Display Order',
            'lock' => 'Lock',
        ];
    }
}
