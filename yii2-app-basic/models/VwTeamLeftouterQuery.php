<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTeamLeftouter]].
 *
 * @see VwTeamLeftouter
 */
class VwTeamLeftouterQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTeamLeftouter[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTeamLeftouter|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
