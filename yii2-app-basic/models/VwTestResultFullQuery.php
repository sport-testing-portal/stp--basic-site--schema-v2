<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTestResultFull]].
 *
 * @see VwTestResultFull
 */
class VwTestResultFullQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwTestResultFull[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwTestResultFull|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
