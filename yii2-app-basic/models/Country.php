<?php

namespace app\models;

use Yii;
use \app\models\base\Country as BaseCountry;

/**
 * This is the model class for table "country".
 */
class Country extends BaseCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['iso2'], 'string', 'max' => 2],
            [['short_name', 'long_name'], 'string', 'max' => 80],
            [['iso3'], 'string', 'max' => 3],
            [['numcode'], 'string', 'max' => 6],
            [['un_member'], 'string', 'max' => 12],
            [['calling_code'], 'string', 'max' => 8],
            [['cctld'], 'string', 'max' => 5],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'country_id' => 'Country ID',
            'iso2' => 'Iso2',
            'short_name' => 'Short Name',
            'long_name' => 'Long Name',
            'iso3' => 'Iso3',
            'numcode' => 'Numcode',
            'un_member' => 'Un Member',
            'calling_code' => 'Calling Code',
            'cctld' => 'Cctld',
        ];
    }
}
