<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SearchCriteriaTemplateItem]].
 *
 * @see SearchCriteriaTemplateItem
 */
class SearchCriteriaTemplateItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SearchCriteriaTemplateItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SearchCriteriaTemplateItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
