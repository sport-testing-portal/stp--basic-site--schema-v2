<?php

namespace app\models;

use Yii;
use \app\models\base\TeamLeague as BaseTeamLeague;

/**
 * This is the model class for table "team_league".
 */
class TeamLeague extends BaseTeamLeague
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['team_league_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['team_league_name'], 'string', 'max' => 45],
            [['team_league_desc_short'], 'string', 'max' => 75],
            [['team_league_desc_long'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'team_league_id' => 'Team League ID',
            'team_league_name' => 'Team League Name',
            'team_league_desc_short' => 'Team League Desc Short',
            'team_league_desc_long' => 'Team League Desc Long',
            'lock' => 'Lock',
        ];
    }
}
