<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SportGender]].
 *
 * @see SportGender
 */
class SportGenderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SportGender[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SportGender|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
