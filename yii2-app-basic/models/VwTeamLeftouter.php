<?php

namespace app\models;

use Yii;
use \app\models\base\VwTeamLeftouter as BaseVwTeamLeftouter;

/**
 * This is the model class for table "vwTeam_leftouter".
 */
class VwTeamLeftouter extends BaseVwTeamLeftouter
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'org_type_id', 'team_id', 'coach_id', 'coach__person_id', 'coach__created_by', 'player_id', 'player__created_by', 'team_player_id', 'team_player__created_by', 'team_coach_id', 'team_coach__created_by', 'player__person_id', 'player__person_org_id', 'team__created_by', 'team__updated_by'], 'integer'],
            [['created_at', 'updated_at', 'team__created_at', 'team__updated_at'], 'safe'],
            [['org_name'], 'string', 'max' => 100],
            [['org_type_name', 'team_age_group', 'organizational_level', 'team_governing_body', 'sport_name'], 'string', 'max' => 45],
            [['team_name', 'team_schedule_url'], 'string', 'max' => 75],
            [['team_division'], 'string', 'max' => 10],
            [['team_website_url', 'coach_name', 'player_name', 'coach__person_name_full'], 'string', 'max' => 90],
            [['team_schedule_uri', 'coach__person_email_work'], 'string', 'max' => 60],
            [['team_statistical_highlights'], 'string', 'max' => 300],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['team_coach_player_composite_key'], 'string', 'max' => 35],
            [['team_created_by_username', 'team_updated_by_username', 'team_updated_by_user_email'], 'string', 'max' => 255],
            [['team_updated_by_user_flname'], 'string', 'max' => 131],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'org_name' => 'Org Name',
            'org_type_name' => 'Org Type Name',
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'organizational_level' => 'Organizational Level',
            'team_governing_body' => 'Team Governing Body',
            'team_website_url' => 'Team Website Url',
            'team_schedule_url' => 'Team Schedule Url',
            'team_schedule_uri' => 'Team Schedule Uri',
            'team_statistical_highlights' => 'Team Statistical Highlights',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'sport_name' => 'Sport Name',
            'org_id' => 'Org ID',
            'org_type_id' => 'Org Type ID',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'coach__person_name_full' => 'Coach  Person Name Full',
            'coach__person_email_work' => 'Coach  Person Email Work',
            'coach__person_id' => 'Coach  Person ID',
            'coach__created_by' => 'Coach  Created By',
            'player_id' => 'Player ID',
            'player__created_by' => 'Player  Created By',
            'team_player_id' => 'Team Player ID',
            'team_player__created_by' => 'Team Player  Created By',
            'team_coach_id' => 'Team Coach ID',
            'team_coach__created_by' => 'Team Coach  Created By',
            'player__person_id' => 'Player  Person ID',
            'player__person_org_id' => 'Player  Person Org ID',
            'team_coach_player_composite_key' => 'Team Coach Player Composite Key',
            'team_created_by_username' => 'Team Created By Username',
            'team_updated_by_username' => 'Team Updated By Username',
            'team_updated_by_user_flname' => 'Team Updated By User Flname',
            'team_updated_by_user_email' => 'Team Updated By User Email',
            'team__created_by' => 'Team  Created By',
            'team__updated_by' => 'Team  Updated By',
            'team__created_at' => 'Team  Created At',
            'team__updated_at' => 'Team  Updated At',
        ];
    }
}
