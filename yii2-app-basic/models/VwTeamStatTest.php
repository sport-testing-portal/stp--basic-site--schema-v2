<?php

namespace app\models;

use Yii;
use \app\models\base\VwTeamStatTest as BaseVwTeamStatTest;

/**
 * This is the model class for table "vwTeamStatTest".
 */
class VwTeamStatTest extends BaseVwTeamStatTest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['team_id', 'coach_id', 'player_id', 'coach__person_id', 'player__person_id', 'test_eval_summary_log_id'], 'integer'],
            [['team_name'], 'string', 'max' => 75],
            [['team_age_group'], 'string', 'max' => 45],
            [['team_division'], 'string', 'max' => 10],
            [['gender_code'], 'string', 'max' => 5],
            [['gender_desc'], 'string', 'max' => 30],
            [['coach_name', 'player_name'], 'string', 'max' => 90],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'team_name' => 'Team Name',
            'team_age_group' => 'Team Age Group',
            'team_division' => 'Team Division',
            'gender_code' => 'Gender Code',
            'gender_desc' => 'Gender Desc',
            'coach_name' => 'Coach Name',
            'player_name' => 'Player Name',
            'team_id' => 'Team ID',
            'coach_id' => 'Coach ID',
            'player_id' => 'Player ID',
            'coach__person_id' => 'Coach  Person ID',
            'player__person_id' => 'Player  Person ID',
            'test_eval_summary_log_id' => 'Test Eval Summary Log ID',
        ];
    }
}
