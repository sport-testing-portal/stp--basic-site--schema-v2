<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeamCoach;

/**
 * app\models\TeamCoachSearch represents the model behind the search form about `app\models\TeamCoach`.
 */
 class TeamCoachSearch extends TeamCoach
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_coach_id', 'team_id', 'coach_id', 'team_coach_coach_type_id', 'created_by', 'updated_by'], 'integer'],
            [['primary_position', 'team_coach_begin_dt', 'team_coach_end_dt', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamCoach::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'team_coach_id' => $this->team_coach_id,
            'team_id' => $this->team_id,
            'coach_id' => $this->coach_id,
            'team_coach_coach_type_id' => $this->team_coach_coach_type_id,
            'team_coach_begin_dt' => $this->team_coach_begin_dt,
            'team_coach_end_dt' => $this->team_coach_end_dt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'primary_position', $this->primary_position])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
