<?php

namespace app\models;

use Yii;
use \app\models\base\EventType as BaseEventType;

/**
 * This is the model class for table "event_type".
 */
class EventType extends BaseEventType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['event_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['event_type_name'], 'string', 'max' => 75],
            [['event_type_desc_short', 'event_type_tags'], 'string', 'max' => 50],
            [['event_type_desc_long'], 'string', 'max' => 250],
            [['lock'], 'string', 'max' => 1],
            [['event_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'event_type_id' => 'Event Type ID',
            'event_type_name' => 'Event Type Name',
            'event_type_desc_short' => 'Event Type Desc Short',
            'event_type_desc_long' => 'Event Type Desc Long',
            'event_type_tags' => 'Event Type Tags',
            'lock' => 'Lock',
        ];
    }
}
