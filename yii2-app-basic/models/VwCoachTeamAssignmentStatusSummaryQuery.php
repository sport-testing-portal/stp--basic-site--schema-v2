<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwCoachTeamAssignmentStatusSummary]].
 *
 * @see VwCoachTeamAssignmentStatusSummary
 */
class VwCoachTeamAssignmentStatusSummaryQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwCoachTeamAssignmentStatusSummary[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwCoachTeamAssignmentStatusSummary|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
