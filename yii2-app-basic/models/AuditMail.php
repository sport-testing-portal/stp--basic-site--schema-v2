<?php

namespace app\models;

use Yii;
use \app\models\base\AuditMail as BaseAuditMail;

/**
 * This is the model class for table "audit_mail".
 */
class AuditMail extends BaseAuditMail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['entry_id', 'created', 'successful'], 'required'],
            [['entry_id', 'successful'], 'integer'],
            [['created'], 'safe'],
            [['text', 'html', 'data'], 'string'],
            [['from', 'to', 'reply', 'cc', 'bcc', 'subject'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'entry_id' => 'Entry ID',
            'successful' => 'Successful',
            'from' => 'From',
            'to' => 'To',
            'reply' => 'Reply',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'subject' => 'Subject',
            'text' => 'Text',
            'html' => 'Html',
            'data' => 'Data',
        ];
    }
}
