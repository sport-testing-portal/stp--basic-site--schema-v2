<?php

namespace app\models;

use Yii;
use \app\models\base\VwDataHdrXlation as BaseVwDataHdrXlation;

/**
 * This is the model class for table "vwDataHdrXlation".
 */
class VwDataHdrXlation extends BaseVwDataHdrXlation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['dhx_id', 'dhxi_id', 'col_ord', 'type_name'], 'integer'],
            [['source_entity', 'source_value', 'target_value'], 'string', 'max' => 75],
            [['target_table', 'type_name_source'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'dhx_id' => 'Dhx ID',
            'dhxi_id' => 'Dhxi ID',
            'source_entity' => 'Source Entity',
            'source_value' => 'Source Value',
            'target_value' => 'Target Value',
            'col_ord' => 'Col Ord',
            'target_table' => 'Target Table',
            'type_name_source' => 'Type Name Source',
            'type_name' => 'Type Name',
        ];
    }
}
