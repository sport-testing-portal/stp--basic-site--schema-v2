<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwSiteTaskGroups]].
 *
 * @see VwSiteTaskGroups
 */
class VwSiteTaskGroupsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwSiteTaskGroups[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwSiteTaskGroups|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
