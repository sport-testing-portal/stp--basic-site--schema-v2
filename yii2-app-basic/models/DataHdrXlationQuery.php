<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DataHdrXlation]].
 *
 * @see DataHdrXlation
 */
class DataHdrXlationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return DataHdrXlation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DataHdrXlation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
