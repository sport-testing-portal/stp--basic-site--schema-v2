<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PlayerTeam]].
 *
 * @see PlayerTeam
 */
class PlayerTeamQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return PlayerTeam[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PlayerTeam|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
