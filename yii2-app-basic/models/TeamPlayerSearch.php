<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeamPlayer;

/**
 * app\models\TeamPlayerSearch represents the model behind the search form about `app\models\TeamPlayer`.
 */
 class TeamPlayerSearch extends TeamPlayer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_player_id', 'team_id', 'player_id', 'team_play_sport_position_id', 'team_play_sport_position2_id', 'coach_id', 'created_by', 'updated_by'], 'integer'],
            [['primary_position', 'team_play_statistical_highlights', 'coach_name', 'team_play_current_team', 'team_play_begin_dt', 'team_play_end_dt', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamPlayer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'team_player_id' => $this->team_player_id,
            'team_id' => $this->team_id,
            'player_id' => $this->player_id,
            'team_play_sport_position_id' => $this->team_play_sport_position_id,
            'team_play_sport_position2_id' => $this->team_play_sport_position2_id,
            'coach_id' => $this->coach_id,
            'team_play_begin_dt' => $this->team_play_begin_dt,
            'team_play_end_dt' => $this->team_play_end_dt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'primary_position', $this->primary_position])
            ->andFilterWhere(['like', 'team_play_statistical_highlights', $this->team_play_statistical_highlights])
            ->andFilterWhere(['like', 'coach_name', $this->coach_name])
            ->andFilterWhere(['like', 'team_play_current_team', $this->team_play_current_team])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
