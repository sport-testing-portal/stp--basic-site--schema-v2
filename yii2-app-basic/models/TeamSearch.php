<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Team;

/**
 * app\models\TeamSearch represents the model behind the search form about `app\models\Team`.
 */
 class TeamSearch extends Team
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id', 'org_id', 'school_id', 'sport_id', 'camp_id', 'gender_id', 'age_group_id', 'team_division_id', 'team_league_id', 'team_competition_season_id', 'team_state_id', 'team_country_id', 'team_wins', 'team_losses', 'team_draws', 'created_by', 'updated_by'], 'integer'],
            [['team_name', 'team_gender', 'team_division', 'team_league', 'organizational_level', 'team_governing_body', 'team_age_group', 'team_website_url', 'team_schedule_url', 'team_schedule_uri', 'team_statistical_highlights', 'team_competition_season', 'team_city', 'created_at', 'updated_at', 'lock'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Team::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'team_id' => $this->team_id,
            'org_id' => $this->org_id,
            'school_id' => $this->school_id,
            'sport_id' => $this->sport_id,
            'camp_id' => $this->camp_id,
            'gender_id' => $this->gender_id,
            'age_group_id' => $this->age_group_id,
            'team_division_id' => $this->team_division_id,
            'team_league_id' => $this->team_league_id,
            'team_competition_season_id' => $this->team_competition_season_id,
            'team_state_id' => $this->team_state_id,
            'team_country_id' => $this->team_country_id,
            'team_wins' => $this->team_wins,
            'team_losses' => $this->team_losses,
            'team_draws' => $this->team_draws,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'team_name', $this->team_name])
            ->andFilterWhere(['like', 'team_gender', $this->team_gender])
            ->andFilterWhere(['like', 'team_division', $this->team_division])
            ->andFilterWhere(['like', 'team_league', $this->team_league])
            ->andFilterWhere(['like', 'organizational_level', $this->organizational_level])
            ->andFilterWhere(['like', 'team_governing_body', $this->team_governing_body])
            ->andFilterWhere(['like', 'team_age_group', $this->team_age_group])
            ->andFilterWhere(['like', 'team_website_url', $this->team_website_url])
            ->andFilterWhere(['like', 'team_schedule_url', $this->team_schedule_url])
            ->andFilterWhere(['like', 'team_schedule_uri', $this->team_schedule_uri])
            ->andFilterWhere(['like', 'team_statistical_highlights', $this->team_statistical_highlights])
            ->andFilterWhere(['like', 'team_competition_season', $this->team_competition_season])
            ->andFilterWhere(['like', 'team_city', $this->team_city])
            ->andFilterWhere(['like', 'lock', $this->lock]);

        return $dataProvider;
    }
}
