<?php

namespace app\models;

use Yii;
use \app\models\base\EntityLinkType as BaseEntityLinkType;

/**
 * This is the model class for table "entity_link_type".
 */
class EntityLinkType extends BaseEntityLinkType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['entity_link_type_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['entity_link_type_name', 'entity_link_type_desc_short', 'entity_link_type_desc_long'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['entity_link_type_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'entity_link_type_id' => 'Entity Link Type ID',
            'entity_link_type_name' => 'Entity Link Type Name',
            'entity_link_type_desc_short' => 'Entity Link Type Desc Short',
            'entity_link_type_desc_long' => 'Entity Link Type Desc Long',
            'lock' => 'Lock',
        ];
    }
}
