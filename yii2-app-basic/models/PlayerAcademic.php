<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerAcademic as BasePlayerAcademic;

/**
 * This is the model class for table "player_academic".
 */
class PlayerAcademic extends BasePlayerAcademic
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_id', 'sat_composite_score_max', 'sat_critical_reading_score_max', 'sat_math_score_max', 'sat_writing_score_max', 'psat_composite_score_max', 'psat_critical_reading_score_max', 'psat_math_score_max', 'psat_writing_score_max', 'act_composite_score_max', 'act_math_score_max', 'act_english_score_max', 'class_rank', 'school_size', 'created_by', 'updated_by'], 'integer'],
            [['sat_scheduled_dt', 'act_scheduled_dt', 'created_at', 'updated_at'], 'safe'],
            [['grade_point_average'], 'number'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_academic_id' => 'Player Academic ID',
            'player_id' => 'Player ID',
            'sat_composite_score_max' => 'Sat Composite Score Max',
            'sat_critical_reading_score_max' => 'Sat Critical Reading Score Max',
            'sat_math_score_max' => 'Sat Math Score Max',
            'sat_writing_score_max' => 'Sat Writing Score Max',
            'sat_scheduled_dt' => 'Sat Scheduled Dt',
            'psat_composite_score_max' => 'Psat Composite Score Max',
            'psat_critical_reading_score_max' => 'Psat Critical Reading Score Max',
            'psat_math_score_max' => 'Psat Math Score Max',
            'psat_writing_score_max' => 'Psat Writing Score Max',
            'act_composite_score_max' => 'Act Composite Score Max',
            'act_math_score_max' => 'Act Math Score Max',
            'act_english_score_max' => 'Act English Score Max',
            'act_scheduled_dt' => 'Act Scheduled Dt',
            'grade_point_average' => 'Grade Point Average',
            'class_rank' => 'Class Rank',
            'school_size' => 'School Size',
            'lock' => 'Lock',
        ];
    }
}
