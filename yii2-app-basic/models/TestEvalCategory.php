<?php

namespace app\models;

use Yii;
use \app\models\base\TestEvalCategory as BaseTestEvalCategory;

/**
 * This is the model class for table "test_eval_category".
 */
class TestEvalCategory extends BaseTestEvalCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['test_eval_category_name'], 'required'],
            [['test_eval_category_display_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['test_eval_category_name'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['test_eval_category_name'], 'unique'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'test_eval_category_id' => 'Test Eval Category ID',
            'test_eval_category_name' => 'Test Eval Category Name',
            'test_eval_category_display_order' => 'Test Eval Category Display Order',
            'lock' => 'Lock',
        ];
    }
}
