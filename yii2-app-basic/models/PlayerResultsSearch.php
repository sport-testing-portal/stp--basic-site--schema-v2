<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerResultsSearch as BasePlayerResultsSearch;

/**
 * This is the model class for table "player_results_search".
 */
class PlayerResultsSearch extends BasePlayerResultsSearch
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['age_range_min', 'age_range_max', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['gender', 'first_name', 'last_name', 'test_provider', 'team', 'position', 'test_type', 'test_category', 'test_description', 'country', 'state_province', 'zip_code', 'zip_code_radius', 'city', 'city_radius'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 1],
            [['person_id'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_results_search_id' => 'Player Results Search ID',
            'gender' => 'Gender',
            'age_range_min' => 'Age Range Min',
            'age_range_max' => 'Age Range Max',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'test_provider' => 'Test Provider',
            'team' => 'Team',
            'position' => 'Position',
            'test_type' => 'Test Type',
            'test_category' => 'Test Category',
            'test_description' => 'Test Description',
            'country' => 'Country',
            'state_province' => 'State Province',
            'zip_code' => 'Zip Code',
            'zip_code_radius' => 'Zip Code Radius',
            'city' => 'City',
            'city_radius' => 'City Radius',
            'lock' => 'Lock',
            'person_id' => 'Person ID',
        ];
    }
}
