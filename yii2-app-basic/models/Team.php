<?php

namespace app\models;

use Yii;
use \app\models\base\Team as BaseTeam;

/**
 * This is the model class for table "team".
 */
class Team extends BaseTeam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['org_id', 'school_id', 'sport_id', 'camp_id', 'gender_id', 'age_group_id', 'team_division_id', 'team_league_id', 'team_competition_season_id', 'team_state_id', 'team_country_id', 'team_wins', 'team_losses', 'team_draws', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['team_name', 'team_league', 'team_schedule_url', 'team_city'], 'string', 'max' => 75],
            [['team_gender', 'lock'], 'string', 'max' => 1],
            [['team_division'], 'string', 'max' => 10],
            [['organizational_level', 'team_governing_body', 'team_age_group'], 'string', 'max' => 45],
            [['team_website_url'], 'string', 'max' => 90],
            [['team_schedule_uri', 'team_competition_season'], 'string', 'max' => 60],
            [['team_statistical_highlights'], 'string', 'max' => 300],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'team_id' => 'Team ID',
            'org_id' => 'Org ID',
            'school_id' => 'School ID',
            'sport_id' => 'Sport ID',
            'camp_id' => 'Camp ID',
            'gender_id' => 'Gender ID',
            'age_group_id' => 'Age Group ID',
            'team_name' => 'Team Name',
            'team_gender' => 'Team Gender',
            'team_division_id' => 'Team Division ID',
            'team_league_id' => 'Team League ID',
            'team_division' => 'Team Division',
            'team_league' => 'Team League',
            'organizational_level' => 'Organizational Level',
            'team_governing_body' => 'Team Governing Body',
            'team_age_group' => 'Team Age Group',
            'team_website_url' => 'Team Website Url',
            'team_schedule_url' => 'Team Schedule Url',
            'team_schedule_uri' => 'Team Schedule Uri',
            'team_statistical_highlights' => 'Team Statistical Highlights',
            'team_competition_season_id' => 'Team Competition Season ID',
            'team_competition_season' => 'Team Competition Season',
            'team_city' => 'Team City',
            'team_state_id' => 'Team State ID',
            'team_country_id' => 'Team Country ID',
            'team_wins' => 'Team Wins',
            'team_losses' => 'Team Losses',
            'team_draws' => 'Team Draws',
            'lock' => 'Lock',
        ];
    }
}
