<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[NotificationLog]].
 *
 * @see NotificationLog
 */
class NotificationLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return NotificationLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return NotificationLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
