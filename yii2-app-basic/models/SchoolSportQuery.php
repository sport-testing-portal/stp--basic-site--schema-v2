<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SchoolSport]].
 *
 * @see SchoolSport
 */
class SchoolSportQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SchoolSport[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SchoolSport|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
