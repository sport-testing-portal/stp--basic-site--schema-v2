<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwCoachTeams6]].
 *
 * @see VwCoachTeams6
 */
class VwCoachTeams6Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return VwCoachTeams6[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return VwCoachTeams6|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
