<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AppChangeLog]].
 *
 * @see AppChangeLog
 */
class AppChangeLogQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AppChangeLog[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AppChangeLog|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
