<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerSchool as BasePlayerSchool;

/**
 * This is the model class for table "player_school".
 */
class PlayerSchool extends BasePlayerSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_id', 'school_id', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['player_school_comment'], 'string', 'max' => 450],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_school_id' => 'Player School ID',
            'player_id' => 'Player ID',
            'school_id' => 'School ID',
            'player_school_comment' => 'Player School Comment',
            'lock' => 'Lock',
        ];
    }
}
