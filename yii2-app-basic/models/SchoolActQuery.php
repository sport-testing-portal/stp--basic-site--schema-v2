<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[SchoolAct]].
 *
 * @see SchoolAct
 */
class SchoolActQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return SchoolAct[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SchoolAct|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
