<?php

namespace app\models;

use Yii;
use \app\models\base\SubscriptionType as BaseSubscriptionType;

/**
 * This is the model class for table "subscription_type".
 */
class SubscriptionType extends BaseSubscriptionType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['subscription_type_name'], 'string', 'max' => 75],
            [['subscription_desc_short'], 'string', 'max' => 45],
            [['subscription_desc_long'], 'string', 'max' => 150],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'subscription_type_id' => 'Subscription Type ID',
            'subscription_type_name' => 'Subscription Type Name',
            'subscription_desc_short' => 'Subscription Desc Short',
            'subscription_desc_long' => 'Subscription Desc Long',
            'lock' => 'Lock',
        ];
    }
}
