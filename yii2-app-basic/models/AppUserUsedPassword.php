<?php

namespace app\models;

use Yii;
use \app\models\base\AppUserUsedPassword as BaseAppUserUsedPassword;

/**
 * This is the model class for table "app_user_used_password".
 */
class AppUserUsedPassword extends BaseAppUserUsedPassword
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['app_user_id', 'password'], 'required'],
            [['app_user_id', 'created_by', 'updated_by'], 'integer'],
            [['set_on', 'created_at', 'updated_at'], 'safe'],
            [['password'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'app_user_used_password_id' => 'App User Used Password ID',
            'app_user_id' => 'App User ID',
            'password' => 'Password',
            'set_on' => 'Set On',
        ];
    }
}
