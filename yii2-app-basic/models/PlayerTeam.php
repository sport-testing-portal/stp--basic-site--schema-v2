<?php

namespace app\models;

use Yii;
use \app\models\base\PlayerTeam as BasePlayerTeam;

/**
 * This is the model class for table "player_team".
 */
class PlayerTeam extends BasePlayerTeam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['player_id', 'team_id', 'created_by', 'updated_by'], 'integer'],
            [['begin_dt', 'end_dt', 'created_at', 'updated_at'], 'safe'],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'player_team_id' => 'Player Team ID',
            'player_id' => 'Player ID',
            'team_id' => 'Team ID',
            'begin_dt' => 'Begin Dt',
            'end_dt' => 'End Dt',
            'lock' => 'Lock',
        ];
    }
}
