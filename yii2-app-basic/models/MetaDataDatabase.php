<?php

namespace app\models;

use Yii;
use \app\models\base\MetaDataDatabase as BaseMetaDataDatabase;

/**
 * This is the model class for table "meta_data__database".
 */
class MetaDataDatabase extends BaseMetaDataDatabase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['table_name'], 'required'],
            [['col_name_len_max', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['table_name'], 'string', 'max' => 150],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'table_name' => 'Table Name',
            'col_name_len_max' => 'Col Name Len Max',
        ];
    }
}
