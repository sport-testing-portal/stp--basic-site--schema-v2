<?php

namespace app\models;

use Yii;
use \app\models\base\AgeGroup as BaseAgeGroup;

/**
 * This is the model class for table "age_group".
 */
class AgeGroup extends BaseAgeGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['sport_id', 'age_group_min', 'age_group_max', 'display_order', 'created_by', 'updated_by'], 'integer'],
            [['age_group_name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['age_group_name', 'age_group_desc'], 'string', 'max' => 45],
            [['lock'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'age_group_id' => 'Age Group ID',
            'sport_id' => 'Sport ID',
            'age_group_name' => 'Age Group Name',
            'age_group_desc' => 'Age Group Desc',
            'age_group_min' => 'Age Group Min',
            'age_group_max' => 'Age Group Max',
            'display_order' => 'Display Order',
            'lock' => 'Lock',
        ];
    }
}
