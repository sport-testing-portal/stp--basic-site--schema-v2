<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TestEvalType]].
 *
 * @see TestEvalType
 */
class TestEvalTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return TestEvalType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TestEvalType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
