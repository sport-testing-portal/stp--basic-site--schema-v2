<?php

namespace app\models;

use Yii;
use \app\models\base\PaymentXferEntity as BasePaymentXferEntity;

/**
 * This is the model class for table "payment_xfer_entity".
 */
class PaymentXferEntity extends BasePaymentXferEntity
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['payment_xfer_entity_name'], 'string', 'max' => 75],
            [['payment_xfer_entity_client_secret', 'payment_xfer_entity_access_token', 'payment_xfer_entity_access_url'], 'string', 'max' => 255],
            [['lock'], 'string', 'max' => 1],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'payment_xfer_entity_id' => 'Payment Xfer Entity ID',
            'payment_xfer_entity_name' => 'Payment Xfer Entity Name',
            'payment_xfer_entity_client_secret' => 'Payment Xfer Entity Client Secret',
            'payment_xfer_entity_access_token' => 'Payment Xfer Entity Access Token',
            'payment_xfer_entity_access_url' => 'Payment Xfer Entity Access Url',
            'lock' => 'Lock',
        ];
    }
}
