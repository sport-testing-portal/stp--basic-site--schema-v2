<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic_utf8',
    //'dsn' => 'mysql:host=localhost;dbname=yii2basic',
	
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    
    // CREATE SCHEMA `yii2basic_utf8` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
